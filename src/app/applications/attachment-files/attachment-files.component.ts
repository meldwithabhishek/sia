import { ConfigurationService } from './../../shared/services/configuration.service';
import { AuthService } from './../../shared/services/auth.service';
import { ApplicationDetailsService } from './../application-details/application-details.service';
import { Component, OnInit, Input } from '@angular/core';
import { FileUploader, Headers } from 'ng2-file-upload';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/Rx' ;

@Component({
  selector: 'app-attachment-files',
  templateUrl: './attachment-files.component.html',
  styleUrls: ['./attachment-files.component.css']
})
export class AttachmentFilesComponent implements OnInit {

  editModeOn = false;

  uploader: FileUploader;
  previousLength = 0;

  fileNames: any

  @Input('isEditAllowed')
  isEditAllowed: boolean;

  @Input('appDetails')
  appDetails: any;

  constructor(private route: ActivatedRoute, private router: Router,
    private appServ: ApplicationDetailsService, public authServ: AuthService,
    private config: ConfigurationService) { }

  ngOnInit() {
    
    let url = this.config.getConfig().SIA_WEB_APP + "File/" + this.appDetails.name + "/" + this.appDetails.airlineName
    let headers : Array<Headers> = []
    let header : Headers = {name : "Requester", value : this.authServ.getUserName()}
    //header.name = 'Requester'
    //header.value = this.authServ.getUserName()
    headers.push(header)
    
    this.uploader = new FileUploader({ url: url , headers : headers });
    this.getFilenames()
  }

  getURL(fileName) {
    return this.config.getConfig().SIA_WEB_APP + 'File/' + this.appDetails.name + '/' + this.appDetails.airlineName + '/' + fileName
  }

  getFilenames() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getFilenames(this.appDetails).subscribe(respList => {
      this.fileNames = respList
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get Uploaded Files"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });

  }

  uploadFile(item) {
    item.upload();
    this.uploader.onSuccessItem = (item, response, status, header) => {
      bootbox.alert({
        title: "Success",
        size: "small",
        message: "Attachment successfully uploaded."
      });
      this.getFilenames()
    };

    this.uploader.onErrorItem = (item, response, status, header) => {
      let message = "Attachment Upload failed."
      try {
        let respJson = JSON.parse(response)
        if (respJson.errorMessage) {
          message = respJson.errorMessage
        }
      } catch (error) {
      }

      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    };

  }

  fileChanged(evt) {

    if (this.uploader.queue.length == this.previousLength) {
      this.uploader.queue.pop();
      this.previousLength = 0;
    }

    if (this.uploader.queue && this.uploader.queue.length > 0) {
      let fileArray = new Array();
      fileArray.push(this.uploader.queue.pop());
      this.uploader.queue = fileArray;
      this.previousLength = this.uploader.queue.length;
    }
  }

  deleteFile(fileName) {
    let thefile = {};
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.deleteFile(fileName, this.appDetails.name, this.appDetails.airlineName).subscribe(respList => { 
      this.getFilenames()
      bootbox.alert({
        title: "Success",
        size: "small",
        message: "Successfully removed " + fileName
      });
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to remove file : " + fileName
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });    

  }
}


declare let bootbox: any;
declare let HoldOn: any;