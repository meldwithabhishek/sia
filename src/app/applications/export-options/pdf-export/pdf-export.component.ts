import { Component, OnInit, Input } from '@angular/core';
import { ChannelInfo } from '../../../shared/models/channel-info-model';
import { ApplicationDetailsService } from '../../application-details/application-details.service';

@Component({
  selector: 'app-pdf-export',
  templateUrl: './pdf-export.component.html',
  styleUrls: ['./pdf-export.component.css']
})
export class PdfExportComponent implements OnInit {

  constructor(private appServ: ApplicationDetailsService) { }
  docDefinition;

  @Input('appDetails')
  appDetails: any

  @Input('intro')
  intro: any

  public fileName: string

  volumeJSON: any

  ngOnInit() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getVolumeControlJSON().subscribe(
      resp => {
        this.volumeJSON = resp
        this.fileName = "SIA_" + this.appDetails.airlineName + "_" + this.appDetails.name + "_v" + this.appDetails.version
        this.docDefinition = this.formPdfStructure()
        HoldOn.close()
      },
      error => {
        this.fileName = "SIA_" + this.appDetails.airlineName + "_" + this.appDetails.name + "_v" + this.appDetails.version
        this.docDefinition = this.formPdfStructure()
        HoldOn.close()
      }
    )


  }

  downloadPDF() {
    pdfMake.createPdf(this.docDefinition).download(this.fileName + '.pdf');
  }

  formPdfStructure() {

    let channelInfo = this.appDetails.channelInfo
    if (channelInfo == null) {
      channelInfo = new ChannelInfo()
      channelInfo.meanSessionsOpenedPerDay = ""
      channelInfo.peakSessionsOpenedPerDay = ""
      channelInfo.meanSessionsOpenedPerSecond = ""
      channelInfo.peakSessionsOpenedPerSecond = ""
      channelInfo.sessionUsageDistribution = ""
      channelInfo.meanConcurrentSessionsOpen = ""
      channelInfo.peakConcurrentSessionsOpen = ""
      channelInfo.meanLoggingServiceCallsPerDay = ""
      channelInfo.blacklistingLevel = ""
      channelInfo.warningThreshold = ""
      channelInfo.blacklistingThreshold = ""
      channelInfo.callerIdPattern = ""
      channelInfo.capiVersion = ""
      channelInfo.customerId = ""
      channelInfo.applicationId = ""
    }

    // playground requires you to assign document definition to a variable called dd

    var title =
      {
        text: "\n\n\n\n" + this.appDetails.name + " " + this.appDetails.version + "\n\n",
        style: ['title']
      }

    var docDetails = {
      style: 'tableExample',
      table: {
        headerRows: 2,
        widths: [200, '*', '*'],
        // keepWithHeaderRows: 1,
        body: [
          [{ text: 'System Inteface Agreement', style: 'tableHeader' }, { text: this.appDetails.name, colSpan: 2, alignment: 'center' }, {}],
          [{ text: 'SIA Version', style: 'tableHeader' }, { text: this.appDetails.version, colSpan: 2, alignment: 'center' }, {}],
          [{ rowSpan: 2, text: 'Approved by application \nsystem responsible', style: 'tableHeader' }, { text: 'Approved by', bold: true }, this.appDetails.applicationSystemApprovedBy],
          ['', { text: 'Approved date', bold: true }, this.appDetails.applicationSystemApprovedDate],
          [{ rowSpan: 2, text: 'Approved by ZAMBAS R6 \nsystem responsible', style: 'tableHeader' }, { text: 'Approved by', bold: true }, this.appDetails.zr6SystemApprovedBy],
          ['', { text: 'Approved date', bold: true }, this.appDetails.zr6SystemApprovedDate],
        ]
      }
    }

    var confidential = [
      {
        text: "\n\n\n\n\n\n\n"
      },
      {
        style: 'tableExample',
        table: {
          headerRows: 0,
          widths: ['*'],
          // keepWithHeaderRows: 1,
          body: [
            [{ text: 'Confidentiality : \n\n This document and its content are the property of Lufthansa Passenger Airline. Complete or partial duplication and delivery to third parties or any other form of publication is prohibited with-out the prior written approval by Lufthansa Passenger Airline. Copies for internal use are not subject to this restriction.' }]
          ]
        }
      }]


    var applicationInformation = {
      style: 'tableExample',
      table: {
        headerRows: 2,
        widths: [200, '*'],
        // keepWithHeaderRows: 1,
        body: [
          [{ text: 'Application name', style: 'tableHeader' }, this.appDetails.name],
          [{ text: 'Airline name', style: 'tableHeader' }, this.appDetails.airlineName],
          [{ text: 'Application owner', style: 'tableHeader' }, this.appDetails.owner],
          [{ text: 'PIT Id', style: 'tableHeader' }, this.appDetails.pitID],
          [{ text: 'Application contacts (notifications)', style: 'tableHeader' }, this.appDetails.contacts_Notification],
          [{ text: 'Application contacts (incident)', style: 'tableHeader' }, this.appDetails.contacts_Incident],
          [{ text: 'Instruction for Incident', style: 'tableHeader' }, this.appDetails.instructionForIncidence],
          [{ text: 'Application description', style: 'tableHeader' }, this.appDetails.desc],
          [{ text: 'Criticality of application', style: 'tableHeader' }, this.appDetails.criticality],
          [{ text: 'Justification of criticality level', style: 'tableHeader' }, this.appDetails.criticalityJustification],

        ]
      }
    }

    var zr6Info = {
      style: 'tableExample',
      table: {
        headerRows: 2,
        widths: [200, '*'],
        // keepWithHeaderRows: 1,
        body: [
          [{ text: 'ZAMBAS R6 System Responsible', style: 'tableHeader' }, { text: this.intro.zr6SysResp }],
          [{ text: 'ZAMBAS R6 Change Management', style: 'tableHeader' }, { text: this.intro.zr6ChngMngmnt }],
          [{ text: 'ZAMBAS R6 Incident Management', style: 'tableHeader' }, { text: this.intro.zr6IncMngmnt }],
          [{ text: 'ZAMBAS R6 Description', style: 'tableHeader' }, { text: this.intro.zr6Desc }],

        ]
      }
    }

    //this.appDetails.channelInfo

    var syncChannel = [
      {
        text:
          [
            { text: 'Synchronous channel ', style: 'subheader' },

            { text: '\n\nSynchronous services are typically accessed via the ZAMBAS R6 Client Application Programming Interface (CAPI). The CAPI consist of the CAPI Core libraries for access to synchronous services on the middleware which make most implementation details transparent to the application and the respective service stubs that enable usage of specific functional services by the application.\n\nThe application is only entitled to use the CAPI Core version as documented below for the access to synchronous services. In addition the application shall access only the synchronous services listed in the respective section of this document. \n\n' }
          ]
      },
      {
        style: 'tableExample',
        table: {
          headerRows: 2,
          widths: [150, 300],
          // keepWithHeaderRows: 1,
          body: [
            [{ text: 'CAPI CORE Version', style: 'tableHeader' }, channelInfo.capiVersion],
            [{ text: 'Caller ID pattern', style: 'tableHeader' }, channelInfo.callerIdPattern],

          ]
        }
      },
      "\nThe CAPI can use JMS or HTTP as transport protocols for access to synchronous services. The usage of JMS is preferred. The application shall only use the transport protocol requested and documented in this SIA, which shall be in turn granted by ZAMBAS R6. \n\n",
      {
        ol: [
          [{
            text:
              [
                { text: 'Synchronous access using JMS', style: 'subheader' },

                { text: '\nFor accessing the services via CAPI using JMS, the application must connect to the following IPs and ports. The connection establishment is typically handled by the CAPI transparently. \n\n' }
              ]
          },
          {
            style: 'tableExample',
            table: {
              headerRows: 2,
              widths: [150, 100, '*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'Transport protocol used', style: 'tableHeader' }, { text: 'IP address', style: 'tableHeader' }, { text: 'Ports', style: 'tableHeader' }],
                [{ text: 'JMS' }, { text: '82.150.238.16' }, { text: '11551(SSL Encrypted service calls);\n11561(SSL Encrypted service calls for services which require encryption);' }],

              ]
            }
          }, "\n"],
          [{
            text:
              [
                { text: 'Synchronous access using HTTPs', style: 'subheader' },

                { text: '\nFor accessing the services via CAPI using HTTP, the application must connect to the following IPs and ports. The connection establishment is typically handled by the CAPI transparently.\n\n' }
              ]
          },
          {
            style: 'tableExample',
            table: {
              headerRows: 2,
              widths: [150, 100, '*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'Transport protocol used', style: 'tableHeader' }, { text: 'IP address', style: 'tableHeader' }, { text: 'Ports', style: 'tableHeader' }],
                [{ text: 'HTTP' }, { text: '82.150.238.20' }, { text: '11544(SSL Encrypted service calls);' }],
                [{ text: 'HTTP' }, { text: '82.150.238.10 (INT)' }, { text: '5044(SSL Encrypted service calls);' }],
                [{ text: 'HTTP' }, { text: '82.150.238.5 (TST)' }, { text: '5044(SSL Encrypted service calls);' }],
              ]
            }
          }, "\n"],
          [{
            text:
              [
                { text: 'Application provided web services', style: 'subheader' },

                { text: '\nData exchange using application provided web services over HTTP or HTTPs is supported by ZAMBAS R6. The application has to make the services to be called by ZAMBAS R6 available on its servers. The IP and port of the application HTTP server is recorded per service in the service interface section of this document.\n\nThe application must provide access for ZAMBAS R6 from the following listed IPs to their service offering web server.\n\n' }
              ]
          },
          {
            style: 'tableExample',
            table: {
              headerRows: 2,
              widths: ['*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'IP address of middleware server(s)', style: 'tableHeader' }],
                [{ text: '82.150.239.145;' }],
                [{ text: '82.150.239.146;' }],
                [{ text: '82.150.239.153;' }],
                [{ text: '82.150.239.164;' }],
                [{ text: '82.150.239.165;' }],
                [{ text: '82.150.239.172' }],
              ]
            }
          }, "\n"],
          [{
            text:
              [
                { text: 'Synchronous session handling', style: 'subheader' },

                { text: '\nSessions opened against Altéa Backend (NOT ZR6!).\n\n' },

                { text: '\nSession establishment and authentication frequency\n\n', bold: true }
              ]
          },
          {
            pageBreak: 'before',
            style: 'tableExample',
            table: {
              headerRows: 2,
              widths: [200, '*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'Mean sessions opened per day', style: 'tableHeader' }, channelInfo.meanSessionsOpenedPerDay],
                [{ text: 'Peak sessions opened per day', style: 'tableHeader' }, channelInfo.peakSessionsOpenedPerDay],
                [{ text: 'Mean sessions opened per second', style: 'tableHeader' }, channelInfo.meanSessionsOpenedPerSecond],
                [{ text: 'Peak sessions opened per second', style: 'tableHeader' }, channelInfo.peakSessionsOpenedPerSecond],
                [{ text: 'Session usage distribution', style: 'tableHeader' }, channelInfo.sessionUsageDistribution],

              ]
            }
          }, {
            text:
              [
                { text: '\nConcurrent sessions open at a time\n', bold: true }
              ]
          },
          {

            style: 'tableExample',
            table: {
              headerRows: 2,
              widths: [200, '*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'Mean concurrent sessions open', style: 'tableHeader' }, channelInfo.meanConcurrentSessionsOpen],
                [{ text: 'Peak concurrent sessions open', style: 'tableHeader' }, channelInfo.peakConcurrentSessionsOpen],

              ]
            }
          }, "\n"],
          [{
            text:
              [
                { text: 'Synchronous channel black listing', style: 'subheader' },

                { text: '\nAs an additional measure to protect the platform from excessive unwanted load a blacklisting functionality is in place. An application is blacklisted if the number of synchronous service calls per 5 minute timeframe exceeds the predefined limit for a specific service. Please note that once blacklisted, all services will be blocked from the application until the application is white listed again. An application will be automatically white listed (removed from blacklisting list) if request load (including rejected requests) drops below the limit. The thresholds are set commonly for all synchronous services used by the application. The blacklisting level is chosen based on the “peak calls per service” value which is provided in the service interface section.\n\n' },

              ]
          },
          {
            style: 'tableExample',
            table: {
              headerRows: 1,
              widths: ['*', '*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'Level', style: 'tableHeader' }, { text: 'Volume Control', style: 'tableHeader' }],
                [channelInfo.blacklistingLevel, this.getVolumeControlValue(channelInfo.blacklistingLevel)],

              ]
            }
          }, "\n"]

        ]
      }
    ]

    var asyncChannel = [
      {
        pageBreak: 'before',
        text:
          [
            { text: 'Asynchronous channel', style: 'subheader' },

            { text: '\n\nFor accessing services working asynchronously, ZAMBAS R6 provides a JMS interface which an application can connect to. This interface provides messaging services compatible with JMS for topics and queues.  The application is encouraged to use the libraries provided as a part of CAPI for accessing asynchronous services, yet other compatible libraries for JMS access.\n\nIf the CAPI Core libraries are used, the version number of these must be documented below. In addition the application shall only access the asynchronous services listed in the respective sec-tion of this document.\n\nZAMBAS R6 asynchronous services are hosted in separate instances providing JMS services. The application has to connect to the proper instance to be able to utilize an asynchronous service.\n\n' }
          ]
      },
      {
        ol: [

          [{
            text:
              [
                { text: 'ASYNCH-IN instance', style: 'subheader' },

                { text: '\nFor accessing the JMS objects in this instance, the application must connect to the following IPs and ports. \n\n' }
              ]
          },
          {
            style: 'tableExample',
            table: {
              headerRows: 1,
              widths: [150, 100, '*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'Instance', style: 'tableHeader' }, { text: 'IP address', style: 'tableHeader' }, { text: 'Ports', style: 'tableHeader' }],
                [{ text: 'ASYNCH-IN' }, { text: '82.150.238.16' }, { text: '11581(SSL Encrypted access);' }],

              ]
            }
          }, "\n"],
          [{
            text:
              [
                { text: 'ASYNCH-OUT instance', style: 'subheader' },

                { text: '\nFor accessing the JMS objects in this instance, the application must connect to the following IPs and ports. \n\n' }
              ]
          },
          {
            style: 'tableExample',
            table: {
              headerRows: 1,
              widths: [150, 100, '*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'Server used for client API', style: 'tableHeader' }, { text: 'IP address', style: 'tableHeader' }, { text: 'Ports', style: 'tableHeader' }],
                [{ text: 'ASYNCH-OUT' }, { text: '82.150.239.155;\n82.150.239.174' }, { text: '11571(SSL Encrypted access);' }],

              ]
            }
          }, "\n"]
        ]
      }
    ]

    var batchChannel = [
      {
        pageBreak: 'before',
        text:
          [
            { text: 'Batch channel', style: 'subheader' },

            { text: '\n\nThe batch channel is based on the File Transfer Protocol (FTP) and used to transfer files between the application and the ZAMBAS R6. Two distinct types of transfer are supported by ZAMBAS R6 – a transfer initiated by the application and a transfer initiated by ZAMBAS R6. In the former case the application connects to ZAMBAS R6 FTP server in order to upload or download files. In the later case ZAMBAS R6 connects to an application FTP server to upload files ready for the appli-cation usage. The ZAMBAS R6 FTP server supports both the standard FTP protocol and secured SFTP protocol for incoming and outgoing connections. The application may choose which proto-col shall be used, which must be documented in the appropriate service interface section in this SIA.\n\n' }
          ]
      },
      {
        ol: [

          [{
            text:
              [
                { text: 'Application initiated file transfer', style: 'subheader' },

                { text: '\nFor application initiated transfer the application can either use the CAPI Core libraries which pro-vide FTP client functionality or any other FTP client to access the ZAMBAS R6 FTP server and upload or download files. If the CAPI Core libraries are used the version number of these must be documented below. In addition the application shall only access the BATCH services listed in the respective section of this.\n\nFor accessing the services via FTP connection, the application must connect to the following IPs and ports.\n\n' }
              ]
          },
          {
            style: 'tableExample',
            table: {
              headerRows: 1,
              widths: [150, 100, '*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'Server used for client API', style: 'tableHeader' }, { text: 'IP address', style: 'tableHeader' }, { text: 'Ports', style: 'tableHeader' }],
                [{ text: 'SFTP Server' }, { text: '82.150.238.16' }, { text: '11522' }],

              ]
            }
          }, "The public key of the ZAMBAS R6 SFTP server will be provided to the application upon request. In case of a key change the ZAMBAS R6 will be responsible to notify the application.\n\n"],
          [{
            text:
              [
                { text: 'ZAMBAS R6 initiated file upload to the application', style: 'subheader' },

                { text: '\nIn case of a ZAMBAS R6 initiated file upload to the application, the ZAMBAS R6 FTP servers, located in the ZAMBAS R6 environment initiate a connection to application FTP or SFTP server and upload a file (as defined per service later in this document). The IP and port of the application FTP or SFTP server is documented within the service interface section.\n\n The application shall provide access for ZAMBAS R6 from following IPs to the applications FTP Server. \n\n' }
              ]
          },
          {
            style: 'tableExample',
            table: {
              headerRows: 1,
              widths: ['*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'Server used for client API', style: 'tableHeader' }],
                [{ text: '82.150.239.142;\n82.150.239.161' }],

              ]
            }
          }, "\n"],
          [{
            text:
              [
                { text: 'Application initiated file download from ZAMBAS R6', style: 'subheader' },

                { text: '\nIn case of a application initiated recovery file download to the application, the application SFTP servers, located in the application environment initiate a connection to ZAMBAS R6 SFTP server and download a file (as defined per service later in this document). The IP and Port of the applica-tion SFTP server(s) and ZAMBAS R6 SFTP server(s) is documented below.\n\nZAMBAS R6 shall provide access for the application from following IPs to the ZAMBAS R6 SFTP Server.\n\n' }
              ]
          },
          {
            style: 'tableExample',
            table: {
              headerRows: 1,
              widths: ['*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'IP address ZAMBAS R6 SFTP server(s)', style: 'tableHeader' }],
                [{ text: '82.150.238.16:11522' }],

              ]
            }
          },
            "\n",
          {
            style: 'tableExample',
            table: {
              headerRows: 1,
              widths: ['*'],
              // keepWithHeaderRows: 1,
              body: [
                [{ text: 'IP address application SFTP server(s)', style: 'tableHeader' }],
                [{ text: 'PROD\t\tmunrrcthpapr01\t172.19.104.245\nPROD\t\tmunrrcthpapr02\t172.19.104.246\nINT\t\t  munrrcthiapr01\t172.19.104.181\nINT\t\t  munrrcthiapr02\t172.19.104.182\nTST1\t\tmunrrcthtwbr01\t172.19.104.76\nTST2\t\tmunrrcthtwbr21\t172.19.104.97\nDEV1\t\tmunrrchtdwbr01\t172.19.104.11\nDEV2\t\tmunrrcthdwbr21\t172.19.104.33' }],

              ]
            }
          }
          ]
        ]
      }
    ]

    //Sync Services
    var syncList = []

    for (let i = 0; i < this.appDetails.syncServices.length; i++) {

      var syncDetails: any[] = []

      syncDetails.push([{ text: 'Service Name', style: 'tableHeader' }, this.appDetails.syncServices[i].serviceName])
      syncDetails.push([{ text: 'Operation', style: 'tableHeader' }, this.appDetails.syncServices[i].operation])
      syncDetails.push([{ text: 'Version', style: 'tableHeader' }, this.appDetails.syncServices[i].version])
      syncDetails.push([{ text: 'Application Role', style: 'tableHeader' }, this.appDetails.syncServices[i].appRole])
      syncDetails.push([{ text: 'Comments', style: 'tableHeader' }, this.appDetails.syncServices[i].comments])
      if (this.appDetails.syncServices[i].appRole == 'Producer') {
        syncDetails.push([{ text: 'Service Url', style: 'tableHeader' }, this.appDetails.syncServices[i].serviceUrl])
      }
      if (this.appDetails.syncServices[i].appRole == 'Consumer') {
        syncDetails.push([{ text: 'Protocol Used', style: 'tableHeader' }, this.appDetails.syncServices[i].protocolUsed])
      }
      syncDetails.push([{ text: 'Purpose Of Usage', style: 'tableHeader' }, this.appDetails.syncServices[i].purposeOfUsage])
      syncDetails.push([{ text: 'Criticality', style: 'tableHeader' }, this.appDetails.syncServices[i].criticality])
      syncDetails.push([{ text: 'Justification of Criticality', style: 'tableHeader' }, this.appDetails.syncServices[i].criticalityJustification])
      syncDetails.push([{ text: 'Workaround', style: 'tableHeader' }, this.appDetails.syncServices[i].workaround])
      syncDetails.push([{ text: 'Data Worth Protection', style: 'tableHeader' }, this.appDetails.syncServices[i].dataWorthProtection])
      if (this.appDetails.syncServices[i].appRole == 'Producer') {
        syncDetails.push([{ text: 'Prov Via Extern', style: 'tableHeader' }, this.appDetails.syncServices[i].provViaExtern])
      }
      if (this.appDetails.syncServices[i].appRole == 'Producer') {
        syncDetails.push([{ text: 'Max Cap', style: 'tableHeader' }, this.appDetails.syncServices[i].maxCap])
      }


      var sync: any[] = [{
        text:
          [
            { text: this.appDetails.syncServices[i].serviceName + '.' + this.appDetails.syncServices[i].operation + '\n\n', style: 'subheader' },
          ]
      },
      {
        table: {
          headerRows: 0,
          widths: [150, 300],
          // keepWithHeaderRows: 1,
          body: syncDetails
        }
      },
        '\n'
      ]

      if (this.appDetails.syncServices[i].appRole == 'Consumer') {

        sync.push({
          text:
            [
              { text: 'Service usage behaviour\n', bold: true },
            ]
        })

        sync.push({
          table: {
            headerRows: 0,
            widths: [150, 300],
            // keepWithHeaderRows: 1,
            body: [
              [{ text: 'Mean Calls Per Day', style: 'tableHeader' }, this.appDetails.syncServices[i].meanCallsPerDay],
              [{ text: 'Peak Calls Per Day', style: 'tableHeader' }, this.appDetails.syncServices[i].peakCallsPerDay],
              [{ text: 'Mean Calls Per Minute', style: 'tableHeader' }, this.appDetails.syncServices[i].meanCallsPerSec],
              [{ text: 'Peak Calls Per Minute', style: 'tableHeader' }, this.appDetails.syncServices[i].peakCallsPerSec],
              [{ text: 'Daily Load Distribution Period', style: 'tableHeader' }, this.appDetails.syncServices[i].dailyLoadDistributionPeriod]

            ]
          }
        })

        sync.push('\n')

      }

      syncList.push(sync)

    }



    var asyncList = []

    for (let i = 0; i < this.appDetails.asyncServices.length; i++) {


      var emsList = []

      if (this.appDetails.asyncServices[i].emsDetails != null) {
        for (let j = 0; j < this.appDetails.asyncServices[i].emsDetails.length; j++) {

          var emsDetailsTag1 = [
            {
              text:
                [
                  { text: this.appDetails.asyncServices[i].emsDetails[j].emsDestinationName + '\n\n', bold: true },
                ]
            },
            '\n'
          ]

          var emsDetailsTag = [
            {
              text:
                [
                  { text: this.appDetails.asyncServices[i].emsDetails[j].emsDestinationName + '\n\n', bold: true, size : 12 },
                ]
            },
            {
              table: {
                headerRows: 0,
                widths: [150, 300],
                // keepWithHeaderRows: 1,
                body: [
                  [{ text: 'EMS Destination Type', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].emsDestinationType],
                  [{ text: 'EMS Destination Name', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].emsDestinationName],
                  [{ text: 'EMS Destination Prop', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].emsDestinationProp],
                  [{ text: 'EMS Destination Purpose', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].emsDestinationPurpose],
                  [{ text: 'EMS Destination Full Strategy', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].emsDestinationFullStrategy],
                  [{ text: 'EMS Durable Name', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].emsDurableName],
                  [{ text: 'EMS Durable Prop', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].emsTechUserGroup],
                  [{ text: 'Bridged Destination Type', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].bridgedDestinationType],
                  [{ text: 'Bridged Destination Name', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].bridgedDestinationName],
                  [{ text: 'User Permission', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].userPermission],
                  [{ text: 'Deployed In Instance', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].deployedInInstance],
                  [{ text: 'Mean Calls Per Day', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].meanCallsPerDay],
                  [{ text: 'Peak Calls Per Day', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].peakCallsPerDay],
                  [{ text: 'Mean Calls Per Minute', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].meanCallsPerSec],
                  [{ text: 'Peak Calls Per Minute', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].peakCallsPerSec],
                  [{ text: 'Daily Load Distribution Period', style: 'tableHeader' }, this.appDetails.asyncServices[i].emsDetails[j].dailyLoadDistributionPeriod]
                ]
              }
            },
            '\n'
          ]

          emsList.push(emsDetailsTag)

        }
      }

      var asyncServ: any[] = [{
        text:
          [
            { text: this.appDetails.asyncServices[i].serviceName + '\n\n', style: 'subheader' },
          ]
      },
      {
        table: {
          headerRows: 0,
          widths: [150, 300],
          // keepWithHeaderRows: 1,
          body: [
            [{ text: 'Service Name', style: 'tableHeader' }, this.appDetails.asyncServices[i].serviceName],
            [{ text: 'Version', style: 'tableHeader' }, this.appDetails.asyncServices[i].version],
            [{ text: 'Application Role', style: 'tableHeader' }, this.appDetails.asyncServices[i].appRole],
            [{ text: 'Comments', style: 'tableHeader' }, this.appDetails.asyncServices[i].comments],
            [{ text: 'Purpose Of Usage', style: 'tableHeader' }, this.appDetails.asyncServices[i].purposeOfUsage],
            [{ text: 'Criticality', style: 'tableHeader' }, this.appDetails.asyncServices[i].criticality],
            [{ text: 'Justification of Criticality', style: 'tableHeader' }, this.appDetails.asyncServices[i].criticalityJustification],
            [{ text: 'Workaround', style: 'tableHeader' }, this.appDetails.asyncServices[i].workaround],
            [{ text: 'Data Worth Protection', style: 'tableHeader' }, this.appDetails.asyncServices[i].dataWorthProtection],
          ]
        }
      },
        '\n',
        emsList,        
        '\n'
      ]

      asyncList.push(asyncServ)
    }



    var batchList = []

    for (let i = 0; i < this.appDetails.batchServices.length; i++) {


      var fileDetails: any[] = [
        [{ text: 'IP Address & Port', style: 'tableHeader' }, this.appDetails.batchServices[i].ipAddPort],
        [{ text: 'File Retention on ZR6 [in Days]', style: 'tableHeader' }, this.appDetails.batchServices[i].fileRetentionZR6],
        [{ text: 'Filename Pattern on ZR6', style: 'tableHeader' }, this.appDetails.batchServices[i].filenamePatternZR6],
        [{ text: 'Filename Location on ZR6', style: 'tableHeader' }, this.appDetails.batchServices[i].filenameLocationZR6],
        [{ text: 'Internet Access', style: 'tableHeader' }, this.appDetails.batchServices[i].internetAccess],
        [{ text: 'Mean File Size', style: 'tableHeader' }, this.appDetails.batchServices[i].meanFileSize],
        [{ text: 'Maximum File Size', style: 'tableHeader' }, this.appDetails.batchServices[i].maxFileSize],
        [{ text: 'Mean Files Transfered Per Month', style: 'tableHeader' }, this.appDetails.batchServices[i].meanFileTransferPerMonth],
        [{ text: 'Maximum Files Transfered Per Month', style: 'tableHeader' }, this.appDetails.batchServices[i].maxFileTransferPerMonth],
        [{ text: 'FTP Transfer Distribution', style: 'tableHeader' }, this.appDetails.batchServices[i].ftpTransferDist],

      ]

      if (this.appDetails.batchServices[i].appRole == 'Consumer') {
        fileDetails.push([{ text: 'Filename Pattern on Application', style: 'tableHeader' }, this.appDetails.batchServices[i].filenamePatternApp])
        fileDetails.push([{ text: 'Filename Location Application', style: 'tableHeader' }, this.appDetails.batchServices[i].filenameLocationApp])
        fileDetails.push([{ text: 'File Transfer Direction', style: 'tableHeader' }, this.appDetails.batchServices[i].fileTransferDirection])

      }

      var appRole = ""
      if (this.appDetails.batchServices[i].appRole == 'Consumer') {
        appRole = 'Downloads from ZR6'
      } else if (this.appDetails.batchServices[i].appRole == 'Producer') {
        appRole = 'Uploads to ZR6'
      }

      var batch = [{
        text:
          [
            { text: this.appDetails.batchServices[i].serviceName + '\n\n', style: 'subheader' },
          ],
      },
      {
        table: {
          headerRows: 0,
          widths: [150, 300],
          // keepWithHeaderRows: 1,
          body: [
            [{ text: 'Service Name', style: 'tableHeader' }, this.appDetails.batchServices[i].serviceName],
            [{ text: 'Version', style: 'tableHeader' }, this.appDetails.batchServices[i].version],
            [{ text: 'Application Role', style: 'tableHeader' }, appRole],
            [{ text: 'Connection Initiated By', style: 'tableHeader' }, this.appDetails.batchServices[i].connectionInitiatedBy],
            [{ text: 'Comments', style: 'tableHeader' }, this.appDetails.batchServices[i].comments],
            [{ text: 'Purpose Of Usage', style: 'tableHeader' }, this.appDetails.batchServices[i].purposeOfUsage],
            [{ text: 'Criticality', style: 'tableHeader' }, this.appDetails.batchServices[i].criticality],
            [{ text: 'Justification of Criticality', style: 'tableHeader' }, this.appDetails.batchServices[i].criticalityJustification],
            [{ text: 'Workaround', style: 'tableHeader' }, this.appDetails.batchServices[i].workaround],
            [{ text: 'Protocol Used', style: 'tableHeader' }, this.appDetails.batchServices[i].protocolUsed],
            [{ text: 'Data Worth Protection', style: 'tableHeader' }, this.appDetails.batchServices[i].dataWorthProtection],

          ]
        }
      },
        '\n',
      {
        text:
          [
            { text: 'File Details\n', bold: true },
          ]
      },
      {
        table: {
          headerRows: 0,
          widths: [150, 300],
          // keepWithHeaderRows: 1,
          body: fileDetails
        }
      },
        '\n'
      ]

      batchList.push(batch)

    }



    var dd = {

      background: { text: "\n" + this.fileName, color: 'gray', alignment: 'center' },

      content: [
        title,
        docDetails,
        confidential,
        {
          pageBreak: 'before',
          ol: [
            {
              text:
                [
                  { text: 'Introduction ', style: 'header', tocItem: true },

                  { text: '\nThis document contains the description of the interface between ZAMBAS R6 and an external application receiving or providing services from or to ZAMBAS R6 for productive use. The descrip-tion of this interface, together with the definitions of usage as described in this document is re-ferred to as System Interface Agreement (SIA). \n\nThe purpose of this document is to establish a mutual agreement and understanding for the inter-face configuration, the provided / consumed services incl. the amount of service usage and used communication protocols between ZAMBAS R6 and the application and the definition of require-ments and responsibilities. This agreement provides the basis to facilitate seamless, reliable and controlled access for service provision and consumption by the respective parties in a production environment.\n\n' }
                ]
            },
            {
              ol: [
                {
                  text:
                    [
                      { text: 'Document Scope ', style: 'subheader' },

                      { text: '\nThe scope of this document is defined by the description and provision of information required for the interaction between the application and ZAMBAS R6. This document shall cover a description of the technical connectivity between the systems and the required configurations, technical pa-rameters and limits of the services consumed by each application. Information about service con-sumption relevant for capacity management on either side and limits for the consumption shall also be described. \n\nThis document does not provide functional specification of services or detailed functional descrip-tion of their usage by the consumer. Information about service behavior like response times, availability and service level can be looked for in the service repository or should be requested from the service responsible. The definition of requirements for service levels or performance characteristics is out of scope of this document and should be filed via the appropriate processes towards the respective service responsible.\n\nThe provision of security related data, mainly credentials to access ZAMBAS R6 or vice versa is out of scope of this document and is handled by a separate process. \n\nThe definition of processes is out of scope of this document. \n\n' }
                    ]
                },
                {
                  text:
                    [
                      { text: 'Responsibilities ', style: 'subheader' },

                      { text: '\nThis document is an operational level agreement (OLA) between the persons responsible for ZAMBAS R6 and the application. Therefore all information, statements and parameters within this document are binding for both parties. The responsible persons are required to maintain this doc-ument in accordance to the SIA Guide, ensuring its content is current and up to date, document standards and deadlines are adhered to.\n\nEspecially information relevant for capacity management recorded in the service usage behavior sections of the SIA has to be reviewed by the service consumer after 6 month following the last review or change at the latest. The review shall be made against actual capacity consumed which is visible from the SOA Portal. Application specific measurements may be used in addition. \n\nService consumer is responsible to request any increase or change of service usage behavior as early as possible by updating the SIA to ensure appropriate platform sizing in case required.\n\n' }
                    ]
                }
              ]
            }
          ],
        }, {
          pageBreak: 'before',
          start: 2,
          ol: [
            {
              text:
                [
                  { text: 'Application information \n', style: 'header' },

                  { text: 'The understanding of the purpose and the contact points of “application” as used in this document shall be as follows: \n\n' }
                ]
            }
          ],
        },
        applicationInformation,
        {
          pageBreak: 'before',
          start: 3,
          ol: [
            {
              text:
                [
                  { text: 'ZAMBAS R6 information \n', style: 'header' },

                  { text: 'The understanding of the purpose and the contact points of “ZAMBAS R6” as used in this document shall be as follows: \n\n' }
                ]
            }
          ],
        },
        zr6Info,
        {
          pageBreak: 'before',
          start: 4,
          ol: [
            {
              text:
                [
                  { text: 'Used communication channels \n', style: 'header' },

                  { text: 'ZAMBAS R6 basically provides three standard channels of communication with the application. \n\nThe following sections contain the basic configuration information and parameters required to provide access to the ZAMBAS R6 services. As mandatory information no service access or provision can be granted without these details. \n\n' }
                ]
            },
            {
              ol: [

                [{
                  text:
                    [
                      { text: 'Common configuration parameters ', style: 'subheader' },

                      { text: '\n\nFor identification reasons and proper assignment of resources, the application owner is required to use a unique application identifier and a customer identifier as defined below in all calls which require the inclusion of ApplicationID and CustomerID. These identifiers enable ZAMBAS R6 to uniquely identify the application and to provide access to the services and support the reporting process. \n\n' }
                    ]
                },
                {
                  style: 'tableExample',
                  table: {
                    headerRows: 2,
                    widths: [200, '*'],
                    // keepWithHeaderRows: 1,
                    body: [
                      [{ text: 'ApplicationID', style: 'tableHeader' }, channelInfo.applicationId],
                      [{ text: 'CustomerID', style: 'tableHeader' }, channelInfo.customerId],

                    ]
                  }
                },
                  "\n"], {
                  text:
                    [
                      { text: 'Error handling', style: 'subheader' },

                      { text: '\n\nThe service consumer is required to handle errors and exceptions encountered as described in the respective platform and service documentation.\n\n' },


                    ]

                },
                syncChannel,
                asyncChannel,
                batchChannel

              ],
            }
          ],
        },
        {
          pageBreak: 'before',
          start: 5,
          ol: [
            {
              text:
                [
                  { text: 'Service Interfaces \n', style: 'header' },

                  { text: 'This chapter contains the complete list of all services which are provided from ZAMBAS R6 to the application or vice versa. For each service, configurable parameters can be found in this section as well as information about the volume of service usage required for capacity planning.\n\nThe service consumer is required to adhere to the recommended volumes and approximate dis-tribution defined for service consumption for each service. Failure to adhere to these requirements may result in the immediate or delayed rejection of the service.\n\nThe service provider is obliged to provide access to each service as defined by the parameters for its consumption in this SIA. The service provider is also responsible to plan and provide a suffi-cient system capacity to accommodate the volume of service consumption defined for each service. \n\n' }
                ]
            },
            {
              ol: [

                [{
                  text:
                    [
                      { text: 'Synchronous services', style: 'subheader' },

                      { text: '\n\nThis section lists all services provided by ZAMBAS R6 to the application or vice versa over the synchronous channel. \n\n' }
                    ]
                },
                {

                  ol: syncList,
                }
                ],
                [{
                  text:
                    [
                      { text: 'Asynchronous services', style: 'subheader' },

                      { text: '\n\nThis section lists all services provided by ZAMBAS R6 to the application or vice versa over the asynchronous channel. \n\n' }
                    ]
                },
                {

                  ol: asyncList,
                }],
                [{
                  text:
                    [
                      { text: 'Batch services', style: 'subheader' },

                      { text: '\n\nThis section lists all services provided by ZAMBAS R6 to the application or vice versa over the batch channel. \n\n' }
                    ]
                },
                {

                  ol: batchList,
                }]

              ],
            }
          ],
        }
        ,
        {
          pageBreak: 'before',
          start: 6,
          ol: [
            [{
              text:
                [
                  { text: 'ZAMBAS R6 portal \n', style: 'header' },

                  { text: '\nAdvanced reporting and monitoring capabilities are available for the application responsible in the ZAMBAS R6 portal. For detailed information how to access and use portal services refer to the portal documentation.\n\nThe ZAMBAS R6 Portal may be accessed using this web page URL:' },

                  { text: '\nhttp://prod-portal.zambas-r6.passage.fra.dlh.de:20080/portal/page/portal/LH_SOA_PORTAL/Home', color: 'blue', fontSize: 10 },

                  { text: '\n\nThe application is obliged to access and use portal for evaluation of consumed capacity and ser-vice usage behavior. \n\nZAMBAS R6 provides SOA Management Portal services on below defined endpoint.\n\n' }
                ]
            },
            {
              style: 'tableExample',
              table: {
                headerRows: 1,
                widths: [100, 100, 100, 100],
                // keepWithHeaderRows: 1,
                body: [
                  [{ text: 'IP', style: 'tableHeader' }, { text: 'Ports', style: 'tableHeader' }, { text: 'Protocols', style: 'tableHeader' }, { text: 'Description', style: 'tableHeader' }],
                  [{ text: '82.150.238.17\n82.150.238.18\n82.150.238.19' }, { text: '20080\n27777\n29401' }, { text: 'HTTP' }, { text: 'Portal' }],
                  [{ text: '82.150.239.143\n82.150.239.144\n82.150.239.162\n82.150.239.163' }, { text: '38100\n38101' }, { text: 'HTTP' }, { text: 'Oracle AS Console' }],
                  [{ text: '82.150.238.20\n82.150.239.141\n82.150.239.160' }, { text: '1159\n4889' }, { text: 'HTTP' }, { text: 'Oracle GRID Console' }],
                  [{ text: '82.150.238.16' }, { text: '38080' }, { text: 'HTTP' }, { text: 'TIBCO Admin' }],
                  [{ text: '82.150.238.20' }, { text: '24400' }, { text: 'HTTP' }, { text: 'SST Admin' }],
                  [{ text: '82.150.238.20' }, { text: '24040' }, { text: 'HTTP' }, { text: 'LookingGlass' }],
                  [{ text: '82.150.239.151\n82.150.239.156\n82.150.239.170\n82.150.239.175\n' }, { text: '20001' }, { text: 'HTTP' }, { text: 'BEA Console' }],
                  [{ text: '82.150.238.16' }, { text: '21100' }, { text: 'TCP Socket' }, { text: 'FTP Admin' }],
                ]
              }
            }
            ]
          ],
        }
        ,
        {
          pageBreak: 'before',
          start: 7,
          ol: [
            [{
              text:
                [
                  { text: 'Glossary and abbreviations \n\n', style: 'header' },
                ]
            },
            {
              style: 'tableExample',
              border: 0,
              table: {
                headerRows: 1,
                widths: [100, 300],
                // keepWithHeaderRows: 1,
                body: [
                  ['1A', 'Amadeus'],
                  ['CAPI', 'Client Application Programming Interface'],
                  ['FTP', 'File Transfer Protocol'],
                  ['HTTP', 'Hypertext Transfer Protocol'],
                  ['HTTPs', 'Hypertext Transfer Protocol over SSL'],
                  ['JMS', 'Java Messaging Services'],
                  ['KB', 'Kilobytes'],
                  ['MB', 'Megabytes'],
                  ['LH', 'Lufthansa'],
                  ['SFTP', 'Secure File Transfer Protocol'],
                  ['SIA', 'System Interface Agreement'],
                  ['SOA', 'Service Orientated Architecture'],
                  ['SSL', 'Secure Sockets Layer'],
                  ['ZAMBAS', 'Zentrale Architektur Mainframe Basierter Anwendungssysteme'],
                ]
              }
            }
            ]
          ],
        }
        ,
        {
          pageBreak: 'before',
          text:
            [
              { text: 'Appendix A (informational) \n', style: 'header' },

              { text: '\nThe content of this appendix is only for informational purposes and provided to store additional information as required by the parties. The information found in the appendix is in no way binding for ZAMBAS R6 or the application. ' },

            ]
        }
      ],
      styles: {
        title: {
          fontSize: 30,
          bold: true,
          alignment: 'center'
        },
        header: {
          fontSize: 18,
          bold: true
        },
        subheader: {
          fontSize: 15,
          bold: true
        },
        quote: {
          italics: true
        },
        small: {
          fontSize: 8
        },
        tableHeader: {
          bold: true,
          fillColor: 'silver'
        }
      },

      // a string or { width: number, height: number }
      pageSize: 'A4',

      // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
      pageMargins: [40, 60, 40, 60],
      footer: function (currentPage, pageCount) {
        return { text: currentPage.toString() + ' of ' + pageCount + '\t', alignment: 'center', color: 'gray' };

      },


    };

    return dd

  }

  getVolumeControlValue(level: string) {

    let volumeControl = ""
    if (this.volumeJSON) {
      this.volumeJSON.forEach(element => {
        if (element.level == level) {
          volumeControl = element.control
        }
      });
    }
    return volumeControl
  }


}

declare let bootbox: any;
declare let HoldOn: any;

declare let pdfMake: any;