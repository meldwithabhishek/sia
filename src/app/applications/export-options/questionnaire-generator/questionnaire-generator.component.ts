import { Component, OnInit, Input } from '@angular/core';
import { Http, Response } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';
import { ApplicationDetailsService } from '../../application-details/application-details.service';
import { AuthService } from '../../../shared/services/auth.service';

@Component({
  selector: 'app-questionnaire-generator',
  templateUrl: './questionnaire-generator.component.html',
  styleUrls: ['./questionnaire-generator.component.css']
})
export class QuestionnaireGeneratorComponent implements OnInit {

  constructor(private domSanitizer: DomSanitizer, private appServ: ApplicationDetailsService, private authService: AuthService) { }

  @Input('appDetails')
  appDetails: any

  filename : string

  content
  private template: string;

  ngOnInit() {

    this.filename = 

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getQuestionnaireTemplate().subscribe(
      resp => {
        this.template = resp
        this.template = this.template.replace('{{applicationName}}', this.appDetails.name)
        this.template = this.template.replace('{{applicationName}}', this.appDetails.name)
        this.template = this.template.replace('{{applicationDescription}}', this.appDetails.desc)
        this.template = this.template.replace('{{applicationDescription}}', this.appDetails.desc)
        this.template = this.template.replace('{{userName}}', this.authService.getUserName())
        this.template = this.template.replace('{{userName}}', this.authService.getUserName())
        this.template = this.template.replace('{{currentDate}}', new Date().toDateString())
        this.template = this.template.replace('{{currentDate}}', new Date().toDateString())

        let syncServices = "<ul>"
        this.appDetails.syncServices.forEach(element => {
          syncServices = syncServices + "<li>" + element.serviceName + ". " + element.operation + "</li>"          
        });
        syncServices = syncServices + "</ul>"
        this.template = this.template.replace('{{syncServices}}', syncServices)

        this.content = this.domSanitizer.bypassSecurityTrustResourceUrl("data:text/html," +  this.template)
        this.filename = this.appDetails.name + "_Questionnaire_v1.doc"
        HoldOn.close();
      },
      error => {
        console.error(error);
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: "Failed to load Questionnaire Template"
        });

      }
    )   

  }

}

declare let HoldOn: any;
declare let bootbox: any;
