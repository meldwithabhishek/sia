import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireGeneratorComponent } from './questionnaire-generator.component';

describe('QuestionnaireGeneratorComponent', () => {
  let component: QuestionnaireGeneratorComponent;
  let fixture: ComponentFixture<QuestionnaireGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
