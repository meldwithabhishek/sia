import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-export-options',
  templateUrl: './export-options.component.html',
  styleUrls: ['./export-options.component.css']
})
export class ExportOptionsComponent implements OnInit {

  exportOption : any

  @Input('appDetails')
  appDetails :any

  @Input('intro')
  intro :any

  constructor() { }

  ngOnInit() {
  }

}
