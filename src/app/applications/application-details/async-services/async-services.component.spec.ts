import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsyncServicesComponent } from './async-services.component';

describe('AsyncServicesComponent', () => {
  let component: AsyncServicesComponent;
  let fixture: ComponentFixture<AsyncServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsyncServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsyncServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
