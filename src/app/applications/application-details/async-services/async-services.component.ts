import { EMSService } from './../../../shared/models/ems-service-model';
import { async } from '@angular/core/testing';
import { AsyncService } from './../../../shared/models/async-service-model';
import { ApplicationDetailsService } from './../application-details.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AccordionConfig } from 'ngx-bootstrap/accordion';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

export function getAccordionConfig(): AccordionConfig {
  return Object.assign(new AccordionConfig(), { closeOthers: true });
}

@Component({
  selector: 'app-async-services',
  templateUrl: './async-services.component.html',
  styleUrls: ['./async-services.component.css'],
  providers: [AccordionConfig/*{provide: AccordionConfig, useFactory: getAccordionConfig}*/]
})
export class AsyncServicesComponent implements OnDestroy, OnInit, OnChanges {

  editModeOn = false;

  @Input('appName')
  appName: any;

  @Input('airline')
  airline: any;

  @Input("customer")
  customer: string

  @Input('asyncServices')
  asyncServices: any = []

  async = new AsyncService();

  @Input('isEditAllowed')
  isEditAllowed: boolean;

  @Input('baselinedAsync')
  baselinedAsync: AsyncService[];

  @Input('enableDiff')
  enableDiff: boolean

  diffAsync: AsyncService[] = []

  saveType: string = ""

  constructor(private appServ: ApplicationDetailsService, private route: ActivatedRoute, private router: Router) { }

  ngOnDestroy(): void {
    if (this.editModeOn) {
      this.onSave()
    }
  }



  ngOnInit() {

    if (this.baselinedAsync == null || this.baselinedAsync == undefined) {
      this.baselinedAsync = []
    }
    if (this.baselinedAsync && this.asyncServices) {
      this.getDiffDetails()
    }

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.isEditAllowed) {
      this.editModeOn = false
      this.async = new AsyncService()
    }
  }

  enableEdit(asyncEdit) {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      this.async = asyncEdit
      this.editModeOn = true
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )

  }

  addAsync() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      this.editModeOn = true
      this.async = new AsyncService()
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )
  }

  remove(asyncEdit) {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      let result = confirm("Are you sure?")

      if (result) {
        HoldOn.open({
          theme: "sk-cube-grid",
          message: 'Loading...'
        });
        this.appServ.removeAsync(this.appName, this.customer, asyncEdit).subscribe(
          response => {
            HoldOn.close()
            bootbox.alert({
              title: "Success",
              size: "small",
              message: "Successfully removed the Async Service"
            });

            this.asyncServices = response.asyncServices
            this.editModeOn = false

          }, error => {
            HoldOn.close();
            let message = "Failed to update application"
            if (error.errorMessage) {
              message = error.errorMessage
            }
            bootbox.alert({
              title: "Error",
              size: "small",
              message: message
            });
          }
        )
      }
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )

  }


  onDiscard() {
    this.editModeOn = false
    this.async = new AsyncService()
    sessionStorage.setItem("tabNumber", "4");
    window.location.reload()
  }

  setSaveType(saveTyp) {
    this.saveType = saveTyp
  }

  onSave() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      if (this.async.serviceName) {
        this.async.serviceName = this.async.serviceName.trim()
        if (this.async.version) {
          this.async.version = this.async.version.trim()
        }
        HoldOn.open({
          theme: "sk-cube-grid",
          message: 'Loading...'
        });
        this.appServ.updateAsync(this.appName, this.customer, this.async).subscribe(
          response => {
            HoldOn.close()
            bootbox.alert({
              title: "Success",
              size: "small",
              message: "Async Service updated successfully"
            });
            this.asyncServices = response.asyncServices
            if (this.saveType == "saveAndClose") {
              this.editModeOn = false
            }

          }, error => {
            HoldOn.close();
            bootbox.alert({
              title: "Error",
              size: "small",
              message: "Failed to update Async Service"
            });
          }
        )
      }
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )





  }

  addDestinations(serviceName?: any) {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      if (serviceName == undefined) {
        if (this.async.serviceName == undefined || this.async.serviceName == null || this.async.serviceName.trim() == "") {
          bootbox.alert({
            title: "Error",
            size: "small",
            message: "Please provide 'Service Name'"
          });
          return
        } else {
          serviceName = this.async.serviceName
        }
      }
      this.router.navigate(["../../" + "select-async-ems-details/" + this.customer + "|" + this.appName + "|" + serviceName], { relativeTo: this.route });

    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )
  }

  removeDestination(destName, destType, inst, serviceName?) {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      if (serviceName == undefined) {
        serviceName = this.async.serviceName
      }

      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appServ.removeEMSDetails(this.customer, this.appName, serviceName, { emsDestinationName: destName, emsDestinationType: destType, deployedInInstance: inst }).subscribe(
        response => {
          HoldOn.close()
          bootbox.alert({
            title: "Success",
            size: "small",
            message: "Successfully removed '" + destName + "'"
          });
          this.asyncServices = response.asyncServices
          this.editModeOn = false

        }, error => {
          HoldOn.close();
          let message = "Failed to update application"
          if (error.errorMessage) {
            message = error.errorMessage
          }
          bootbox.alert({
            title: "Error",
            size: "small",
            message: message
          });
        }
      )
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )






  }

  getDiffDetails() {

    let tempSyncServices = this.asyncServices

    let l: number = this.asyncServices.length
    if (this.baselinedAsync.length > l) {
      l = this.baselinedAsync.length
    }

    let i = 0, j = 0
    for (; i < this.baselinedAsync.length && j < this.asyncServices.length;) {

      if (this.baselinedAsync[i].serviceName == this.asyncServices[j].serviceName &&
        this.baselinedAsync[i].version == this.asyncServices[j].version) {

        this.diffAsync.push(this.diffSyncServices(this.baselinedAsync[i], this.asyncServices[j]))

        i++
        j++

      } else {
        //Check if match found further down baselined Sync Service.
        let syncIndex = j
        let baseIndex = i + 1
        let matchFound: boolean = false
        while (baseIndex < this.baselinedAsync.length) {
          if (this.baselinedAsync[baseIndex].serviceName == this.asyncServices[syncIndex].serviceName &&
            this.baselinedAsync[baseIndex].version == this.asyncServices[syncIndex].version) {
            //If found this would mean some services were deleted from the baseline
            matchFound = true
            for (let x = i; x < baseIndex; x++) {
              this.diffAsync.push(this.diffSyncServices(this.baselinedAsync[x], new AsyncService()))
            }
            this.diffAsync.push(this.diffSyncServices(this.baselinedAsync[baseIndex], this.asyncServices[syncIndex]))

            i = baseIndex + 1
            j = syncIndex + 1
            break
          } else {
            baseIndex++
          }
        }

        if (!matchFound) {
          //If not found this would mean some services was added new
          this.diffAsync.push(this.diffSyncServices(new AsyncService(), this.asyncServices[syncIndex]))
          j = syncIndex + 1
        }
      }

    }

    if (i < this.baselinedAsync.length) {

      for (let x = i; x < this.baselinedAsync.length; x++) {
        this.diffAsync.push(this.diffSyncServices(this.baselinedAsync[x], new AsyncService()))
        //this.baselinedSync[x] = undefined
      }

    }

    if (j < this.asyncServices.length) {

      for (let x = j; x < this.asyncServices.length; x++) {
        this.diffAsync.push(this.diffSyncServices(new AsyncService(), this.asyncServices[x]))
        //this.syncServices[x] = undefined
      }

    }

    this.asyncServices = tempSyncServices

  }

  diffSyncServices(baseline: AsyncService, newValue: AsyncService): AsyncService {



    let asyncDiff = new AsyncService()

    if (!baseline || !newValue) {
      return asyncDiff
    }

    let changeFlag: boolean = false

    asyncDiff.appRole = this.diff(baseline.appRole, newValue.appRole)
    if (asyncDiff.appRole.includes("<ins>") || asyncDiff.appRole.includes("<del>")) {
      changeFlag = true
    }

    asyncDiff.purposeOfUsage = this.diff(baseline.purposeOfUsage, newValue.purposeOfUsage)
    if (asyncDiff.purposeOfUsage.includes("<ins>") || asyncDiff.purposeOfUsage.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.comments = this.diff(baseline.comments, newValue.comments)
    if (asyncDiff.comments.includes("<ins>") || asyncDiff.comments.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.parameters = this.diff(baseline.parameters, newValue.parameters)
    if (asyncDiff.parameters.includes("<ins>") || asyncDiff.parameters.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.purposeOfUsage = this.diff(baseline.purposeOfUsage, newValue.purposeOfUsage)
    if (asyncDiff.purposeOfUsage.includes("<ins>") || asyncDiff.purposeOfUsage.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.criticality = this.diff(baseline.criticality, newValue.criticality)
    if (asyncDiff.criticality.includes("<ins>") || asyncDiff.criticality.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.workaround = this.diff(baseline.workaround, newValue.workaround)
    if (asyncDiff.workaround.includes("<ins>") || asyncDiff.workaround.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.criticalityJustification = this.diff(baseline.criticalityJustification, newValue.criticalityJustification)
    if (asyncDiff.criticalityJustification.includes("<ins>") || asyncDiff.criticalityJustification.includes("<del>")) {
      changeFlag = true
    }
    if (baseline.dataWorthProtection && newValue.dataWorthProtection) {
      asyncDiff.dataWorthProtection = this.diff(baseline.dataWorthProtection.toString(), newValue.dataWorthProtection.toString())
    } else if (newValue.dataWorthProtection) {
      asyncDiff.dataWorthProtection = this.diff("", newValue.dataWorthProtection.toString())
    } else if (baseline.dataWorthProtection) {
      asyncDiff.dataWorthProtection = this.diff(baseline.dataWorthProtection.toString(), "")
    } else {
      asyncDiff.dataWorthProtection = this.diff("", "")
    }
    if (asyncDiff.dataWorthProtection.toString().includes("<ins>") || asyncDiff.dataWorthProtection.toString().includes("<del>")) {
      changeFlag = true
    }

    asyncDiff.emsDetails = this.getEMSDiff(newValue.emsDetails, baseline.emsDetails)

    if (baseline.serviceName && JSON.stringify(baseline.emsDetails) != JSON.stringify(newValue.emsDetails)) {
      changeFlag = true
    }

    /*
    asyncDiff.emsDestinationType = this.diff(baseline.emsDestinationType, newValue.emsDestinationType)
    if (asyncDiff.emsDestinationType.includes("<del>") || asyncDiff.emsDestinationType.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDestinationName = this.diff(baseline.emsDestinationName, newValue.emsDestinationName)
    if (asyncDiff.emsDestinationName.includes("<del>") || asyncDiff.emsDestinationName.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDestinationProp = this.diff(baseline.emsDestinationProp, newValue.emsDestinationProp)
    if (asyncDiff.emsDestinationProp.includes("<del>") || asyncDiff.emsDestinationProp.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDestinationPurpose = this.diff(baseline.emsDestinationPurpose, newValue.emsDestinationPurpose)
    if (asyncDiff.emsDestinationPurpose.includes("<del>") || asyncDiff.emsDestinationPurpose.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDestinationFullStrategy = this.diff(baseline.emsDestinationFullStrategy, newValue.emsDestinationFullStrategy)
    if (asyncDiff.emsDestinationFullStrategy.includes("<del>") || asyncDiff.emsDestinationFullStrategy.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDurableName = this.diff(baseline.emsDurableName, newValue.emsDurableName)
    if (asyncDiff.emsDurableName.includes("<del>") || asyncDiff.emsDurableName.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDurableProp = this.diff(baseline.emsDurableProp, newValue.emsDurableProp)
    if (asyncDiff.emsDurableProp.includes("<del>") || asyncDiff.emsDurableProp.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsTechUserGroup = this.diff(baseline.emsTechUserGroup, newValue.emsTechUserGroup)
    if (asyncDiff.emsTechUserGroup.includes("<del>") || asyncDiff.emsTechUserGroup.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsTechUserGroup = this.diff(baseline.emsTechUserGroup, newValue.emsTechUserGroup)
    if (asyncDiff.emsTechUserGroup.includes("<del>") || asyncDiff.emsTechUserGroup.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.bridgedDestinationType = this.diff(baseline.bridgedDestinationType, newValue.bridgedDestinationType)
    if (asyncDiff.bridgedDestinationType.includes("<del>") || asyncDiff.bridgedDestinationType.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.bridgedDestinationName = this.diff(baseline.bridgedDestinationName, newValue.bridgedDestinationName)
    if (asyncDiff.bridgedDestinationName.includes("<del>") || asyncDiff.bridgedDestinationName.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.userPermission = this.diff(baseline.userPermission, newValue.userPermission)
    if (asyncDiff.userPermission.includes("<del>") || asyncDiff.userPermission.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.deployedInInstance = this.diff(baseline.deployedInInstance, newValue.deployedInInstance)
    if (asyncDiff.deployedInInstance.includes("<del>") || asyncDiff.deployedInInstance.includes("<del>")) {
      changeFlag = true
    }
    

    


    if (baseline.meanCallsPerDay && newValue.meanCallsPerDay) {
      asyncDiff.meanCallsPerDay = this.diff(baseline.meanCallsPerDay.toString(), newValue.meanCallsPerDay.toString())
    } else if (newValue.meanCallsPerDay) {
      asyncDiff.meanCallsPerDay = this.diff("", newValue.meanCallsPerDay.toString())
    } else if (baseline.meanCallsPerDay) {
      asyncDiff.meanCallsPerDay = this.diff(baseline.meanCallsPerDay.toString(), "")
    } else {
      asyncDiff.meanCallsPerDay = this.diff("", "")
    }
    if (asyncDiff.meanCallsPerDay.toString().includes("<del>") || asyncDiff.meanCallsPerDay.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.peakCallsPerDay && newValue.peakCallsPerDay) {
      asyncDiff.peakCallsPerDay = this.diff(baseline.peakCallsPerDay.toString(), newValue.peakCallsPerDay.toString())
    } else if (newValue.peakCallsPerDay) {
      asyncDiff.peakCallsPerDay = this.diff("", newValue.peakCallsPerDay.toString())
    } else if (baseline.peakCallsPerDay) {
      asyncDiff.peakCallsPerDay = this.diff(baseline.peakCallsPerDay.toString(), "")
    } else {
      asyncDiff.peakCallsPerDay = this.diff("", "")
    }
    if (asyncDiff.peakCallsPerDay.toString().includes("<del>") || asyncDiff.peakCallsPerDay.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.meanCallsPerSec && newValue.meanCallsPerSec) {
      asyncDiff.meanCallsPerSec = this.diff(baseline.meanCallsPerSec.toString(), newValue.meanCallsPerSec.toString())
    } else if (newValue.meanCallsPerSec) {
      asyncDiff.meanCallsPerSec = this.diff("", newValue.meanCallsPerSec.toString())
    } else if (baseline.meanCallsPerSec) {
      asyncDiff.meanCallsPerSec = this.diff(baseline.meanCallsPerSec.toString(), "")
    } else {
      asyncDiff.meanCallsPerSec = this.diff("", "")
    }
    if (asyncDiff.meanCallsPerSec.toString().includes("<del>") || asyncDiff.meanCallsPerSec.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.peakCallsPerSec && newValue.peakCallsPerSec) {
      asyncDiff.peakCallsPerSec = this.diff(baseline.peakCallsPerSec.toString(), newValue.peakCallsPerSec.toString())
    } else if (newValue.peakCallsPerSec) {
      asyncDiff.peakCallsPerSec = this.diff("", newValue.peakCallsPerSec.toString())
    } else if (baseline.peakCallsPerSec) {
      asyncDiff.peakCallsPerSec = this.diff(baseline.peakCallsPerSec.toString(), "")
    } else {
      asyncDiff.peakCallsPerSec = this.diff("", "")
    }
    if (asyncDiff.peakCallsPerSec.toString().includes("<del>") || asyncDiff.peakCallsPerSec.toString().includes("<del>")) {
      changeFlag = true
    }

    asyncDiff.dailyLoadDistributionPeriod = this.diff(baseline.dailyLoadDistributionPeriod, newValue.dailyLoadDistributionPeriod)
    if (asyncDiff.dailyLoadDistributionPeriod.includes("<del>") || asyncDiff.dailyLoadDistributionPeriod.includes("<del>")) {
      changeFlag = true
    }
    */

    if (baseline.serviceName != null && newValue.serviceName == null) {
      asyncDiff.title = "<b><i><font color='#FFA5A2'>" + baseline.serviceName + "</font></i></b>"
      asyncDiff.serviceName = "<del>" + baseline.serviceName + "</del>"
      asyncDiff.version = "<del>" + baseline.version + "</del>"
    } else if (baseline.serviceName == null && newValue.serviceName != null) {
      asyncDiff.title = "<b><i><font color='#B1FCCF'>" + newValue.serviceName + "</font></i></b>"
      asyncDiff.serviceName = "<ins>" + newValue.serviceName + "</ins>"
      asyncDiff.version = "<ins>" + newValue.version + "</ins>"
    } else if (changeFlag) {
      asyncDiff.title = "<b><i><font color='#F5F9A6'>" + newValue.serviceName + "</font></i></b>"
      asyncDiff.serviceName = newValue.serviceName
      asyncDiff.version = this.diff(baseline.version, newValue.version)
    } else {
      asyncDiff.title = newValue.serviceName
      asyncDiff.serviceName = newValue.serviceName
      asyncDiff.version = newValue.version
    }

    return asyncDiff
  }

  getEMSDiff(newEmsDetailsList: EMSService[], baselineEmsDetailsList: EMSService[]): EMSService[] {

    let diffEmsList: EMSService[] = []

    let tempSyncServices = newEmsDetailsList

    let l: number = newEmsDetailsList.length
    if (baselineEmsDetailsList.length > l) {
      l = baselineEmsDetailsList.length
    }

    let i = 0, j = 0
    for (; i < baselineEmsDetailsList.length && j < newEmsDetailsList.length;) {

      if (baselineEmsDetailsList[i].emsDestinationType == newEmsDetailsList[j].emsDestinationType &&
        baselineEmsDetailsList[i].emsDestinationName == newEmsDetailsList[j].emsDestinationName &&
        baselineEmsDetailsList[i].deployedInInstance == newEmsDetailsList[j].deployedInInstance) {

        diffEmsList.push(this.diffEms(baselineEmsDetailsList[i], newEmsDetailsList[j]))

        i++
        j++

      } else {
        //Check if match found further down baselined Sync Service.
        let syncIndex = j
        let baseIndex = i + 1
        let matchFound: boolean = false
        while (baseIndex < baselineEmsDetailsList.length) {
          if (baselineEmsDetailsList[i].emsDestinationType == newEmsDetailsList[j].emsDestinationType &&
            baselineEmsDetailsList[i].emsDestinationName == newEmsDetailsList[j].emsDestinationName &&
            baselineEmsDetailsList[i].deployedInInstance == newEmsDetailsList[j].deployedInInstance) {
            //If found this would mean some services were deleted from the baseline
            matchFound = true
            for (let x = i; x < baseIndex; x++) {
              diffEmsList.push(this.diffEms(baselineEmsDetailsList[x], new EMSService()))
            }
            diffEmsList.push(this.diffEms(baselineEmsDetailsList[baseIndex], newEmsDetailsList[syncIndex]))

            i = baseIndex + 1
            j = syncIndex + 1
            break
          } else {
            baseIndex++
          }
        }

        if (!matchFound) {
          //If not found this would mean some services was added new
          diffEmsList.push(this.diffEms(new EMSService(), newEmsDetailsList[syncIndex]))
          j = syncIndex + 1
        }
      }

    }

    if (i < baselineEmsDetailsList.length) {

      for (let x = i; x < baselineEmsDetailsList.length; x++) {
        diffEmsList.push(this.diffEms(baselineEmsDetailsList[x], new EMSService()))
        //this.baselinedSync[x] = undefined
      }

    }

    if (j < newEmsDetailsList.length) {

      for (let x = j; x < newEmsDetailsList.length; x++) {
        diffEmsList.push(this.diffEms(new EMSService(), newEmsDetailsList[x]))
        //this.syncServices[x] = undefined
      }

    }

    return diffEmsList

  }

  diffEms(baseline: EMSService, newValue: EMSService): EMSService {



    let asyncDiff = new EMSService()

    if (!baseline || !newValue) {
      return asyncDiff
    }

    let changeFlag: boolean = false




    asyncDiff.emsDestinationType = this.diff(baseline.emsDestinationType, newValue.emsDestinationType)
    if (asyncDiff.emsDestinationType.includes("<del>") || asyncDiff.emsDestinationType.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDestinationName = this.diff(baseline.emsDestinationName, newValue.emsDestinationName)
    if (asyncDiff.emsDestinationName.includes("<del>") || asyncDiff.emsDestinationName.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDestinationProp = this.diff(baseline.emsDestinationProp, newValue.emsDestinationProp)
    if (asyncDiff.emsDestinationProp.includes("<del>") || asyncDiff.emsDestinationProp.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDestinationPurpose = this.diff(baseline.emsDestinationPurpose, newValue.emsDestinationPurpose)
    if (asyncDiff.emsDestinationPurpose.includes("<del>") || asyncDiff.emsDestinationPurpose.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDestinationFullStrategy = this.diff(baseline.emsDestinationFullStrategy, newValue.emsDestinationFullStrategy)
    if (asyncDiff.emsDestinationFullStrategy.includes("<del>") || asyncDiff.emsDestinationFullStrategy.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDurableName = this.diff(baseline.emsDurableName, newValue.emsDurableName)
    if (asyncDiff.emsDurableName.includes("<del>") || asyncDiff.emsDurableName.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsDurableProp = this.diff(baseline.emsDurableProp, newValue.emsDurableProp)
    if (asyncDiff.emsDurableProp.includes("<del>") || asyncDiff.emsDurableProp.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsTechUserGroup = this.diff(baseline.emsTechUserGroup, newValue.emsTechUserGroup)
    if (asyncDiff.emsTechUserGroup.includes("<del>") || asyncDiff.emsTechUserGroup.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.emsTechUserGroup = this.diff(baseline.emsTechUserGroup, newValue.emsTechUserGroup)
    if (asyncDiff.emsTechUserGroup.includes("<del>") || asyncDiff.emsTechUserGroup.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.bridgedDestinationType = this.diff(baseline.bridgedDestinationType, newValue.bridgedDestinationType)
    if (asyncDiff.bridgedDestinationType.includes("<del>") || asyncDiff.bridgedDestinationType.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.bridgedDestinationName = this.diff(baseline.bridgedDestinationName, newValue.bridgedDestinationName)
    if (asyncDiff.bridgedDestinationName.includes("<del>") || asyncDiff.bridgedDestinationName.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.userPermission = this.diff(baseline.userPermission, newValue.userPermission)
    if (asyncDiff.userPermission.includes("<del>") || asyncDiff.userPermission.includes("<del>")) {
      changeFlag = true
    }
    asyncDiff.deployedInInstance = this.diff(baseline.deployedInInstance, newValue.deployedInInstance)
    if (asyncDiff.deployedInInstance.includes("<del>") || asyncDiff.deployedInInstance.includes("<del>")) {
      changeFlag = true
    }





    if (baseline.meanCallsPerDay && newValue.meanCallsPerDay) {
      asyncDiff.meanCallsPerDay = this.diff(baseline.meanCallsPerDay.toString(), newValue.meanCallsPerDay.toString())
    } else if (newValue.meanCallsPerDay) {
      asyncDiff.meanCallsPerDay = this.diff("", newValue.meanCallsPerDay.toString())
    } else if (baseline.meanCallsPerDay) {
      asyncDiff.meanCallsPerDay = this.diff(baseline.meanCallsPerDay.toString(), "")
    } else {
      asyncDiff.meanCallsPerDay = this.diff("", "")
    }
    if (asyncDiff.meanCallsPerDay.toString().includes("<del>") || asyncDiff.meanCallsPerDay.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.peakCallsPerDay && newValue.peakCallsPerDay) {
      asyncDiff.peakCallsPerDay = this.diff(baseline.peakCallsPerDay.toString(), newValue.peakCallsPerDay.toString())
    } else if (newValue.peakCallsPerDay) {
      asyncDiff.peakCallsPerDay = this.diff("", newValue.peakCallsPerDay.toString())
    } else if (baseline.peakCallsPerDay) {
      asyncDiff.peakCallsPerDay = this.diff(baseline.peakCallsPerDay.toString(), "")
    } else {
      asyncDiff.peakCallsPerDay = this.diff("", "")
    }
    if (asyncDiff.peakCallsPerDay.toString().includes("<del>") || asyncDiff.peakCallsPerDay.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.meanCallsPerSec && newValue.meanCallsPerSec) {
      asyncDiff.meanCallsPerSec = this.diff(baseline.meanCallsPerSec.toString(), newValue.meanCallsPerSec.toString())
    } else if (newValue.meanCallsPerSec) {
      asyncDiff.meanCallsPerSec = this.diff("", newValue.meanCallsPerSec.toString())
    } else if (baseline.meanCallsPerSec) {
      asyncDiff.meanCallsPerSec = this.diff(baseline.meanCallsPerSec.toString(), "")
    } else {
      asyncDiff.meanCallsPerSec = this.diff("", "")
    }
    if (asyncDiff.meanCallsPerSec.toString().includes("<del>") || asyncDiff.meanCallsPerSec.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.peakCallsPerSec && newValue.peakCallsPerSec) {
      asyncDiff.peakCallsPerSec = this.diff(baseline.peakCallsPerSec.toString(), newValue.peakCallsPerSec.toString())
    } else if (newValue.peakCallsPerSec) {
      asyncDiff.peakCallsPerSec = this.diff("", newValue.peakCallsPerSec.toString())
    } else if (baseline.peakCallsPerSec) {
      asyncDiff.peakCallsPerSec = this.diff(baseline.peakCallsPerSec.toString(), "")
    } else {
      asyncDiff.peakCallsPerSec = this.diff("", "")
    }
    if (asyncDiff.peakCallsPerSec.toString().includes("<del>") || asyncDiff.peakCallsPerSec.toString().includes("<del>")) {
      changeFlag = true
    }

    asyncDiff.dailyLoadDistributionPeriod = this.diff(baseline.dailyLoadDistributionPeriod, newValue.dailyLoadDistributionPeriod)
    if (asyncDiff.dailyLoadDistributionPeriod.includes("<del>") || asyncDiff.dailyLoadDistributionPeriod.includes("<del>")) {
      changeFlag = true
    }

    return asyncDiff
  }

  diff(str1: string, str2: string): string {
    //return JsDiff.diffChars(str1,"str2")
    if (str1 == null || str1 == undefined) {
      str1 = ""
    } else {
      while (str1.includes("<")) {
        str1 = str1.replace("<", "&lt;")
      }
      while (str1.includes(">")) {
        str1 = str1.replace(">", "&gt;")
      }
    }
    if (str2 == null || str2 == undefined) {
      str2 = ""
    } else {
      while (str2.includes("<")) {
        str2 = str2.replace("<", "&lt;")
      }
      while (str1.includes(">")) {
        str1 = str1.replace(">", "&gt;")
      }
    }
    return diff(str1, str2)

  }

}

declare let bootbox: any;
declare let HoldOn: any;

const diff = require('rich-text-diff')
