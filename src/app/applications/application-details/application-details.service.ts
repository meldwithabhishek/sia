import { AuthService } from './../../shared/services/auth.service';
import { ConfigurationService } from './../../shared/services/configuration.service';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { Observable } from "rxjs/Observable";

@Injectable()
export class ApplicationDetailsService {

  private _url_app_name: string = "assets/json/info-text.json";
  private _url_airline: string = "assets/json/carrier.json";
  private _url_volume_control: string = "assets/json/volumeControl.json";

  private userName

  constructor(private _http: Http, private config: ConfigurationService, private authService: AuthService) {
    this.userName = this.authService.getUserName();
  }

  getAirlinesList() {
    return this._http.get(this._url_airline)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));;

  }

  getVolumeControlJSON() {
    return this._http.get(this._url_volume_control)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));;

  }

  getApplicationDetails(customer: string, appName: string, version: string) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "ApplicationsFinal/" + customer + "/" + appName + "/" + version)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));;
  }

  getApplicationsForAirline(airlineName) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "ApplicationsFinal?airline=" + airlineName)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));

  }

  getApplicationsPendingApprovalForAirline(airlineName) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "UnapprovedApplications?airline=" + airlineName)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));

  }

  getApplicationsPendingApproval() {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "UnapprovedApplications")
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));

  }

  getDraftApplicationDetails(customer: string, appName: string) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "ApplicationsDraft/" + customer + "/" + appName)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));;
  }

  getLatestBaselinedSIA(appName: string, customer: string) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "GetLatestApplications/" + customer + "/" + appName)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));;
  }

  getDraftApplicationsForAirline(airlineName) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "ApplicationsDraft?airline=" + airlineName)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));

  }

  getComments(appName, customer) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "ReviewComments/" + customer + "/" + appName)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));

  }

  getUniqueApplications() {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "GetUniqueApplications")
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));

  }

  getUniqueApplicationsForAirlne(airlineName) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "GetUniqueApplications?airline=" + airlineName)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));

  }

  getApplicationIntro() {
    return this._http.get(this._url_app_name)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));;
  }

  updateApplicationDetails(appName: string, customer: string, appDetails) {
    return this._http.put(this.config.getConfig().SIA_WEB_APP + "ApplicationsDraft/" + customer + "/" + appName, appDetails, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  submitForReview(appName: string, customer: string, param) {
    return this._http.post(this.config.getConfig().SIA_WEB_APP + "SubmitForReview/" + customer + "/" + appName, param, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  approve(appName: string, customer: string) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "ApproveDraft/" + customer + "/" + appName, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  returnToOwner(appName: string, customer: string, param) {
    return this._http.post(this.config.getConfig().SIA_WEB_APP + "ReturnToOwner/" + customer + "/" + appName, param, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }


  createApplication(appDetails) {
    return this._http.post(this.config.getConfig().SIA_WEB_APP + "ApplicationsDraft", appDetails, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  updateChannelInfo(appName: string, customer: string, chnlInfo) {
    return this._http.put(this.config.getConfig().SIA_WEB_APP + "ChannelInfo/" + customer + "/" + appName, chnlInfo, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  updateAsync(appName: string, customer: string, async) {
    return this._http.post(this.config.getConfig().SIA_WEB_APP + "AsyncService/" + customer + "/" + appName, async, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  removeAsync(appName: string, customer: string, async) {
    return this._http.delete(this.config.getConfig().SIA_WEB_APP + "AsyncService/" + customer + "/" + appName + "/" + async.serviceName, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  addSync(appName: string, customer: string, sync) {
    return this._http.post(this.config.getConfig().SIA_WEB_APP + "SyncService/" + customer + "/" + appName, sync, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  updateSync(appName: string, customer: string, sync) {
    return this._http.put(this.config.getConfig().SIA_WEB_APP + "SyncService/" + customer + "/" + appName, sync, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  removeSync(appName: string, customer: string, sync) {
    let body = JSON.stringify(sync)
    let headers = this.headers().headers
    let options = new RequestOptions({
      headers: headers,
      body: body
    })
    return this._http.delete(this.config.getConfig().SIA_WEB_APP + "SyncService/" + customer + "/" + appName, options)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  updateBatch(appName: string, customer: string, batch) {
    return this._http.post(this.config.getConfig().SIA_WEB_APP + "BatchService/" + customer + "/" + appName, batch, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  removeBatch(appName: string, customer: string, batch) {
    return this._http.delete(this.config.getConfig().SIA_WEB_APP + "BatchService/" + customer + "/" + appName + "/" + batch.serviceName, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  unlockApp(appName: string, customer: string) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "UnlockApplication/" + customer + "/" + appName, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  lockApp(appName: string, customer: string) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "LockApplication/" + customer + "/" + appName, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  removeApp(appName: string, customer: string) {
    return this._http.delete(this.config.getConfig().SIA_WEB_APP + "ApplicationsDraft/" + customer + "/" + appName, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  getServiceCatalogue() {
    return this._http.get(this.config.getConfig().SERV_CAT_TST + "SyncServiceCatalogue", this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  getBatchServiceCatalogue() {
    return this._http.get(this.config.getConfig().SERV_CAT + "BatchServiceCatalogue", this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  getAsyncServiceCatalogue(env: string) {

    let url = this.config.getConfig().SERV_CAT_TST + "AsyncServiceCatalogue/" + env
    if (env == "PROD") {
      url = this.config.getConfig().SERV_CAT + "AsyncServiceCatalogue/" + env
    }

    return this._http.get(url, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  deleteBatchServiceCatalogue(param) {
    let body = JSON.stringify(param)
    let headers = this.headers().headers
    let options = new RequestOptions({
      headers: headers,
      body: body
    })
    return this._http.delete(this.config.getConfig().SERV_CAT + "BatchServiceCatalogue", options)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  getFilenames(appDetails: any) {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "File/" + appDetails.name + "/" + appDetails.airlineName, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  deleteFile(filename: string, appName: string, airline: string) {
    return this._http.delete(this.config.getConfig().SIA_WEB_APP + "File/" + appName + "/" + airline + "/" + filename, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }


  /*********** EMS Catalogue **********/

  getAllDestinations(env: string) {

    let url = this.config.getConfig().EMS_CAT_TST + "Destination/" + env
    if (env == "PRD") {
      url = this.config.getConfig().EMS_CAT + "Destination/" + env
    }

    return this._http.get(url, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  getAllDurables(env: string) {

    let url = this.config.getConfig().EMS_CAT_TST + "Durable/" + env
    if (env == "PRD") {
      url = this.config.getConfig().EMS_CAT + "Durable/" + env
    }

    return this._http.get(url, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  getAllBridges(env: string) {

    let url = this.config.getConfig().EMS_CAT_TST + "Bridge/" + env
    if (env == "PRD") {
      url = this.config.getConfig().EMS_CAT + "Bridge/" + env
    }

    return this._http.get(url, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  getAllTechUsers(env: string) {

    let url = this.config.getConfig().EMS_CAT_TST + "TechUser/" + env
    if (env == "PRD") {
      url = this.config.getConfig().EMS_CAT + "TechUser/" + env
    }

    return this._http.get(url, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()))
  }

  addEMSDetails(carrier: string, applicationName: string, serviceName: string, param) {
    return this._http.post(this.config.getConfig().SIA_WEB_APP + "AsynServiceEMS/" + carrier + "/" + applicationName + "/" + serviceName, param, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));

  }

  removeEMSDetails(carrier: string, applicationName: string, serviceName: string, param) {
    let body = JSON.stringify(param)
    let headers = this.headers().headers
    let options = new RequestOptions({
      headers: headers,
      body: body
    })

    return this._http.delete(this.config.getConfig().SIA_WEB_APP + "AsynServiceEMS/" + carrier + "/" + applicationName + "/" + serviceName, options)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));

  }


  /************************************/

  getQuestionnaireTemplate() {
    return this._http.get('assets/questionairre.html')
      .map((response: Response) => response.text() )
      .catch(error => Observable.throw(error.json()))
  }


  headers() {
    let options = new RequestOptions();
    let headers = new Headers();
    headers.set('Content-Type', 'application/json');
    headers.set('Requester', this.userName);
    options.headers = headers;
    return options;
  }

}

declare let HoldOn: any;
