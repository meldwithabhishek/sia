import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationDetailsService } from './../application-details.service';
import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-app-details',
  templateUrl: './app-details.component.html',
  styleUrls: ['./app-details.component.css']
})
export class AppDetailsComponent implements OnInit, OnChanges, OnDestroy {


  editModeOn = false;

  @Input('appName')
  appName: any;

  @Input('appDetails')
  appDetails: any;

  @Input('baselinedAppDetails')
  baselinedAppDetails: any;

  appDetailsBackup: any;

  @Input('isEditAllowed')
  isEditAllowed: boolean;

  @Input('enableDiff')
  enableDiff: boolean

  @Output() onAppChange = new EventEmitter<any>()


  constructor(private appServ: ApplicationDetailsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

  }

  ngOnDestroy(): void {

    if (this.editModeOn) {

      var result = confirm("Would you like to save you changes to 'Application Details' before leaving this page?");

      if (result) {
        this.onSave()
      }
    }

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.isEditAllowed) {
      this.onDiscard()
    }
  }


  enableEdit() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.appDetails.airlineName).subscribe(resp => {
      /*this.appDetails = resp;
      this.airlineName = this.appDetails.airlineName;
      this.isEditAllowed = true;
      this.isEditAllowedForApplication()
      this.getAppList();*/
      HoldOn.close();
      this.appDetailsBackup = JSON.stringify(this.appDetails)
      this.editModeOn = true
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
        this.editModeOn = false
      }
    )

  }

  onDiscard() {
    if (this.appDetailsBackup) {
      this.appDetails = JSON.parse(this.appDetailsBackup)
    }
    this.editModeOn = false
  }

  diff(str1: string, str2: string) {
    //return JsDiff.diffChars(str1,"str2")
    if (str1 == null || str1 == undefined) {
      str1 = ""
    } else {
      while (str1.includes("<")) {
        str1 = str1.replace("<", "&lt;")
      }
    }
    if (str2 == null || str2 == undefined) {
      str2 = ""
    } else {
      while (str2.includes("<")) {
        str2 = str2.replace("<", "&lt;")
      }
    }
    return diff(str1, str2)

  }

  checkNum() {
    let parts: Array<any> = this.appDetails.version.split(".")
    let ver = ""
    parts.forEach(
      element => {
        if (element == "0" || element == "1" || element == "2" || element == "3" || element == "4"
          || element == "5" || element == "6" || element == "7" || element == "8" || element == "9") {
          ver = ver + element + "."
        } else {
          ver = ver + 0 + "."
        }
      }
    )
    ver = ver.substring(0, ver.length - 1)
    this.appDetails.version = ver
  }

  onSave() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.appDetails.airlineName).subscribe(resp => {

      HoldOn.close();

      var updatedAppDetails = this.appDetails
      updatedAppDetails.channelInfo = null
      updatedAppDetails.syncServices = null
      updatedAppDetails.asyncServices = null
      updatedAppDetails.batchServices = null

      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appServ.updateApplicationDetails(this.appName, this.appDetails.airlineName, updatedAppDetails).subscribe(
        response => {
          HoldOn.close()
          bootbox.alert({
            title: "Success",
            size: "small",
            message: "Application Details updated successfully"
          });
          this.editModeOn = false
          this.appDetails = response
          this.onAppChange.emit(this.appDetails)

        }, error => {
          HoldOn.close();
          let message = "Failed to update Application Details"
          if (error.errorMessage) {
            message = error.errorMessage
          }
          bootbox.alert({
            title: "Error",
            size: "small",
            message: message
          });
        }
      )


    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        /*bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
        this.editModeOn = false*/
        window.location.reload()
      }
    )
  }

}

declare let bootbox: any;
declare let HoldOn: any;
declare let JsDiff: any

const diff = require('rich-text-diff')