import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationDetailsService } from './../application-details.service';
import { BatchService } from './../../../shared/models/batch-service-model';
import { AccordionConfig } from 'ngx-bootstrap/accordion';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

export function getAccordionConfig(): AccordionConfig {
  return Object.assign(new AccordionConfig(), { closeOthers: true });
}

@Component({
  selector: 'app-batch-services',
  templateUrl: './batch-services.component.html',
  styleUrls: ['./batch-services.component.css'],
  providers: [AccordionConfig/*{provide: AccordionConfig, useFactory: getAccordionConfig}*/]
})
export class BatchServicesComponent implements OnDestroy, OnInit, OnChanges {

  editModeOn = false;

  @Input('appName')
  appName: any;

  @Input("customer")
  customer: string

  @Input('airline')
  airline: any;

  @Input('batchServices')
  batchServices: any

  batchServ = new BatchService()

  @Input('isEditAllowed')
  isEditAllowed: boolean;

  @Input('baselinedBatch')
  baselinedBatch: BatchService[];

  @Input('enableDiff')
  enableDiff: boolean

  diffBatch: BatchService[] = []

  saveType: string = ""

  constructor(private appServ: ApplicationDetailsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    if (this.baselinedBatch == null || this.baselinedBatch == undefined) {
      this.baselinedBatch = []
    }
    if (this.baselinedBatch && this.batchServices) {
      this.getDiffDetails()
    }

  }

  ngOnDestroy(): void {
    if (this.editModeOn) {

      var result = confirm("Would you like to save you changes to 'Batch Services' before leaving this page?");

      if (result) {
        this.onSave()
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.isEditAllowed) {
      this.editModeOn = false
      this.batchServ = new BatchService()
    }
  }

  enableEdit(batchEdit) {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      this.batchServ = batchEdit
      this.editModeOn = true
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )
  }

  addBatch() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      this.editModeOn = true
      this.batchServ = new BatchService()
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )

  }

  remove(batchEdit) {


    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      let result = confirm("Are you sure?")

      if (result) {
        HoldOn.open({
          theme: "sk-cube-grid",
          message: 'Loading...'
        });
        this.appServ.removeBatch(this.appName, this.customer, batchEdit).subscribe(
          response => {
            HoldOn.close()
            bootbox.alert({
              title: "Success",
              size: "small",
              message: "Successfully removed the Batch Service"
            });

            this.batchServices = response.batchServices
            this.editModeOn = false

          }, error => {
            HoldOn.close();
            let message = "Failed to update application"
            if (error.errorMessage) {
              message = error.errorMessage
            }
            bootbox.alert({
              title: "Error",
              size: "small",
              message: message
            });
          }
        )
      }
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )

  }

  onDiscard() {
    this.editModeOn = false
    this.batchServ = new BatchService()
    sessionStorage.setItem("tabNumber","5");
    window.location.reload()
  }

  setSaveType(saveTyp) {
    this.saveType = saveTyp
  }

  onSave() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      this.batchServ.serviceName = this.batchServ.serviceName.trim()
      if (this.batchServ.version) {
        this.batchServ.version = this.batchServ.version.trim()
      }

      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appServ.updateBatch(this.appName, this.customer, this.batchServ).subscribe(
        response => {
          HoldOn.close()
          bootbox.alert({
            title: "Success",
            size: "small",
            message: "Batch Service updated successfully"
          });
          this.batchServices = response.batchServices
          if (this.saveType == "saveAndClose") {
            this.editModeOn = false
          }

        }, error => {
          HoldOn.close();
          let message = "Failed to update Batch Service"
          if (error.errorMessage) {
            message = error.errorMessage
          }
          bootbox.alert({
            title: "Error",
            size: "small",
            message: message
          });
        }
      )
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )



  }


  getDiffDetails() {

    let tempbatchServices = this.batchServices

    let l: number = this.batchServices.length
    if (this.baselinedBatch.length > l) {
      l = this.baselinedBatch.length
    }

    let i = 0, j = 0
    for (; i < this.baselinedBatch.length && j < this.batchServices.length;) {

      if (this.baselinedBatch[i].serviceName == this.batchServices[j].serviceName &&
        this.baselinedBatch[i].version == this.batchServices[j].version) {

        this.diffBatch.push(this.diffbatchServices(this.baselinedBatch[i], this.batchServices[j]))

        i++
        j++

      } else {
        //Check if match found further down baselined Sync Service.
        let syncIndex = j
        let baseIndex = i + 1
        let matchFound: boolean = false
        while (baseIndex < this.baselinedBatch.length) {
          if (this.baselinedBatch[baseIndex].serviceName == this.batchServices[syncIndex].serviceName &&
            this.baselinedBatch[baseIndex].version == this.batchServices[syncIndex].version) {
            //If found this would mean some services were deleted from the baseline
            matchFound = true
            for (let x = i; x < baseIndex; x++) {
              this.diffBatch.push(this.diffbatchServices(this.baselinedBatch[x], new BatchService()))
            }
            this.diffBatch.push(this.diffbatchServices(this.baselinedBatch[baseIndex], this.batchServices[syncIndex]))

            i = baseIndex + 1
            j = syncIndex + 1
            break
          } else {
            baseIndex++
          }
        }

        if (!matchFound) {
          //If not found this would mean some services was added new
          this.diffBatch.push(this.diffbatchServices(new BatchService(), this.batchServices[syncIndex]))
          j = syncIndex + 1
        }
      }

    }
    
    if (i < this.baselinedBatch.length) {

      for (let x = i; x < this.baselinedBatch.length; x++) {
        this.diffBatch.push(this.diffbatchServices(this.baselinedBatch[x], new BatchService()))
        //this.baselinedSync[x] = undefined
      }

    }

    if (j < this.batchServices.length) {

      for (let x = j; x < this.batchServices.length; x++) {
        this.diffBatch.push(this.diffbatchServices(new BatchService(), this.batchServices[x]))
        //this.batchServices[x] = undefined
      }

    }

    this.batchServices = tempbatchServices

  }

  diffbatchServices(baseline: BatchService, newValue: BatchService): BatchService {

    let batchDiff = new BatchService()

    if (!baseline || !newValue) {
      return batchDiff
    }

    let changeFlag: boolean = false

    let base = ""
    if (baseline.appRole == 'Consumer') {
      base = 'Downloads from ZR6'
    } else if (baseline.appRole == 'Producer') {
      base = 'Uploads to ZR6'
    }
    let newVal = ""
    if (newValue.appRole == 'Consumer') {
      newVal = 'Downloads from ZR6'
    } else if (newValue.appRole == 'Producer') {
      newVal = 'Uploads to ZR6'
    }
    batchDiff.appRole = this.diff(base, newVal)
    if (batchDiff.appRole.includes("<del>") || batchDiff.appRole.includes("<del>")) {
      changeFlag = true
    }

    batchDiff.connectionInitiatedBy = this.diff(baseline.connectionInitiatedBy, newValue.connectionInitiatedBy)
    if (batchDiff.connectionInitiatedBy.includes("<del>") || batchDiff.connectionInitiatedBy.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.comments = this.diff(baseline.comments, newValue.comments)
    if (batchDiff.comments.includes("<del>") || batchDiff.comments.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.ipAddPort = this.diff(baseline.ipAddPort, newValue.ipAddPort)
    if (batchDiff.ipAddPort.includes("<del>") || batchDiff.ipAddPort.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.protocolUsed = this.diff(baseline.protocolUsed, newValue.protocolUsed)
    if (batchDiff.protocolUsed.includes("<del>") || batchDiff.protocolUsed.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.purposeOfUsage = this.diff(baseline.purposeOfUsage, newValue.purposeOfUsage)
    if (batchDiff.purposeOfUsage.includes("<del>") || batchDiff.purposeOfUsage.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.criticality = this.diff(baseline.criticality, newValue.criticality)
    if (batchDiff.criticality.includes("<del>") || batchDiff.criticality.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.workaround = this.diff(baseline.workaround, newValue.workaround)
    if (batchDiff.workaround.includes("<del>") || batchDiff.workaround.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.criticalityJustification = this.diff(baseline.criticalityJustification, newValue.criticalityJustification)
    if (batchDiff.criticalityJustification.includes("<del>") || batchDiff.criticalityJustification.includes("<del>")) {
      changeFlag = true
    }
    if (baseline.dataWorthProtection && newValue.dataWorthProtection) {
      batchDiff.dataWorthProtection = this.diff(baseline.dataWorthProtection.toString(), newValue.dataWorthProtection.toString())
    } else if (newValue.dataWorthProtection) {
      batchDiff.dataWorthProtection = this.diff("", newValue.dataWorthProtection.toString())
    } else if (baseline.dataWorthProtection) {
      batchDiff.dataWorthProtection = this.diff(baseline.dataWorthProtection.toString(), "")
    } else {
      batchDiff.dataWorthProtection = this.diff("", "")
    }
    if (batchDiff.dataWorthProtection.toString().includes("<del>") || batchDiff.dataWorthProtection.toString().includes("<del>")) {
      changeFlag = true
    }


    batchDiff.ipAddPort = this.diff(baseline.ipAddPort, newValue.ipAddPort)
    if (batchDiff.ipAddPort.includes("<del>") || batchDiff.ipAddPort.includes("<del>")) {
      changeFlag = true
    }

    batchDiff.filenamePatternZR6 = this.diff(baseline.filenamePatternZR6, newValue.filenamePatternZR6)
    if (batchDiff.filenamePatternZR6.includes("<del>") || batchDiff.filenamePatternZR6.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.filenameLocationZR6 = this.diff(baseline.filenameLocationZR6, newValue.filenameLocationZR6)
    if (batchDiff.filenameLocationZR6.includes("<del>") || batchDiff.filenameLocationZR6.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.filenamePatternApp = this.diff(baseline.filenamePatternApp, newValue.filenamePatternApp)
    if (batchDiff.filenamePatternApp.includes("<del>") || batchDiff.filenamePatternApp.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.filenameLocationApp = this.diff(baseline.filenameLocationApp, newValue.filenameLocationApp)
    if (batchDiff.filenameLocationApp.includes("<del>") || batchDiff.filenameLocationApp.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.fileTransferDirection = this.diff(baseline.fileTransferDirection, newValue.fileTransferDirection)
    if (batchDiff.fileTransferDirection.includes("<del>") || batchDiff.fileTransferDirection.includes("<del>")) {
      changeFlag = true
    }


    if (baseline.fileRetentionZR6 && newValue.fileRetentionZR6) {
      batchDiff.fileRetentionZR6 = this.diff(baseline.fileRetentionZR6.toString(), newValue.fileRetentionZR6.toString())
    } else if (newValue.fileRetentionZR6) {
      batchDiff.fileRetentionZR6 = this.diff("", newValue.fileRetentionZR6.toString())
    } else if (baseline.fileRetentionZR6) {
      batchDiff.fileRetentionZR6 = this.diff(baseline.fileRetentionZR6.toString(), "")
    } else {
      batchDiff.fileRetentionZR6 = this.diff("", "")
    }
    if (batchDiff.fileRetentionZR6.toString().includes("<del>") || batchDiff.fileRetentionZR6.toString().includes("<ins>")) {
      changeFlag = true
    }

    if (baseline.internetAccess && newValue.internetAccess) {
      batchDiff.internetAccess = this.diff(baseline.internetAccess.toString(), newValue.internetAccess.toString())
    } else if (newValue.internetAccess) {
      batchDiff.internetAccess = this.diff("", newValue.internetAccess.toString())
    } else if (baseline.internetAccess) {
      batchDiff.internetAccess = this.diff(baseline.internetAccess.toString(), "")
    } else {
      batchDiff.internetAccess = this.diff("", "")
    }
    if (batchDiff.internetAccess.toString().includes("<del>") || batchDiff.internetAccess.toString().includes("<del>")) {
      changeFlag = true
    }

    batchDiff.meanFileSize = this.diff(baseline.meanFileSize, newValue.meanFileSize)
    if (batchDiff.meanFileSize.includes("<del>") || batchDiff.meanFileSize.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.maxFileSize = this.diff(baseline.maxFileSize, newValue.maxFileSize)
    if (batchDiff.maxFileSize.includes("<del>") || batchDiff.maxFileSize.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.meanFileTransferPerMonth = this.diff(baseline.meanFileTransferPerMonth, newValue.meanFileTransferPerMonth)
    if (batchDiff.meanFileTransferPerMonth.includes("<del>") || batchDiff.meanFileTransferPerMonth.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.maxFileTransferPerMonth = this.diff(baseline.maxFileTransferPerMonth, newValue.maxFileTransferPerMonth)
    if (batchDiff.maxFileTransferPerMonth.includes("<del>") || batchDiff.maxFileTransferPerMonth.includes("<del>")) {
      changeFlag = true
    }
    batchDiff.ftpTransferDist = this.diff(baseline.ftpTransferDist, newValue.ftpTransferDist)
    if (batchDiff.ftpTransferDist.includes("<del>") || batchDiff.ftpTransferDist.includes("<del>")) {
      changeFlag = true
    }

    if (baseline.serviceName != null && newValue.serviceName == null) {
      batchDiff.title = "<b><i><font color='#FFA5A2'>" + baseline.serviceName + "</font></i></b>"
      batchDiff.serviceName = "<del>" + baseline.serviceName + "</del>"
      batchDiff.version = "<del>" + baseline.version + "</del>"
    } else if (baseline.serviceName == null && newValue.serviceName != null) {
      batchDiff.title = "<b><i><font color='#B1FCCF'>" + newValue.serviceName + "</font></i></b>"
      batchDiff.serviceName = "<ins>" + newValue.serviceName + "</ins>"
      batchDiff.version = "<ins>" + newValue.version + "</ins>"
    } else if (changeFlag) {
      batchDiff.title = "<b><i><font color='#F5F9A6'>" + newValue.serviceName + "</font></i></b>"
      batchDiff.serviceName = newValue.serviceName
      batchDiff.version = this.diff(baseline.version, newValue.version)
    } else {
      batchDiff.title = newValue.serviceName
      batchDiff.serviceName = newValue.serviceName
      batchDiff.version = newValue.version
    }


    return batchDiff
  }

  diff(str1: string, str2: string): string {
    //return JsDiff.diffChars(str1,"str2")

    if (str1 == null || str1 == undefined) {
      str1 = ""
    } else {
      while (str1.includes("<")) {
        str1 = str1.replace("<", "&lt;")
      }
      while (str1.includes(">")) {
        str1 = str1.replace(">", "&gt;")
      }
    }
    if (str2 == null || str2 == undefined) {
      str2 = ""
    } else {
      while (str2.includes("<")) {
        str2 = str2.replace("<", "&lt;")
      }
      while (str1.includes(">")) {
        str1 = str1.replace(">", "&gt;")
      }
    }
    return diff(str1, str2)

  }

}


declare let bootbox: any;
declare let HoldOn: any;

const diff = require('rich-text-diff')