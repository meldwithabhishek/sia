import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { SyncServiceCat } from './../../../shared/models/sync-serv-cat-model';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationDetailsService } from './../application-details.service';
import { SyncService } from './../../../shared/models/sync-service-model';
import { AccordionConfig } from 'ngx-bootstrap/accordion';
import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

export function getAccordionConfig(): AccordionConfig {
  return Object.assign(new AccordionConfig(), { closeOthers: true });
}

@Component({
  selector: 'app-sync-services',
  templateUrl: './sync-services.component.html',
  styleUrls: ['./sync-services.component.css'],
  providers: [AccordionConfig/*{provide: AccordionConfig, useFactory: getAccordionConfig}*/]
})
export class SyncServicesComponent implements OnDestroy, OnInit, OnChanges {

  editModeOn = false;

  @Input('appName')
  appName: any;

  @Input('airline')
  airline: any;

  @Input("customer")
  customer: string

  @Input('syncServices')
  syncServices: any[] = [];

  sync = new SyncService();

  @Input('isEditAllowed')
  isEditAllowed: boolean;

  @Input('baselinedSync')
  baselinedSync: SyncService[];

  @Input('enableDiff')
  enableDiff: boolean

  diffSync: SyncService[] = []

  saveType: string = ""

  constructor(private appServ: ApplicationDetailsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    if (this.baselinedSync == null || this.baselinedSync == undefined) {
      this.baselinedSync = []
    }
    if (this.baselinedSync && this.syncServices) {
      this.getDiffDetails()
    }

  }

  ngOnDestroy(): void {
    if (this.editModeOn) {

      var result = confirm("Would you like to save you changes to 'Sync Services' before leaving this page?");

      if (result) {
        this.onSave()
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.isEditAllowed) {
      this.editModeOn = false
      this.sync = new SyncService()
    }
  }

  enableEdit(syncEdit) {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      this.sync = syncEdit
      //this.sync.syncCat = syncEdit.syncCat
      this.editModeOn = true
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
        this.editModeOn = false
      }
    )
  }

  addSync() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      this.router.navigate(["../../" + "sync-catalogue/" + this.customer + "|" + this.appName], { relativeTo: this.route });

    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();  
        window.location.reload()
      }
    )

  }

  remove(syncEdit) {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();
      var result = confirm("Are you sure?");

      if (result) {
        HoldOn.open({
          theme: "sk-cube-grid",
          message: 'Loading...'
        });
        this.appServ.removeSync(this.appName, this.customer, syncEdit).subscribe(
          response => {
            HoldOn.close()
            bootbox.alert({
              title: "Success",
              size: "small",
              message: "Successfully removed the Sync Service"
            });
            this.syncServices = response.syncServices
            this.editModeOn = false
          }, error => {
            HoldOn.close();
            let message = "Failed to update application"
            if (error.errorMessage) {
              message = error.errorMessage
            }
            bootbox.alert({
              title: "Error",
              size: "small",
              message: message
            });
          }
        )
      }
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        window.location.reload()
      }
    )


  }

  onDiscard() {
    this.editModeOn = false
    this.sync = new SyncService()
    sessionStorage.setItem("tabNumber","3");
    window.location.reload()
  }

  setSaveType(saveTyp) {
    this.saveType = saveTyp
  }

  onSave() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();

      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appServ.updateSync(this.appName, this.customer, this.sync).subscribe(
        response => {
          HoldOn.close()
          bootbox.alert({
            title: "Success",
            size: "small",
            message: "Sync Service updated successfully"
          });
          this.syncServices = response.syncServices
          if (this.saveType == "saveAndClose") {
            this.editModeOn = false
          }

        }, error => {
          HoldOn.close();
          let message = "Failed to update Sync Service"
          if (error.errorMessage) {
            message = error.errorMessage
          }
          bootbox.alert({
            title: "Error",
            size: "small",
            message: message
          });
        }
      )


    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        /*bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
        this.editModeOn = false*/
        window.location.reload()
      }
    )


  }

  getDiffDetails() {

    let tempSyncServices = this.syncServices

    let l: number = this.syncServices.length
    if (this.baselinedSync.length > l) {
      l = this.baselinedSync.length
    }

    let i = 0, j = 0
    for (; i < this.baselinedSync.length && j < this.syncServices.length;) {

      if (this.baselinedSync[i].serviceName == this.syncServices[j].serviceName &&
        this.baselinedSync[i].operation == this.syncServices[j].operation &&
        this.baselinedSync[i].version == this.syncServices[j].version) {

        this.diffSync.push(this.diffSyncServices(this.baselinedSync[i], this.syncServices[j]))

        i++
        j++

      } else {
        //Check if match found further down baselined Sync Service.
        let syncIndex = j
        let baseIndex = i + 1
        let matchFound: boolean = false
        while (baseIndex < this.baselinedSync.length) {
          if (this.baselinedSync[baseIndex].serviceName == this.syncServices[syncIndex].serviceName &&
            this.baselinedSync[baseIndex].operation == this.syncServices[syncIndex].operation &&
            this.baselinedSync[baseIndex].version == this.syncServices[syncIndex].version) {
            //If found this would mean some services were deleted from the baseline
            matchFound = true
            for (let x = i; x < baseIndex; x++) {
              this.diffSync.push(this.diffSyncServices(this.baselinedSync[x], new SyncService()))
            }
            this.diffSync.push(this.diffSyncServices(this.baselinedSync[baseIndex], this.syncServices[syncIndex]))

            i = baseIndex + 1
            j = syncIndex + 1
            break
          } else {
            baseIndex++
          }
        }

        if (!matchFound) {
          //If not found this would mean some services was added new
          this.diffSync.push(this.diffSyncServices(new SyncService(), this.syncServices[syncIndex]))
          j = syncIndex + 1
        }
      }

    }

    if (i < this.baselinedSync.length) {

      for (let x = i; x < this.baselinedSync.length; x++) {
        this.diffSync.push(this.diffSyncServices(this.baselinedSync[x], new SyncService()))
        //this.baselinedSync[x] = undefined
      }

    }

    if (j < this.syncServices.length) {

      for (let x = j; x < this.syncServices.length; x++) {
        this.diffSync.push(this.diffSyncServices(new SyncService(), this.syncServices[x]))
        //this.syncServices[x] = undefined
      }

    }

    this.syncServices = tempSyncServices

  }

  diffSyncServices(baseline: SyncService, newValue: SyncService): SyncService {



    let syncDiff = new SyncService()

    if (!baseline || !newValue) {
      return syncDiff
    }

    let changeFlag: boolean = false

    syncDiff.appRole = this.diff(baseline.appRole, newValue.appRole)
    if (syncDiff.appRole.includes("<del>") || syncDiff.appRole.includes("<del>")) {
      changeFlag = true
    }

    syncDiff.connectionInitiatedBy = this.diff(baseline.connectionInitiatedBy, newValue.connectionInitiatedBy)
    if (syncDiff.connectionInitiatedBy.includes("<del>") || syncDiff.connectionInitiatedBy.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.comments = this.diff(baseline.comments, newValue.comments)
    if (syncDiff.comments.includes("<del>") || syncDiff.comments.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.parameters = this.diff(baseline.parameters, newValue.parameters)
    if (syncDiff.parameters.includes("<del>") || syncDiff.parameters.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.ipAddPort = this.diff(baseline.ipAddPort, newValue.ipAddPort)
    if (syncDiff.ipAddPort.includes("<del>") || syncDiff.ipAddPort.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.serviceUrl = this.diff(baseline.serviceUrl, newValue.serviceUrl)
    if (syncDiff.serviceUrl.includes("<del>") || syncDiff.serviceUrl.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.protocolUsed = this.diff(baseline.protocolUsed, newValue.protocolUsed)
    if (syncDiff.protocolUsed.includes("<del>") || syncDiff.protocolUsed.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.purposeOfUsage = this.diff(baseline.purposeOfUsage, newValue.purposeOfUsage)
    if (syncDiff.purposeOfUsage.includes("<del>") || syncDiff.purposeOfUsage.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.criticality = this.diff(baseline.criticality, newValue.criticality)
    if (syncDiff.criticality.includes("<del>") || syncDiff.criticality.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.workaround = this.diff(baseline.workaround, newValue.workaround)
    if (syncDiff.workaround.includes("<del>") || syncDiff.workaround.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.criticalityJustification = this.diff(baseline.criticalityJustification, newValue.criticalityJustification)
    if (syncDiff.criticalityJustification.includes("<del>") || syncDiff.criticalityJustification.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.provViaExtern = this.diff(baseline.provViaExtern, newValue.provViaExtern)
    if (syncDiff.provViaExtern.includes("<del>") || syncDiff.provViaExtern.includes("<del>")) {
      changeFlag = true
    }
    syncDiff.maxCap = this.diff(baseline.maxCap, newValue.maxCap)
    if (syncDiff.maxCap.includes("<del>") || syncDiff.maxCap.includes("<del>")) {
      changeFlag = true
    }

    if (baseline.dataWorthProtection && newValue.dataWorthProtection) {
      syncDiff.dataWorthProtection = this.diff(baseline.dataWorthProtection.toString(), newValue.dataWorthProtection.toString())
    } else if (newValue.dataWorthProtection) {
      syncDiff.dataWorthProtection = this.diff("", newValue.dataWorthProtection.toString())
    } else if (baseline.dataWorthProtection) {
      syncDiff.dataWorthProtection = this.diff(baseline.dataWorthProtection.toString(), "")
    } else {
      syncDiff.dataWorthProtection = this.diff("", "")
    }
    if (syncDiff.dataWorthProtection.toString().includes("<del>") || syncDiff.dataWorthProtection.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.sensitive && newValue.sensitive) {
      syncDiff.sensitive = this.diff(baseline.sensitive.toString(), newValue.sensitive.toString())
    } else if (newValue.sensitive) {
      syncDiff.sensitive = this.diff("", newValue.sensitive.toString())
    } else if (baseline.sensitive) {
      syncDiff.sensitive = this.diff(baseline.sensitive.toString(), "")
    } else {
      syncDiff.sensitive = this.diff("", "")
    }
    if (syncDiff.sensitive.toString().includes("<del>") || syncDiff.sensitive.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.meanCallsPerDay && newValue.meanCallsPerDay) {
      syncDiff.meanCallsPerDay = this.diff(baseline.meanCallsPerDay.toString(), newValue.meanCallsPerDay.toString())
    } else if (newValue.meanCallsPerDay) {
      syncDiff.meanCallsPerDay = this.diff("", newValue.meanCallsPerDay.toString())
    } else if (baseline.meanCallsPerDay) {
      syncDiff.meanCallsPerDay = this.diff(baseline.meanCallsPerDay.toString(), "")
    } else {
      syncDiff.meanCallsPerDay = this.diff("", "")
    }
    if (syncDiff.meanCallsPerDay.toString().includes("<del>") || syncDiff.meanCallsPerDay.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.peakCallsPerDay && newValue.peakCallsPerDay) {
      syncDiff.peakCallsPerDay = this.diff(baseline.peakCallsPerDay.toString(), newValue.peakCallsPerDay.toString())
    } else if (newValue.peakCallsPerDay) {
      syncDiff.peakCallsPerDay = this.diff("", newValue.peakCallsPerDay.toString())
    } else if (baseline.peakCallsPerDay) {
      syncDiff.peakCallsPerDay = this.diff(baseline.peakCallsPerDay.toString(), "")
    } else {
      syncDiff.peakCallsPerDay = this.diff("", "")
    }
    if (syncDiff.peakCallsPerDay.toString().includes("<del>") || syncDiff.peakCallsPerDay.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.meanCallsPerSec && newValue.meanCallsPerSec) {
      syncDiff.meanCallsPerSec = this.diff(baseline.meanCallsPerSec.toString(), newValue.meanCallsPerSec.toString())
    } else if (newValue.meanCallsPerSec) {
      syncDiff.meanCallsPerSec = this.diff("", newValue.meanCallsPerSec.toString())
    } else if (baseline.meanCallsPerSec) {
      syncDiff.meanCallsPerSec = this.diff(baseline.meanCallsPerSec.toString(), "")
    } else {
      syncDiff.meanCallsPerSec = this.diff("", "")
    }
    if (syncDiff.meanCallsPerSec.toString().includes("<del>") || syncDiff.meanCallsPerSec.toString().includes("<del>")) {
      changeFlag = true
    }


    if (baseline.peakCallsPerSec && newValue.peakCallsPerSec) {
      syncDiff.peakCallsPerSec = this.diff(baseline.peakCallsPerSec.toString(), newValue.peakCallsPerSec.toString())
    } else if (newValue.peakCallsPerSec) {
      syncDiff.peakCallsPerSec = this.diff("", newValue.peakCallsPerSec.toString())
    } else if (baseline.peakCallsPerSec) {
      syncDiff.peakCallsPerSec = this.diff(baseline.peakCallsPerSec.toString(), "")
    } else {
      syncDiff.peakCallsPerSec = this.diff("", "")
    }
    if (syncDiff.peakCallsPerSec.toString().includes("<del>") || syncDiff.peakCallsPerSec.toString().includes("<del>")) {
      changeFlag = true
    }

    syncDiff.dailyLoadDistributionPeriod = this.diff(baseline.dailyLoadDistributionPeriod, newValue.dailyLoadDistributionPeriod)
    if (syncDiff.dailyLoadDistributionPeriod.includes("<del>") || syncDiff.dailyLoadDistributionPeriod.includes("<del>")) {
      changeFlag = true
    }



    if (baseline.serviceName != null && newValue.serviceName == null) {
      syncDiff.title = "<b><i><font color='#FFA5A2'>" + baseline.serviceName + ". " + baseline.operation + "</font></i></b>"
      syncDiff.serviceName = "<del>" + baseline.serviceName + "</del>"
      syncDiff.operation = "<del>" + baseline.operation + "</del>"
      syncDiff.version = "<del>" + baseline.version + "</del>"
    } else if (baseline.serviceName == null && newValue.serviceName != null) {
      syncDiff.title = "<b><i><font color='#B1FCCF'>" + newValue.serviceName + ". " + newValue.operation + "</font></i></b>"
      syncDiff.serviceName = "<ins>" + newValue.serviceName + "</ins>"
      syncDiff.operation = "<ins>" + newValue.operation + "</ins>"
      syncDiff.version = "<ins>" + newValue.version + "</ins>"
    } else if (changeFlag) {
      syncDiff.title = "<b><i><font color='#F5F9A6'>" + newValue.serviceName + ". " + newValue.operation + "</font></i></b>"
      syncDiff.serviceName = newValue.serviceName
      syncDiff.operation = newValue.operation
      syncDiff.version = this.diff(baseline.version, newValue.version)
    } else {
      syncDiff.title = newValue.serviceName + ". " + newValue.operation
      syncDiff.serviceName = newValue.serviceName
      syncDiff.operation = newValue.operation
      syncDiff.version = newValue.version
    }

    return syncDiff
  }

  diff(str1: string, str2: string): string {
    //return JsDiff.diffChars(str1,"str2")
    if (str1 == null || str1 == undefined) {
      str1 = ""
    } else {
      while (str1.includes("<")) {
        str1 = str1.replace("<", "&lt;")
      }
      while (str1.includes(">")) {
        str1 = str1.replace(">", "&gt;")
      }
    }
    if (str2 == null || str2 == undefined) {
      str2 = ""
    } else {
      while (str2.includes("<")) {
        str2 = str2.replace("<", "&lt;")
      }
      while (str1.includes(">")) {
        str1 = str1.replace(">", "&gt;")
      }
    }
    return diff(str1, str2)

  }

}

declare let bootbox: any;
declare let HoldOn: any;

const diff = require('rich-text-diff')