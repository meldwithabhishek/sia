import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncServicesComponent } from './sync-services.component';

describe('SyncServicesComponent', () => {
  let component: SyncServicesComponent;
  let fixture: ComponentFixture<SyncServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyncServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
