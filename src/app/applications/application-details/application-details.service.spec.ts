import { TestBed, inject } from '@angular/core/testing';

import { ApplicationDetailsService } from './application-details.service';

describe('ApplicationDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicationDetailsService]
    });
  });

  it('should ...', inject([ApplicationDetailsService], (service: ApplicationDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
