import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { E2eIntroComponent } from './e2e-intro.component';

describe('E2eIntroComponent', () => {
  let component: E2eIntroComponent;
  let fixture: ComponentFixture<E2eIntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ E2eIntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(E2eIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
