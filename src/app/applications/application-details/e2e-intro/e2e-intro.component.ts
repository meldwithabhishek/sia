import { ApplicationDetailsService } from './../application-details.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-e2e-intro',
  templateUrl: './e2e-intro.component.html',
  styleUrls: ['./e2e-intro.component.css']
})
export class E2eIntroComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}