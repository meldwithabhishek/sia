import { element } from 'protractor';
import { ApplicationDetailsService } from './../application-details.service';
import { AuthService } from './../../../shared/services/auth.service';
import { Component, OnChanges, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-search-params',
  templateUrl: './search-params.component.html',
  styleUrls: ['./search-params.component.css']
})
export class SearchParamsComponent implements OnInit, OnChanges {


  ngOnChanges(changes: SimpleChanges): void {
    this.onSIANameChange();

  }

  @Input() path: string
  @Input() airline: string;
  airlineList: any[];
  versionList: any[] = []
  @Input() applicationName: string;
  @Input() version: string;
  @Input() applicationNameList: string[] = [];
  @Input() applicationCompleteNameList: any[];
  message: string;

  @Output() onFormChanged = new EventEmitter<any>();

  constructor(public appListServ: ApplicationDetailsService, public authServ: AuthService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appListServ.getAirlinesList().subscribe(resp => {
      if (this.authServ.getUserCarrier() == 'LX') {
        this.airlineList = []
        resp.forEach(element => {
          if (element.name == "LX" || element.name == "SN") {
            this.airlineList.push(element)
          }
        });
      } else {
        this.airlineList = resp;
      }
      
      HoldOn.close();
    },
      error => {
        console.error(error);
        HoldOn.close();
      });



  }



  getAppList() {

    this.versionList = []
    this.applicationName = undefined
    this.version = undefined
    if (this.path == "my-applications") {
      let userAppDetails: any[] = this.authServ.getUserApplications()
      let userApps: string[] = []
      userAppDetails.forEach(apps => {
        if (this.airline == apps.customerId) {
          userApps.push(apps.application)
        }
      }
      )
      userApps.sort(function (a, b) {
        return a.toLowerCase().localeCompare(b.toLowerCase());
      });

      //let userApps: string[] = this.authServ.getUserApplications()
      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appListServ.getUniqueApplicationsForAirlne(this.airline).subscribe(appList => {
        this.applicationNameList = []
        appList.forEach(element => {
          if (userApps.includes(element.name)) {
            this.applicationNameList.push(element);
          }
        });
        this.applicationNameList.sort(function (a : any, b : any) {
          return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
        })

        HoldOn.close();
        if (this.applicationNameList.length == 0) {
          this.message = "No Applications available for " + this.airline;
        } else {
          this.message = undefined;
        }
      },
        error => {
          console.error(error);
          HoldOn.close();
          let message = "Failed to get applications."
          if (error.errorMessage) {
            message = error.errorMessage
          }
          HoldOn.close();
        });


    } else if (this.path == "approvals") {

      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appListServ.getApplicationsPendingApprovalForAirline(this.airline).subscribe(appList => {
        this.applicationNameList = appList.sort(function (a : any, b : any) {
          return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
        })
        HoldOn.close();
        if (this.applicationNameList.length == 0) {
          this.message = "No Applications available for " + this.airline;
        } else {
          this.message = undefined;
        }
      },
        error => {
          console.error(error);
          HoldOn.close();
          let message = "Failed to get applications."
          if (error.errorMessage) {
            message = error.errorMessage
          }
          HoldOn.close();
        });

    } else {

      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appListServ.getApplicationsForAirline(this.airline).subscribe(appList => {
        this.applicationCompleteNameList = appList;

        this.applicationNameList = []
        let tempList: string[] = []
        appList.forEach(element => {
          if (!tempList.includes(element.name)) {
            this.applicationNameList.push(element);
            tempList.push(element.name)
          }
        });
        this.applicationNameList.sort(function (a : any, b : any) {
          return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
        })
        ////this.applicationNameList = this.sortService(this.applicationNameList)


        HoldOn.close();
        if (this.applicationNameList.length == 0) {
          this.message = "No Applications available for " + this.airline;
        } else {
          this.message = undefined;
        }
      },
        error => {
          console.error(error);
          HoldOn.close();
          let message = "Failed to get applications."
          if (error.errorMessage) {
            message = error.errorMessage
          }
          HoldOn.close();
        });

    }



  }

  sortService(applicationNameList) {
    let newList = applicationNameList;

    for (let i = 0; i < newList.length - 1; i++) {
      for (let j = i+1; j < newList.length -1; j++) {
       
        if (newList[i].name.toLowerCase().toString().localeCompare(newList[j].name.toString().toLowerCase()) > 0) {
          let temp = newList[i]
          newList[i] = newList[j]
          newList[j] = temp
        }
      }
    }

    return newList;

  }

  onSIANameChange() {
    this.versionList = []
    if (this.applicationCompleteNameList) {
      this.applicationCompleteNameList.forEach(element => {
        if (element.name == this.applicationName) {
          this.versionList.push(element.version)
        }
      })
      this.versionList.sort(function (a, b) {
        return a.toLowerCase().localeCompare(b.toLowerCase());
      });

      if (this.versionList.length > 0) {
        this.version = this.versionList[this.versionList.length - 1]
        this.emitSelectedAppName()
      }
    }
  }

  emitSelectedAppName() {
    sessionStorage.removeItem("tabNumber");
    this.onFormChanged.emit({ airline: this.airline, applicationName: this.applicationName, version: this.version });        
  }

  redirect(url: string) {
    this.router.navigate([url]);
  }

}

declare let HoldOn: any;