import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsyncServCatComponent } from './async-serv-cat.component';

describe('AsyncServCatComponent', () => {
  let component: AsyncServCatComponent;
  let fixture: ComponentFixture<AsyncServCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsyncServCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsyncServCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
