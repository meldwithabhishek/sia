import { ApplicationDetailsService } from './../application-details.service';
import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-async-serv-cat',
  templateUrl: './async-serv-cat.component.html',
  styleUrls: ['./async-serv-cat.component.css']
})
export class AsyncServCatComponent implements OnInit {

  public env
  // data supplied to the data table
  private completeData
  data: any[];

  private customer : string = ""
  private processName : string = ""
  // array of currently selected entities in the data table
  selectedEntities: Map<String, String>[];

  constructor(private route: ActivatedRoute, private router: Router, private appServ: ApplicationDetailsService) { }

  ngOnInit() {
    this.env = "PROD"
    this.getAsyncCatalogue()
  }

  getAsyncCatalogue() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });    
    this.appServ.getAsyncServiceCatalogue(this.env).subscribe(respList => {
      this.data = respList;
      this.completeData = this.data
      this.completeData.forEach(element => {
        while (element.topics && ( element.topics.includes(";") || element.topics.includes(",") )) {
          element.topics = element.topics.replace(";", "<br>")
          element.topics = element.topics.replace(",", "<br>")
        }
        while (element.queues && ( element.queues.includes(";") || element.queues.includes(","))) {
          element.queues = element.queues.replace(";", "<br>")
          element.queues = element.queues.replace(",", "<br>")
        }
        while (element.referencedServiceGroups && ( element.referencedServiceGroups.includes(",") || element.referencedServiceGroups.includes(";"))) {
          element.referencedServiceGroups = element.referencedServiceGroups.replace(",", "<br>")
          element.referencedServiceGroups = element.referencedServiceGroups.replace(";", "<br>")
        
        }
        let start = 0
        while (element.sharepointReferences && element.sharepointReferences.substring(start,element.sharepointReferences.length).includes("http")) {
          start = element.sharepointReferences.indexOf("http", start)
          let end = element.sharepointReferences.indexOf(" ", start)
          if (end < start) {
            end = element.sharepointReferences.length
          }
          let sub = element.sharepointReferences.substring(start,end)
          element.sharepointReferences = element.sharepointReferences.replace(sub, "Click Here".link(sub))
          start = end
        }


      });
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get Async Service Catalogue"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });
  }

  // function to handle data/entities selected/deselected in the table 
  public setSelectedEntities($event: any) {
    this.selectedEntities = $event;
  }
  
  filter() {
    this.data   = []
    this.completeData.forEach(element => {
      if (element.customer.toLowerCase().includes(this.customer.toLowerCase()) && element.processName.toLowerCase().includes(this.processName.toLowerCase())) {
        this.data.push(element)
      }
    });  
  }

}

function isBigEnough(element, index, array) { 
  return (element >= 10); 
}
declare let bootbox: any;
declare let HoldOn: any;
