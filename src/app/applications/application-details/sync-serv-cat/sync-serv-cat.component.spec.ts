import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncServCatComponent } from './sync-serv-cat.component';

describe('SyncServCatComponent', () => {
  let component: SyncServCatComponent;
  let fixture: ComponentFixture<SyncServCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyncServCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncServCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
