import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ApplicationDetailsService } from '../application-details.service';


@Component({
  selector: 'app-sync-serv-cat',
  templateUrl: './sync-serv-cat.component.html',
  styleUrls: ['./sync-serv-cat.component.css']
})
export class SyncServCatComponent implements OnInit {

  // data supplied to the data table
  private appName: string;
  private customer: string;
  private completeData
  data: any[];
  private sgName: string = ""
  private op: string = ""
  // array of currently selected entities in the data table
  selectedEntities: Map<String, String>[];

  constructor(private route: ActivatedRoute, private router: Router, private appServ: ApplicationDetailsService) { }

  ngOnInit() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getServiceCatalogue().subscribe(respList => {
      this.data = respList;
      this.completeData = this.data
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get Sync Service Catalogue"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });

    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        let split: string[] = params['id'].split("|")
        this.appName = split[1];
        this.customer = split[0];
      }
    });
  }

  // function to handle data/entities selected/deselected in the table 
  public setSelectedEntities($event: any) {
    this.selectedEntities = $event;
  }

  submitSelectedFields() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.addSync(this.appName, this.customer, this.selectedEntities).subscribe(
      response => {
        HoldOn.close()
        bootbox.alert({
          title: "Success",
          size: "small",
          message: "SIA updated successfully. Selected sync service(s) have been added successfully. "
        });
        sessionStorage.setItem("tabNumber","3");
        this.router.navigate(["../../" + "app-details/" + this.customer + "|" + this.appName], { relativeTo: this.route });

      }, error => {
        HoldOn.close();
        let message = "Failed to update SIA with the selected services."
        if (error.errorMessage) {
          message = error.errorMessage
        }
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
      }
    )
  }

  filter() {
    this.data = []
    this.completeData.forEach(element => {
      if (element.operation.toLowerCase().includes(this.op.toLowerCase()) && element.name.toLowerCase().includes(this.sgName.toLowerCase())) {
        this.data.push(element)
      }
    });
  }
}

function isBigEnough(element, index, array) {
  return (element >= 10);
}
declare let bootbox: any;
declare let HoldOn: any;