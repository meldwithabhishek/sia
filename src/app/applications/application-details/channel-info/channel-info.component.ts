import { ChannelInfo } from './../../../shared/models/channel-info-model';
import { ApplicationDetailsService } from './../application-details.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-channel-info',
  templateUrl: './channel-info.component.html',
  styleUrls: ['./channel-info.component.css']
})
export class ChannelInfoComponent implements OnInit, OnChanges, OnDestroy {



  editModeOn = false;

  @Input('appName')
  appName: any;

  @Input('airline')
  airline: any;

  @Input("customer")
  customer: string

  @Input('channelInfo')
  channelInfo: any = new ChannelInfo()

  @Input('baselinedChannelInfo')
  baselinedChannelInfo: any;

  @Input('enableDiff')
  enableDiff: boolean

  channelInfoBackup: any;

  @Input('isEditAllowed')
  isEditAllowed: boolean;

  volumeJSON: any

  constructor(private appServ: ApplicationDetailsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    if (this.channelInfo == null || this.channelInfo == undefined) {
      this.channelInfo = new ChannelInfo()
    }

    if (this.baselinedChannelInfo == null || this.baselinedChannelInfo == undefined) {
      this.baselinedChannelInfo = new ChannelInfo()
    }

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getVolumeControlJSON().subscribe(
      resp => {
        this.volumeJSON = resp
        HoldOn.close()
      },
      error => {
        HoldOn.close()
      }
    )

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.isEditAllowed) {
      if (this.channelInfoBackup) {
        this.channelInfo = JSON.parse(this.channelInfoBackup)
      }
      this.editModeOn = false
    }
  }

  ngOnDestroy(): void {
    if (this.editModeOn) {

      var result = confirm("Would you like to save you changes  to 'Channel Info' before leaving this page?");

      if (result) {
        this.onSave()
      }
    }
  }

  enableEdit() {
    
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {
      
      HoldOn.close();
      this.channelInfoBackup = JSON.stringify(this.channelInfo)
      this.editModeOn = true
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
        this.editModeOn = false
      }
    )


  }

  onDiscard() {
    if (this.channelInfoBackup) {
      this.channelInfo = JSON.parse(this.channelInfoBackup)
    }
    this.editModeOn = false
    sessionStorage.setItem("tabNumber","2");
    window.location.reload()
  }

  onSave() {


    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.lockApp(this.appName, this.airline).subscribe(resp => {

      HoldOn.close();

      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appServ.updateChannelInfo(this.appName, this.customer, this.channelInfo).subscribe(
        response => {
          HoldOn.close()
          this.router.navigate(["../" + this.customer + "|" + this.appName], { relativeTo: this.route });
          this.editModeOn = false
          bootbox.alert({
            title: "Success",
            size: "small",
            message: "Channel Info updated successfully"
          });
  
  
        }, error => {
          HoldOn.close();
          let message = "Failed to update Channel Info"
          if (error.errorMessage) {
            message = error.errorMessage
          }
          bootbox.alert({
            title: "Error",
            size: "small",
            message: message
          });
        }
      )


    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        /*bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
        this.editModeOn = false*/
        window.location.reload()
      }
    )
    
  }

  diff(str1: string, str2: string) {
    //return JsDiff.diffChars(str1,"str2")
    if (str1 == null || str1 == undefined) {
      str1 = ""
    } else {
      while (str1.includes("<")) {
        str1 = str1.replace("<", "&lt;")
      }
    }
    if (str2 == null || str2 == undefined) {
      str2 = ""
    } else {
      while (str2.includes("<")) {
        str2 = str2.replace("<", "&lt;")
      }
    }
    return diff(str1, str2)

  }

  getVolumeControlValue(level: string) {

    let volumeControl = "**TBD**"
    if (this.volumeJSON) {
      this.volumeJSON.forEach(element => {
        if (element.level == level) {
          volumeControl = element.control
        }
      });
    }
    return volumeControl
  }

}

declare let bootbox: any;
declare let HoldOn: any;

const diff = require('rich-text-diff')
