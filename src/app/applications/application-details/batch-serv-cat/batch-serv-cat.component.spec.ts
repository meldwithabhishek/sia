import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchServCatComponent } from './batch-serv-cat.component';

describe('BatchServCatComponent', () => {
  let component: BatchServCatComponent;
  let fixture: ComponentFixture<BatchServCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchServCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchServCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
