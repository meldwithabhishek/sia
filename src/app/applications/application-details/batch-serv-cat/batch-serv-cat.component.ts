import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ApplicationDetailsService } from '../application-details.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ConfigurationService } from '../../../shared/services/configuration.service';

@Component({
  selector: 'app-batch-serv-cat',
  templateUrl: './batch-serv-cat.component.html',
  styleUrls: ['./batch-serv-cat.component.css']
})
export class BatchServCatComponent implements OnInit {


  // data supplied to the data table
  private appName: string;
  private completeData
  data: any[];
  private productType: string = ""

  // array of currently selected entities in the data table
  selectedEntities: Map<String, String>[];
  uploader: FileUploader;
  previousLength = 0;

  constructor(private route: ActivatedRoute, private router: Router,
    private appServ: ApplicationDetailsService, public authServ: AuthService,
    private config: ConfigurationService) { }

  ngOnInit() {
    let url = this.config.getConfig().SERV_CAT + this.authServ.getUserName() + "/BatchUpload"
    //let url = "http://171.17.25.37:9002/siawebapp/sia/v1.0/SIADataLoader"
    this.uploader = new FileUploader({ url: url });
    this.getBatchServices()
  }

  fileChanged(evt) {

    if (this.uploader.queue.length == this.previousLength) {
      this.uploader.queue.pop();
      this.previousLength = 0;
    }

    if (this.uploader.queue && this.uploader.queue.length > 0) {
      let fileArray = new Array();
      fileArray.push(this.uploader.queue.pop());

      let regExp = /\.(csv)$/gi;
      if (regExp.test(fileArray[0].file.name)) {
        this.uploader.queue = fileArray;
        this.previousLength = this.uploader.queue.length;
      }
      else {
        bootbox.alert({
          title: "Error",
          size: "small",
          message: "Only .csv files allowed."
        });
        this.uploader.queue.pop();
        this.previousLength = 0;
      }
    }
  }

  getBatchServices() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getBatchServiceCatalogue().subscribe(respList => {
      this.data = respList;
      this.completeData = this.data
      this.completeData.forEach(element => {
        while (element.fileName.includes(",")) {
          element.fileName = element.fileName.replace(",", "<br>")
        }
      });
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get Batch Service Catalogue"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });
  }

  // function to handle data/entities selected/deselected in the table 
  public setSelectedEntities($event: any) {
    this.selectedEntities = $event;
  }

  filter() {
    this.data = []
    this.completeData.forEach(element => {
      if (element.productType.toLowerCase().includes(this.productType.toLowerCase())) {
        this.data.push(element)
      }
    });
  }

  uploadFile(item) {
    item.upload();
    this.uploader.onSuccessItem = (item, response, status, header) => {
      bootbox.alert({
        title: "Success",
        size: "small",
        message: "CSV successfully uploaded."
      });
      this.getBatchServices()
    };

    this.uploader.onErrorItem = (item, response, status, header) => {
      let message = "CSV Upload failed."
      try {
        let respJson = JSON.parse(response)
        if (respJson.errorMessage) {
          message = respJson.errorMessage
        }
      } catch (error) {
      }
         
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    };

  }

  submitSelectedFields() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.deleteBatchServiceCatalogue(this.selectedEntities).subscribe(
      response => {
        HoldOn.close()
        bootbox.alert({
          title: "Success",
          size: "small",
          message: "Successfully deleted Batch Catalogue entries"
        });
        this.getBatchServices();

      }, error => {
        HoldOn.close();
        let message = "Failed to delete Batch Catalogue entries"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
      }
    )
  }
}

function isBigEnough(element, index, array) {
  return (element >= 10);
}
declare let bootbox: any;
declare let HoldOn: any;
