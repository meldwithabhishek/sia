import { element } from 'protractor';
import { AuthService } from './../../shared/services/auth.service';
import { ConfigurationService } from './../../shared/services/configuration.service';
import { ApplicationDetailsService } from './application-details.service';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';


@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.css']
})
export class ApplicationDetailsComponent implements OnInit, OnChanges {


  appName: string;
  airlineName: string;
  version: string
  appNameList: any[];
  applicationCompleteNameList: any[] = []
  appDetails: any;
  message: string;
  isEditAllowed: boolean = false;
  intro: any;
  path: string;

  enableDiff: boolean = true

  latestBaselinedAppDetails: any;

  tabNumber: Number = 1


  constructor(private route: ActivatedRoute, private router: Router,
    private appDetailServ: ApplicationDetailsService,
    private configService: ConfigurationService, private authServ: AuthService) { }

  ngOnChanges(changes: SimpleChanges): void {
    let tabNum = sessionStorage.getItem("tabNumber");
    sessionStorage.removeItem("tabNumber");
  }

  ngOnInit() {

    let tabNum = sessionStorage.getItem("tabNumber");
    sessionStorage.removeItem("tabNumber");
    if (tabNum != null || tabNum != undefined) {
      this.tabNumber = new Number(tabNum);
    } else {
      this.tabNumber = 1
    }

    if (this.appDetails && this.appDetails.lockedBy != this.authServ.getUserName()
      && this.appDetails.approvalStatus != 'SUBMITTED_FOR_APPROVAL') {
      this.lockApp()
    }

    if (this.route.pathFromRoot.toString().includes("my-applications")) {
      this.path = "my-applications"
    }
    if (this.route.pathFromRoot.toString().includes("approvals")) {
      this.path = "approvals"
    }
    if (this.route.pathFromRoot.toString().includes("incident")) {
      this.path = "incident-management"
    } 
    if (this.route.pathFromRoot.toString().includes("review")) {
      this.path = "review"
    }
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appDetailServ.getApplicationIntro().subscribe(
      intro => {
        this.intro = intro
        HoldOn.close();
      }, error => {
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: "Failed to load application configuration"
        });
      }
    )


    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.configService.readConfigFile().subscribe(
      data => {
        if (data == true) {
          this.configService.getConfig();
          this.route.params.subscribe((params: Params) => {
            let app: string = params['id']
            this.appDetails = undefined
            if (app) {
              let appSplit: string[] = app.split("|")
              if (appSplit.length == 3) {
                this.airlineName = appSplit[0]
                this.appName = appSplit[1]
                this.version = appSplit[2]
                this.getApplicationDetails("baseline");
              } else {
                this.airlineName = appSplit[0]
                this.appName = appSplit[1]
                this.getApplicationDetails("draft");
              }

            } else {
              let carrier = this.authServ.getUserCarrier();
              if (carrier == "1A") {
                this.airlineName = "LH";
                this.getAppList()
              } else {
                this.airlineName = carrier
                this.getAppList()
              }
            }
          });
        }
        else {
          bootbox.alert({
            title: "Error",
            size: "small",
            message: "Failed to load application configuration"
          });
        }
        HoldOn.close();
      },
      error => {
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: "Failed to load application configuration"
        });

      }
    )
  }

  getApplicationDetails(type: string) {
    if (type == "baseline") {
      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appDetailServ.getApplicationDetails(this.airlineName, this.appName, this.version).subscribe(resp => {
        this.appDetails = resp;
        if (this.appDetails) {
          this.airlineName = this.appDetails.airlineName;
          this.getAppList();
          this.isEditAllowedForApplication()
        }
        HoldOn.close();
      },
        error => {
          console.error(error);
          HoldOn.close();
        }
      )
    } else {
      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appDetailServ.getDraftApplicationDetails(this.airlineName, this.appName).subscribe(resp => {
        this.appDetails = resp;
        if (this.path == "my-applications" && this.appDetails && this.appDetails.lockedBy != this.authServ.getUserName()
          && this.appDetails.approvalStatus != 'SUBMITTED_FOR_APPROVAL') {
          this.lockApp()
        }
        if (this.appDetails) {
          this.airlineName = this.appDetails.airlineName;
          this.getAppList();
          this.isEditAllowedForApplication()
          if (this.path == "approvals" || this.path == "review") {
            this.getLatestBaselinedSIA()
          }
        }
        HoldOn.close();
      },
        error => {
          console.error(error);
          HoldOn.close();
        }
      )
    }
  }

  getLatestBaselinedSIA() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appDetailServ.getLatestBaselinedSIA(this.appName, this.airlineName).subscribe(resp => {
      this.latestBaselinedAppDetails = resp;
      HoldOn.close();
    },
      error => {
        console.error(error);
        HoldOn.close();
      }
    )
  }



  getAppList() {
    if (!this.appNameList) {
      if (this.path == "my-applications") {
        //let userApps: string[] = this.authServ.getUserApplications()
        let userAppDetails: any[] = this.authServ.getUserApplications()
        let userApps: string[] = []
        userAppDetails.forEach(apps => {
          if (this.airlineName == apps.customerId) {
            userApps.push(apps.application)
          }
        }
        )
        userApps.sort(function (a, b) {
          return a.toLowerCase().localeCompare(b.toLowerCase());
        });
        HoldOn.open({
          theme: "sk-cube-grid",
          message: 'Loading...'
        });
        this.appDetailServ.getUniqueApplicationsForAirlne(this.airlineName).subscribe(appList => {
          this.appNameList = []
          appList.forEach(element => {
            if (userApps.includes(element.name)) {
              this.appNameList.push(element);
            }
          });
          this.appNameList.sort(function (a: any, b: any) {
            return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
          })
          HoldOn.close();
          if (this.appNameList.length == 0) {
            this.message = "No Applications available for " + this.airlineName;
          } else {
            this.message = undefined;
          }
        },
          error => {
            console.error(error);
            HoldOn.close();
            let message = "Failed to get applications."
            if (error.errorMessage) {
              message = error.errorMessage
            }
            HoldOn.close();
            bootbox.alert({
              title: "Error",
              size: "small",
              message: message
            });
          });


      } else if (this.path == "approvals") {

        HoldOn.open({
          theme: "sk-cube-grid",
          message: 'Loading...'
        });
        this.appDetailServ.getApplicationsPendingApprovalForAirline(this.airlineName).subscribe(appList => {
          this.appNameList = appList.sort(function (a: any, b: any) {
            return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
          })
          HoldOn.close();
          if (this.appNameList.length == 0) {
            this.message = "No Applications available for " + this.airlineName;
          } else {
            this.message = undefined;
          }
        },
          error => {
            console.error(error);
            HoldOn.close();
            let message = "Failed to get applications."
            if (error.errorMessage) {
              message = error.errorMessage
            }
            HoldOn.close();
            bootbox.alert({
              title: "Error",
              size: "small",
              message: message
            });
          });

      } else {

        HoldOn.open({
          theme: "sk-cube-grid",
          message: 'Loading...'
        });
        this.appDetailServ.getApplicationsForAirline(this.airlineName).subscribe(appList => {
          this.applicationCompleteNameList = appList
          this.appNameList = []
          let tempList: string[] = []
          appList.forEach(element => {
            if (!tempList.includes(element.name)) {
              this.appNameList.push(element);
              tempList.push(element.name)
            }
          });
          this.appNameList.sort(function (a: any, b: any) {
            return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
          })


          HoldOn.close();
          if (this.appNameList.length == 0) {
            this.message = "No Applications available for " + this.airlineName;
          } else {
            this.message = undefined;
          }
        },
          error => {
            console.error(error);
            HoldOn.close();
            let message = "Failed to get applications."
            if (error.errorMessage) {
              message = error.errorMessage
            }
            HoldOn.close();
            bootbox.alert({
              title: "Error",
              size: "small",
              message: message
            });
          });

      }
    }

  }

  onDetailsChanged(evt) {
    this.airlineName = evt.airline
    this.tabNumber = 1
    if (this.appName == undefined) {

      this.appName = evt.applicationName
      this.version = evt.version

      if (this.appName != undefined) {
        if (this.version == undefined) {
          sessionStorage.removeItem("tabNumber");
          sessionStorage.setItem("tabNumber", "1");
          this.router.navigate([this.airlineName + "|" + this.appName], { relativeTo: this.route });
        } else {
          sessionStorage.removeItem("tabNumber");
          sessionStorage.setItem("tabNumber", "1");
          this.router.navigate([this.airlineName + "|" + this.appName + "|" + this.version], { relativeTo: this.route });
        }
      }
    } else {
      this.appName = evt.applicationName
      this.version = evt.version
      if (this.version == undefined) {
        sessionStorage.removeItem("tabNumber");
        sessionStorage.setItem("tabNumber", "1");
        this.router.navigate(["../" + this.airlineName + "|" + this.appName], { relativeTo: this.route });
      } else {
        sessionStorage.removeItem("tabNumber");
        sessionStorage.setItem("tabNumber", "1");
        this.router.navigate(["../" + this.airlineName + "|" + this.appName + "|" + this.version], { relativeTo: this.route });
      }
    }

  }

  onSubmissionForReview(event) {
    this.appDetails = event
    this.isEditAllowed = false
  }

  isEditAllowedForApplication() {
    this.isEditAllowed = false
    if (this.appDetails.lockedBy == this.authServ.getUserName()) {
      let applications: any[] = this.authServ.getUserApplications()
      let doesUserOwnTheApplication: boolean = false
      applications.forEach(app => {
        if (app.application == this.appName && app.customerId == this.airlineName) {
          doesUserOwnTheApplication = true
        }
      })
      if (applications != null && applications != undefined
        && doesUserOwnTheApplication) {
        this.isEditAllowed = true;
      }
    }
  }

  isUserAllowedToCreateNewApplication() {
    return this.authServ.isUserAllowedToCreateNewApplication()
  }

  removeApp() {
    let result = confirm("Are you sure you want to delete draft SIA for '" + this.appName + "' ?")

    if (result) {
      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appDetailServ.removeApp(this.appName, this.airlineName).subscribe(appList => {
        HoldOn.close();
        bootbox.alert({
          title: "Success",
          size: "small",
          message: "Draft for SIA '" + this.appName + "' deleted successfully"
        });
        this.router.navigate(["../"], { relativeTo: this.route });
      },
        error => {
          console.error(error);
          let message = "Failed to delete draft SIA for '" + this.appName + "'"
          if (error.errorMessage) {
            message = error.errorMessage
          }
          HoldOn.close();
          bootbox.alert({
            title: "Error",
            size: "small",
            message: message
          });
        });

    }

  }

  unlockApp() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appDetailServ.unlockApp(this.appName, this.airlineName).subscribe(resp => {
      this.appDetails = resp;
      this.airlineName = this.appDetails.airlineName;
      this.isEditAllowed = false;
      this.isEditAllowedForApplication()
      this.getAppList();
      HoldOn.close();
    },
      error => {
        console.error(error);
        let message = "Failed to unlock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
      }
    )

  }

  lockApp() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appDetailServ.lockApp(this.appName, this.airlineName).subscribe(resp => {
      this.appDetails = resp;
      this.airlineName = this.appDetails.airlineName;
      this.isEditAllowed = true;
      this.isEditAllowedForApplication()
      this.getAppList();
      HoldOn.close();
    },
      error => {
        console.error(error);
        let message = "Failed to lock application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
      }
    )

  }

  onAppChange(event) {
    this.appDetails = event
  }

  reviewMode() {
    sessionStorage.removeItem("tabNumber");
    sessionStorage.setItem("tabNumber", "1");
    this.router.navigate(["../../../review/review-details/" + this.airlineName + "|" + this.appName ], { relativeTo: this.route });
  }

  returnToEditMode() {
    sessionStorage.removeItem("tabNumber");
    sessionStorage.setItem("tabNumber", "1");
    this.router.navigate(["../../../my-applications/app-details/" + this.airlineName + "|" + this.appName ], { relativeTo: this.route });
  }


}

declare let HoldOn: any;
declare let bootbox: any;
const diff = require('rich-text-diff')
