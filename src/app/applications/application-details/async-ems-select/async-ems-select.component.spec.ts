import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsyncEmsSelectComponent } from './async-ems-select.component';

describe('AsyncEmsSelectComponent', () => {
  let component: AsyncEmsSelectComponent;
  let fixture: ComponentFixture<AsyncEmsSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsyncEmsSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsyncEmsSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
