import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ApplicationDetailsService } from '../application-details.service';
import { EMSService } from '../../../shared/models/ems-service-model';

@Component({
  selector: 'app-async-ems-select',
  templateUrl: './async-ems-select.component.html',
  styleUrls: ['./async-ems-select.component.css']
})
export class AsyncEmsSelectComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private appServ: ApplicationDetailsService) { }

  private destinationName: string = ""
  private destinationType: string = ""
  private emsInstance: string = ""
  private customer: string = ""

  private sourceDestinationName: string = ""
  private targetDestinationName: string = ""

  private durableName: string = ""

  private emsTechnicalUser: string = ""

  carrier = ""
  applicationName = ""
  serviceName = ""

  selectionList = ["Destinations", "Bridges", "Durables", "UserPermissions"]
  currentPosition = 0
  previous = -1
  next = 1

  private completeData
  data: any[]

  env = "TST"

  // array of currently selected entities in the data table
  selectedEntities: any[] = []

  tooltipMessage = "No destinations selected"

  selectedDestinations: any[] = []
  isTopicPresent: boolean = false
  selectedBridges: any[] = []
  selectedDurables: any[] = []
  selectedUserPerms: any[] = []

  emsDetailsList : EMSService[] = []

  ngOnInit() {

    // CustomerName|ApplicationName|ServiceName
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        let split: string[] = params['id'].split("|")
        this.carrier = split[0];
        this.applicationName = split[1]
        this.serviceName = split[2]
      }
      this.getDestinations()

    });
  }

  getDestinations() {

    this.data = undefined
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getAllDestinations(this.env).subscribe(
      respList => {
        this.data = []
        respList.forEach(element => {
          if (element.customer == this.carrier) {
            this.data.push(element)
          }
        });

        //this.data = respList;
        this.completeData = this.data
        console.log(this.completeData);
        HoldOn.close();
      }, error => {
        HoldOn.close();
        let message = "Failed to get EMS Destinations"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
      }
    )
  }

  filterDestination() {
    this.data = []
    this.completeData.forEach(element => {
      if (element.customer.toLowerCase().includes(this.customer.toLowerCase())
        && element.emsInstance.toLowerCase().includes(this.emsInstance.toLowerCase())
        && element.emsDestinationType.toLowerCase().includes(this.destinationType.toLowerCase())
        && element.emsDestinationName.toLowerCase().includes(this.destinationName.toLowerCase())) {
        this.data.push(element)
      }
    });
  }


  getBridges() {
    this.data = undefined
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getAllBridges(this.env).subscribe(respList => {
      //this.data = respList;
      this.data = []
      respList.forEach(elementBridge => {
        this.selectedDestinations.forEach(elementDest => {
          if (elementBridge.sourceDestination.emsDestinationType == elementDest.emsDestinationType &&
            elementBridge.sourceDestination.emsDestinationName == elementDest.emsDestinationName &&
            elementBridge.sourceDestination.emsInstance == elementDest.emsInstance) {
            this.data.push(elementBridge)
            return
          }
        })
      });
      this.completeData = this.data
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get EMS Bridges"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });
  }

  filterBridges() {
    this.data = []
    this.completeData.forEach(element => {
      if (element.emsInstance.toLowerCase().includes(this.emsInstance.toLowerCase())
        && element.sourceDestination.emsDestinationName.toLowerCase().includes(this.sourceDestinationName.toLowerCase())
        && element.targetDestination.emsDestinationName.toLowerCase().includes(this.targetDestinationName.toLowerCase())) {
        this.data.push(element)
      }
    });
  }

  getDurables() {
    this.data = undefined
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getAllDurables(this.env).subscribe(respList => {
      //this.data = respList;
      this.data = []
      respList.forEach(elementDurable => {
        this.selectedDestinations.forEach(elementDest => {
          if (elementDurable.destination.emsDestinationType == elementDest.emsDestinationType &&
            elementDurable.destination.emsDestinationName == elementDest.emsDestinationName &&
            elementDurable.destination.emsInstance == elementDest.emsInstance) {
            this.data.push(elementDurable)
            return
          }
        })
      });
      this.completeData = this.data
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get EMS Destinations"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });
  }

  filterDurables() {
    this.data = []
    this.completeData.forEach(element => {
      if (element.durableName.toLowerCase().includes(this.durableName.toLowerCase())
        && element.emsInstance.toLowerCase().includes(this.emsInstance.toLowerCase())
        && (element.destination && element.destination.emsDestinationName.toLowerCase().includes(this.destinationName.toLowerCase()))) {
        this.data.push(element)
      }
    });
  }




  getAllTechUsers() {
    this.data = undefined
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.getAllTechUsers(this.env).subscribe(respList => {
      //this.data = respList;
      this.data = []
      respList.forEach(elementTech => {
        this.selectedDestinations.forEach(elementDest => {
          if (elementTech.destination.emsDestinationType == elementDest.emsDestinationType &&
            elementTech.destination.emsDestinationName == elementDest.emsDestinationName &&
            elementTech.destination.emsInstance == elementDest.emsInstance) {
            this.data.push(elementTech)
            return
          }
        })
      });
      this.completeData = this.data
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get EMS Destinations"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });
  }



  filterTechUsers() {
    this.data = []
    this.completeData.forEach(element => {
      if (element.emsTechnicalUser.toLowerCase().includes(this.emsTechnicalUser.toLowerCase())
        && (element.destination && element.destination.emsInstance.toLowerCase().includes(this.emsInstance.toLowerCase()))
        && (element.destination && element.destination.emsDestinationName.toLowerCase().includes(this.destinationName.toLowerCase()))
        && (element.destination && element.destination.emsDestinationType.toLowerCase().includes(this.destinationType.toLowerCase()))) {
        this.data.push(element)
      }
    });
  }









  // function to handle data/entities selected/deselected in the table 
  public setSelectedEntities($event: any) {
    this.selectedEntities = $event;

    if (this.currentPosition == 0) {
      this.tooltipMessage = "Selected Destinations: \n";
      this.selectedEntities.forEach( element => {
        this.tooltipMessage = this.tooltipMessage + element.emsDestinationName + " ( " + element.emsDestinationType + " ) \n"
      })
    }
  }

  nextCatalog() {
    if (this.currentPosition == 0) {
      this.selectedDestinations = this.selectedEntities;
      if (this.selectedDestinations.length == 0) {
        bootbox.alert({
          title: "Error",
          size: "small",
          message: "Please select destinations"
        });
        return
      }
      this.selectedDestinations.forEach(
        element => {
          if (element.emsDestinationType == "Topic") {
            this.isTopicPresent = true;
          }
        }
      )
      this.getBridges()
      this.previous = this.currentPosition
      this.currentPosition = this.next
      if (this.isTopicPresent) {
        this.next++
      } else {
        this.next = this.next + 2
      }

    } else if (this.currentPosition == 1) {
      this.selectedBridges = this.selectedEntities;
      if (this.isTopicPresent) {
        this.getDurables()

      } else {
        this.getAllTechUsers()

      }

      this.previous = this.currentPosition
      this.currentPosition = this.next
      this.next++
    } else if (this.currentPosition == 2) {
      this.selectedDurables = this.selectedEntities;
      this.getAllTechUsers()
      this.previous = this.currentPosition
      this.currentPosition = this.next
      this.next++
    }
    this.selectedEntities = []
  }

  previousCatalog() {
    if (this.currentPosition == 1) {
      this.selectedEntities = this.selectedDestinations;
      this.getDestinations()

      this.next = this.currentPosition
      this.currentPosition = this.previous
      this.previous--

    } else if (this.currentPosition == 2) {
      this.getBridges()

      this.next = this.currentPosition
      this.currentPosition = this.previous
      this.previous--

    } else if (this.currentPosition == 3) {

      if (this.isTopicPresent) {
        this.getDurables()
        this.next = this.currentPosition
        this.currentPosition = this.previous
        this.previous--

      } else {
        this.getAllTechUsers()
        this.next = this.currentPosition
        this.currentPosition = this.previous
        this.previous = this.previous - 2

      }


      
    }
  }

  submitEMSDetails() {

    this.selectedUserPerms = this.selectedEntities

    this.selectedDestinations.forEach ( elementDestination => {
      let emsDetail : EMSService = new EMSService()
      emsDetail.emsDestinationName = elementDestination.emsDestinationName
      emsDetail.emsDestinationType = elementDestination.emsDestinationType
      emsDetail.emsDestinationProp = elementDestination.emsDestinationProperties
      emsDetail.emsDestinationFullStrategy = elementDestination.emsDestinationFullStrategy
      emsDetail.deployedInInstance = elementDestination.emsInstance

      let durableName : string = ""
      let durableProp : string = ""
      this.selectedDurables.forEach ( elementDurable => {
        if (elementDurable.destination.emsDestinationName == emsDetail.emsDestinationName 
              && elementDurable.destination.emsDestinationType == emsDetail.emsDestinationType 
              && elementDurable.destination.emsInstance == emsDetail.deployedInInstance) {
                  durableName = durableName + elementDurable.durableName + "   "
                  if (elementDurable.durableProperties != null)
                     durableProp = durableProp + elementDurable.durableName + "(" + elementDurable.durableProperties + ")   "
              }
      })
      emsDetail.emsDurableName = durableName
      emsDetail.emsDurableProp = durableProp

      let bridgedDestinationName : string = ""
      let bridgedDestinationType : string = ""
      this.selectedBridges.forEach ( element => {
        if (element.sourceDestination.emsDestinationName == emsDetail.emsDestinationName 
              && element.sourceDestination.emsDestinationType == emsDetail.emsDestinationType 
              && element.sourceDestination.emsInstance == emsDetail.deployedInInstance) {
                  bridgedDestinationName = bridgedDestinationName + element.targetDestination.emsDestinationName + "   "
                  if (element.targetDestination.emsDestinationType != null)
                     bridgedDestinationType = bridgedDestinationType + element.targetDestination.emsDestinationName 
                                              + "(" + element.targetDestination.emsDestinationType + ")   "
              }
      })
      emsDetail.bridgedDestinationName = bridgedDestinationName
      emsDetail.bridgedDestinationType = bridgedDestinationType


      let emsTechUserGroup : string = ""
      let userPermission : string = ""
      this.selectedUserPerms.forEach ( element => {
        if (element.destination.emsDestinationName == emsDetail.emsDestinationName 
              && element.destination.emsDestinationType == emsDetail.emsDestinationType 
              && element.destination.emsInstance == emsDetail.deployedInInstance) {
                emsTechUserGroup = emsTechUserGroup + element.emsTechnicalUser + "   "
                if (element.permission != null)
                   userPermission = userPermission + element.emsTechnicalUser + "(" + element.permission + ")   "
              }
      })
      emsDetail.emsTechUserGroup = emsTechUserGroup
      emsDetail.userPermission = userPermission

      this.emsDetailsList.push(emsDetail);

    })




    //this.appServ.addEMSDetails()


    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appServ.addEMSDetails(this.carrier, this.applicationName, this.serviceName, this.emsDetailsList).subscribe(
      response => {
        HoldOn.close()
        bootbox.alert({
          title: "Success",
          size: "small",
          message: "SIA updated successfully. Selected EMS Details have been added successfully. "
        });
        sessionStorage.setItem("tabNumber","4");
        this.router.navigate(["../../" + "app-details/" + this.carrier + "|" + this.applicationName], { relativeTo: this.route });

      }, error => {
        HoldOn.close();
        let message = "Failed to update SIA with the selected EMS Details."
        if (error.errorMessage) {
          message = error.errorMessage
        }
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
      }
    )

  } 

}

function isBigEnough(element, index, array) {
  return (element >= 10);
}
declare let bootbox: any;
declare let HoldOn: any;
