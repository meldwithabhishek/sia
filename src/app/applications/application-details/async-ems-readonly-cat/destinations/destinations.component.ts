import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ApplicationDetailsService } from '../../application-details.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.css']
})
export class DestinationsComponent implements OnInit, OnChanges {

  
  ngOnChanges(changes: SimpleChanges): void {
    if (this.env) {
      this.getDestinations()
    }
  }

  @Input('env')
  env : string

  // data supplied to the data table
  private completeData
  data: any[];

  private destinationName : string = ""
  private destinationType : string = ""
  private emsInstance : string = ""
  private customer : string = ""
  // array of currently selected entities in the data table
  selectedEntities: Map<String, String>[];

  constructor(private route: ActivatedRoute, private router: Router, private appServ: ApplicationDetailsService) { }

  ngOnInit(): void {
    if (this.env) {
      this.getDestinations()
    }
    
  }

  getDestinations() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });    
    this.appServ.getAllDestinations(this.env).subscribe(respList => {
      this.data = respList;
      this.completeData = this.data  
      console.log(this.completeData)
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get EMS Destinations"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });
  }

  // function to handle data/entities selected/deselected in the table 
  public setSelectedEntities($event: any) {
    this.selectedEntities = $event;
  }
  
  filter() {
    this.data   = []
    let count =0;
    this.completeData.forEach(element => {
      console.log(element)
      if (element.customer.toLowerCase().includes(this.customer.toLowerCase()) 
          && element.emsInstance.toLowerCase().includes(this.emsInstance.toLowerCase())
          && element.emsDestinationType.toLowerCase().includes(this.destinationType.toLowerCase())
          && element.emsDestinationName.toLowerCase().includes(this.destinationName.toLowerCase())) {
        this.data.push(element)
      }
    });  
  }

}

function isBigEnough(element, index, array) { 
  return (element >= 10); 
}
declare let bootbox: any;
declare let HoldOn: any;
