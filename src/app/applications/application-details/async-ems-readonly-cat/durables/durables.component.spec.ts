import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DurablesComponent } from './durables.component';

describe('DurablesComponent', () => {
  let component: DurablesComponent;
  let fixture: ComponentFixture<DurablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DurablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DurablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
