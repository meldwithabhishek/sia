import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationDetailsService } from '../../application-details.service';

@Component({
  selector: 'app-user-permissions',
  templateUrl: './user-permissions.component.html',
  styleUrls: ['./user-permissions.component.css']
})
export class UserPermissionsComponent implements OnInit {

  ngOnChanges(changes: SimpleChanges): void {
    if (this.env) {
      this.getAllTechUsers()
    }
  }

  @Input('env')
  env : string

  // data supplied to the data table
  private completeData
  data: any[];

  private destinationName : string = ""
  private destinationType : string = ""
  private emsInstance : string = ""
  private emsTechnicalUser : string = ""
  
  // array of currently selected entities in the data table
  selectedEntities: Map<String, String>[];

  constructor(private route: ActivatedRoute, private router: Router, private appServ: ApplicationDetailsService) { }

  ngOnInit(): void {
    if (this.env) {
      this.getAllTechUsers()
    }
    
  }

  getAllTechUsers() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });    
    this.appServ.getAllTechUsers(this.env).subscribe(respList => {
      this.data = respList;
      this.completeData = this.data      
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get EMS Destinations"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });
  }

  // function to handle data/entities selected/deselected in the table 
  public setSelectedEntities($event: any) {
    this.selectedEntities = $event;
  }
  
  filter() {
    this.data   = []
    this.completeData.forEach(element => {
      if (element.emsTechnicalUser.toLowerCase().includes(this.emsTechnicalUser.toLowerCase()) 
          && (element.destination && element.destination.emsInstance.toLowerCase().includes(this.emsInstance.toLowerCase()))         
          && (element.destination && element.destination.emsDestinationName.toLowerCase().includes(this.destinationName.toLowerCase()))
          && (element.destination && element.destination.emsDestinationType.toLowerCase().includes(this.destinationType.toLowerCase()))) {
        this.data.push(element)
      }
    });  
  }

}

function isBigEnough(element, index, array) { 
  return (element >= 10); 
}
declare let bootbox: any;
declare let HoldOn: any;