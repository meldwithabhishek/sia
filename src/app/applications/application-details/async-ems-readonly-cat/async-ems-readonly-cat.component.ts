import { ApplicationDetailsService } from './../application-details.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-async-ems-readonly-cat',
  templateUrl: './async-ems-readonly-cat.component.html',
  styleUrls: ['./async-ems-readonly-cat.component.css']
})
export class AsyncEmsReadonlyCatComponent implements OnInit {

  public env = "TST"
  // data supplied to the data table
  private completeData
  data: any[];

  private customer : string = ""
  private processName : string = ""
  // array of currently selected entities in the data table
  selectedEntities: Map<String, String>[];

  constructor(private route: ActivatedRoute, private router: Router, private appServ: ApplicationDetailsService) { }

  ngOnInit() {
    this.env = "TST"    
  }

 

}


declare let bootbox: any;
declare let HoldOn: any;