import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationDetailsService } from '../../application-details.service';

@Component({
  selector: 'app-bridges',
  templateUrl: './bridges.component.html',
  styleUrls: ['./bridges.component.css']
})
export class BridgesComponent implements OnInit {

  ngOnChanges(changes: SimpleChanges): void {
    if (this.env) {
      this.getBridges()
    }
  }

  @Input('env')
  env : string

  // data supplied to the data table
  private completeData
  data: any[];

  private sourceDestinationName : string = ""
  private targetDestinationName : string = ""
  private emsInstance : string = ""
  // array of currently selected entities in the data table
  selectedEntities: Map<String, String>[];

  constructor(private route: ActivatedRoute, private router: Router, private appServ: ApplicationDetailsService) { }

  ngOnInit(): void {
    if (this.env) {
      this.getBridges()
    }
    
  }

  getBridges() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });    
    this.appServ.getAllBridges(this.env).subscribe(respList => {
      this.data = respList;
      this.completeData = this.data      
      HoldOn.close();
    }, error => {
      HoldOn.close();
      let message = "Failed to get EMS Bridges"
      if (error.errorMessage) {
        message = error.errorMessage
      }
      bootbox.alert({
        title: "Error",
        size: "small",
        message: message
      });
    });
  }

  // function to handle data/entities selected/deselected in the table 
  public setSelectedEntities($event: any) {
    this.selectedEntities = $event;
  }
  
  filter() {
    this.data   = []
    this.completeData.forEach(element => {
      if (element.emsInstance.toLowerCase().includes(this.emsInstance.toLowerCase())
          && element.sourceDestination.emsDestinationName.toLowerCase().includes(this.sourceDestinationName.toLowerCase())
          && element.targetDestination.emsDestinationName.toLowerCase().includes(this.targetDestinationName.toLowerCase())) {
        this.data.push(element)
      }
    });  
  }

}

function isBigEnough(element, index, array) { 
  return (element >= 10); 
}
declare let bootbox: any;
declare let HoldOn: any;
