import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsyncEmsReadonlyCatComponent } from './async-ems-readonly-cat.component';

describe('AsyncEmsReadonlyCatComponent', () => {
  let component: AsyncEmsReadonlyCatComponent;
  let fixture: ComponentFixture<AsyncEmsReadonlyCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsyncEmsReadonlyCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsyncEmsReadonlyCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
