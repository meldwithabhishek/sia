import { MatSelectModule } from '@angular/material/select';
import { AppDetailsComponent } from './application-details/app-details/app-details.component';
import { ApplicationDetailsService } from './application-details/application-details.service';
import { ApplicationsComponent } from './applications.component';
import { ApplicationsRoutingModule } from './applications-routing.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { SelectModule } from 'ng2-select';
import { ChannelInfoComponent } from './application-details/channel-info/channel-info.component';
import { SyncServicesComponent } from './application-details/sync-services/sync-services.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AsyncServicesComponent } from './application-details/async-services/async-services.component';
import { BatchServicesComponent } from './application-details/batch-services/batch-services.component';
import { SearchParamsComponent } from './application-details/search-params/search-params.component';
import { FormsModule } from "@angular/forms";
import { IntroComponent } from './application-details/intro/intro.component';
import { CreateApplicationComponent } from './create-application/create-application.component';
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { SyncServCatComponent } from './application-details/sync-serv-cat/sync-serv-cat.component';
import { DataTableModule } from "ng2-data-table";
import { ReviewComponent } from './review/review.component';
import { ApprovalComponent } from './approval/approval.component';
import { BatchServCatComponent } from './application-details/batch-serv-cat/batch-serv-cat.component';
import { FileUploadModule } from 'ng2-file-upload/file-upload/file-upload.module';
import { AsyncServCatComponent } from './application-details/async-serv-cat/async-serv-cat.component';
import { AttachmentFilesComponent } from './attachment-files/attachment-files.component';
import { MatTabsModule } from '@angular/material/tabs';
import { E2eIntroComponent } from './application-details/e2e-intro/e2e-intro.component';
// remove /src for local deployments. It will be present for all weblogic deployments. In the UI switch import.
import { UiSwitchModule } from 'ngx-toggle-switch';
// import { UiSwitchModule } from 'ngx-toggle-switch';
import { ExportOptionsComponent } from './export-options/export-options.component';
import { PdfExportComponent } from './export-options/pdf-export/pdf-export.component';
import { AsyncEmsReadonlyCatComponent } from './application-details/async-ems-readonly-cat/async-ems-readonly-cat.component';
import { DestinationsComponent } from './application-details/async-ems-readonly-cat/destinations/destinations.component';
import { DurablesComponent } from './application-details/async-ems-readonly-cat/durables/durables.component';
import { BridgesComponent } from './application-details/async-ems-readonly-cat/bridges/bridges.component';
import { UserPermissionsComponent } from './application-details/async-ems-readonly-cat/user-permissions/user-permissions.component';
import { AsyncEmsSelectComponent } from './application-details/async-ems-select/async-ems-select.component';
import { QuestionnaireGeneratorComponent } from './export-options/questionnaire-generator/questionnaire-generator.component';


/*import {BrowserAnimationsModule} from '@angular/platform-browser/animations';*/

/*import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule, MdNativeDateModule} from '@angular/material';
import {HttpModule} from '@angular/http';*/

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    ApplicationsRoutingModule,
    SelectModule,
    AccordionModule,
    FileUploadModule,
    FormsModule,
    MatTabsModule,
    MatSelectModule,
    TooltipModule.forRoot(),
    UiSwitchModule

    /*,
    BrowserAnimationsModule/*,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MdNativeDateModule,
    HttpModule*/
  ],
  declarations: [
    ApplicationDetailsComponent,
    ApplicationsComponent,
    AppDetailsComponent,
    ChannelInfoComponent,
    SyncServicesComponent, AsyncServicesComponent, BatchServicesComponent, SearchParamsComponent, IntroComponent, CreateApplicationComponent, SyncServCatComponent, ReviewComponent, ApprovalComponent, BatchServCatComponent, AsyncServCatComponent, AttachmentFilesComponent, E2eIntroComponent, ExportOptionsComponent, PdfExportComponent, AsyncEmsReadonlyCatComponent, DestinationsComponent, DurablesComponent, BridgesComponent, UserPermissionsComponent, AsyncEmsSelectComponent, QuestionnaireGeneratorComponent
  ],
  providers: [ApplicationDetailsService],
  schemas: []
})
export class ApplicationsModule { }
