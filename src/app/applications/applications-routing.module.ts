import { AsyncEmsReadonlyCatComponent } from './application-details/async-ems-readonly-cat/async-ems-readonly-cat.component';
import { AsyncServCatComponent } from './application-details/async-serv-cat/async-serv-cat.component';
import { BatchServCatComponent } from './application-details/batch-serv-cat/batch-serv-cat.component';
import { ApprovalComponent } from './approval/approval.component';
import { SyncServCatComponent } from './application-details/sync-serv-cat/sync-serv-cat.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { ApplicationsComponent } from './applications.component';
import { AsyncEmsSelectComponent } from './application-details/async-ems-select/async-ems-select.component';

const appRoutes: Routes = [
  {
    path: '',
    component: ApplicationsComponent,
    children: [
      { path: '', redirectTo: 'app-details', pathMatch: 'full' },
      { path: 'app-details', component: ApplicationDetailsComponent },
      { path: 'review-details/:id', component: ApplicationDetailsComponent },
      { path: 'app-details/:id', component: ApplicationDetailsComponent },
      { path: 'sync-catalogue', component: SyncServCatComponent },
      { path: 'batch-catalogue', component: BatchServCatComponent },
      { path: 'async-catalogue', component: AsyncServCatComponent },
      { path: 'async-ems-catalogue', component: AsyncEmsReadonlyCatComponent },
      { path: 'select-async-ems-details/:id', component: AsyncEmsSelectComponent },
      { path: 'sync-catalogue/:id', component: SyncServCatComponent }
    ]
  },
  {
    path: 'pending-approvals',
    component: ApplicationsComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: ApprovalComponent },
      { path: 'app-details/:id', component: ApplicationDetailsComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ApplicationsRoutingModule { }