import { UserModel } from './../../shared/models/auth-models/user-model';
import { AuthService } from './../../shared/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationDetailsService } from './../application-details/application-details.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-create-application',
  templateUrl: './create-application.component.html',
  styleUrls: ['./create-application.component.css']
})
export class CreateApplicationComponent implements OnInit {

  @ViewChild('myModal')
  myModal 

   airlineList: any[] = []
   appName : string = ""
   customer : string = ""
   showCarrierOption = true;
   carrier : string = ""

  constructor(private appListServ: ApplicationDetailsService, private route: ActivatedRoute,
     private router: Router, private authServ : AuthService) { }

  ngOnInit() {

    if (this.carrier == "") {
      this.carrier = this.authServ.getUserCarrier()
      if ('1A' != this.carrier && 'LX' != this.carrier ) {
        this.customer = this.carrier
        this.showCarrierOption = false
      }
    }
    

     HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appListServ.getAirlinesList().subscribe(resp => {
      if ('LX' == this.carrier) {
        this.airlineList = []
        resp.forEach(element => {
          if (element.name == "LX" || element.name == "SN") {
            this.airlineList.push(element)
          }
        });

      } else {
        this.airlineList = resp;
      }
      
      HoldOn.close();
    },
      error => {
        console.error(error);
        HoldOn.close();
      });


  }

  trimInput(event : Event) {
    this.appName = this.appName.trim()

  }
  

  onSubmit() {

   
    
    HoldOn.open({
      theme: "sk-rect",
      message: 'Loading...'
    });
    this.appListServ.createApplication({ "name" : this.appName, "airlineName" : this.customer }).subscribe(
      response => {        
        this.authServ.updateCurrentUser().subscribe( 
          resp => {
            let user : UserModel = JSON.parse(sessionStorage.getItem('currentUser'));
            user.applications = resp.applications
            sessionStorage.setItem("currentUser",JSON.stringify(user))
            HoldOn.close();
            document.getElementById("dismissModal").click();
            this.router.navigate(["/dashboard/my-applications/app-details/" + this.customer + "|" +this.appName]);
          },
          error=> {
            console.error(error);
            HoldOn.close();
          }
        )
        
      },
      error => {
        HoldOn.close();
        let message = "Failed to create application.";
        if (error.errorMessage) {
          message = error.errorMessage
        }
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
             
      }
    );

  }

}

declare let HoldOn: any;
declare let bootbox: any;