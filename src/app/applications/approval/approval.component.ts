import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationDetailsService } from './../application-details/application-details.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-approval',
  templateUrl: './approval.component.html',
  styleUrls: ['./approval.component.css']
})
export class ApprovalComponent implements OnInit {

  unapprovedApplications : any  

  constructor(private appDetailServ: ApplicationDetailsService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appDetailServ.getApplicationsPendingApproval().subscribe(appList => {
      this.unapprovedApplications = appList
      HoldOn.close();      
    },
      error => {
        this.unapprovedApplications = []
        console.error(error);
        HoldOn.close();
        let message = "Failed to get applications for approval"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });   
      });
  }

  onClick(app : string, customer : string) {
    this.router.navigate(["../app-details/" + customer + "|" + app ], { relativeTo: this.route });
  }

}


declare let HoldOn: any;
declare let bootbox: any;