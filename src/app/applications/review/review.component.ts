import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApplicationDetailsService } from '../application-details/application-details.service';
import { ConfigurationService } from '../../shared/services/configuration.service';
import { AuthService } from '../../shared/services/auth.service';


@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  @Input("appName")
  appName: string

  @Input("customer")
  customer: string

  @Input("path")
  path: string

  @Input("isEditAllowed")
  isEditAllowed: boolean

  @Input('appDetails')
  appDetails: any;

  @Output() onFormChanged = new EventEmitter<any>();

  comments: any[]

  submissionComment: string = ""

  validationFailures


  constructor(private route: ActivatedRoute, private router: Router,
    private appDetailServ: ApplicationDetailsService,
    private configService: ConfigurationService, public authServ: AuthService) { }

  ngOnInit() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appDetailServ.getComments(this.appName, this.customer).subscribe(commentList => {
      this.comments = commentList
      HoldOn.close();
      //this.router.navigate(["../"], { relativeTo: this.route });
    },
      error => {
        console.error(error);
        let message = "Failed to delete application '" + this.appName + "'"
        if (error.errorMessage) {
          message = error.errorMessage
        }
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: message
        });
      });

  }

  onSave(event) {
    if (this.path == "my-applications") {
      this.validationFailures = undefined
      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appDetailServ.submitForReview(this.appName, this.customer, { "comment": this.submissionComment }).subscribe(commentList => {
        this.comments = commentList
        this.appDetailServ.unlockApp(this.appName, this.customer).subscribe(
          resp => {
            HoldOn.close();
            this.onFormChanged.emit(resp  )
            //this.router.navigate(["../"], { relativeTo: this.route });
            bootbox.alert({
              title: "Success",
              size: "small",
              message: "Successfully submitted for Review"
            });
          },
          error => {
            let message = "Failed to submit SIA '" + this.appName + "' for Review"
            if (error.errorMessage) {
              message = error.errorMessage
            }
            HoldOn.close();
            bootbox.alert({
              title: "Error",
              size: "small",
              message: message
            });
          }
        )
      },
        error => {
          HoldOn.close();          
          if (error[0]) {   
            this.validationFailures = error
            bootbox.alert({
              title: "Error",
              size: "small",
              message: "SIA cannot be submitted for review due to data issues"
            });
          } else {
            let message = "Failed to submit SIA '" + this.appName + "' for Review"
            if (error.errorMessage) {
              message = error.errorMessage
            }

            bootbox.alert({
              title: "Error",
              size: "small",
              message: message
            });
          }
        });
    } else {
      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appDetailServ.returnToOwner(this.appName, this.customer, { "comment": this.submissionComment }).subscribe(commentList => {
        this.comments = commentList
        HoldOn.close();
        this.router.navigate([""]);
        bootbox.alert({
          title: "Success",
          size: "small",
          message: "Successfully updated the issue"
        });
      },
        error => {
          console.error(error);
          let message = "Failed to raise issue on SIA '" + this.appName + "'"
          if (error.errorMessage) {
            message = error.errorMessage
          }
          HoldOn.close();
          this.router.navigate(["../"]);
          bootbox.alert({
            title: "Error",
            size: "small",
            message: message
          });
        });
    }
  }

  approveSIA() {
    let val: boolean = confirm("Are you sure? This will baseline the SIA '" + this.appDetails.name + " | " + this.appDetails.version + "'")
    if (val) {
      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.appDetailServ.approve(this.appName, this.customer,).subscribe(commentList => {
        this.comments = commentList
        HoldOn.close();
        this.router.navigate([""]);
        bootbox.alert({
          title: "Success",
          size: "small",
          message: "Successfully approved"
        });
      },
        error => {
          console.error(error);
          let message = "Failed to approve SIA '" + this.appDetails.name + " | " + this.appDetails.version + "'"
          if (error.errorMessage) {
            message = error.errorMessage
          }
          HoldOn.close();
          bootbox.alert({
            title: "Error",
            size: "small",
            message: message
          });
        });
    }

  }

}

declare let HoldOn: any;
declare let bootbox: any;