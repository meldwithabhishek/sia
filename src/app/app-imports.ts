import { AdminGuardService } from './shared/routeguard/admin-guard.service';
import { UserManagementComponent } from './user-management/user-management.component';
import { UserManagementService } from './user-management/user-management.service';
import { ApplicationsComponent } from './applications/applications.component';
import { HeaderComponent } from './header/header.component';
import { AuthService } from './shared/services/auth.service';
import { ConfigurationService } from './shared/services/configuration.service';
import { AuthGuard } from './shared/routeguard/auth-guard.service';
import { AlertService } from './alert/alert.service';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { AuthLoginComponent } from './auth/auth-login/auth-login.component';
import { AlertComponent } from './alert/alert.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidenavComponent } from './sidenav/sidenav.component';

export var COMPONENTS = [
    AppComponent,
    AuthComponent,
    AuthLoginComponent,
    AlertComponent,
    DashboardComponent,
    SidenavComponent,
    HeaderComponent,
    UserManagementComponent
];

export var SERVICE_PROVIDERS = [
    AuthGuard,
    AdminGuardService,
    AlertService,
    ConfigurationService,
    AuthService,
    UserManagementService
]