import { ConfigurationService } from './../../shared/services/configuration.service';
import { AuthService } from './../../shared/services/auth.service';
import { UserModel } from './../../shared/models/auth-models/user-model';
import { AlertService } from './../../alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./../auth.component.css']
})
export class AuthLoginComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private authService: AuthService,
    private alertService: AlertService, private config: ConfigurationService) {
    this.user = new UserModel();
    this.attempts = 3;
  }

  user: UserModel;
  loading: boolean;
  returnUrl: string;
  attempts: number;
  alreadyLoggedIn: boolean;
  carrierList: any[];

  ngOnInit() {

    let lastCarrier: string = localStorage.getItem("defaultCarrier")

    if (lastCarrier) {
      this.user.carrier = lastCarrier
    }



    // get return url from route parameters or default to '/'
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/dashboard']);
    } else {

      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.config.getAirlinesList().subscribe(resp => {
        this.carrierList = resp;
        HoldOn.close();
      },
        error => {
          console.error(error);
          HoldOn.close();
          /*let errorMessage = JSON.parse(error.errorMessage);
          bootbox.alert({
            title: "Error",
            size: "small",
            message: '<p><font color="red">' + 'Operation - ' + errorMessage.type + ' failed.' + '</font></p>'
          });*/
        });
    }
  }
  
  login() {
    this.alreadyLoggedIn = false;
    if (this.attempts > 0) {
      this.loading = true;
      this.authService.login({
        userName: this.user.userName, password: btoa(this.user.password)
        , carrier: this.user.carrier, environment: this.authService.getEnvironment()
      }).subscribe(
        userResp => {
          localStorage.setItem("defaultCarrier", this.user.carrier)
          //document.cookie = this.user.carrier
          this.user.password = undefined;
          this.user.roles = userResp.roles
          this.user.applications = userResp.applications
          sessionStorage.setItem("currentUser", JSON.stringify(this.user))
          this.router.navigate(['/dashboard']);
        },
        error => {
          if (error.errorMessage) {
            this.alertService.error(error.errorMessage);
          } else {
            this.alertService.error("Server Error, Contact ZR6 System Administrator.");
          }          
          this.loading = false;
          this.attempts = this.attempts - 1;
        });
    }
    else {
      this.alertService.error("Maximum login attempts reached. Contact ZR6 System Administrator.");
    }
  }


  logout() {
    this.alreadyLoggedIn = false;
    this.authService.logoutExistingSession(this.user).subscribe(data => {
      let response = JSON.parse(data.text());
      bootbox.alert({
        title: "Info",
        size: "small",
        message: response.message
      });
      this.router.navigate(['/auth']);
    },
      error => {
        bootbox.alert({
          title: "Info",
          size: "small",
          message: "Server Error",
        });
        this.router.navigate(['/auth']);
      });
  }

  doGuestLogin() {
    sessionStorage.setItem("isGuestLoggedIn", "true");
    this.router.navigate(['/dashboard']);

  }

}

declare let bootbox: any;
declare let HoldOn: any;
