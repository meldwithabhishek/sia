import { AuthService } from './../shared/services/auth.service';
import { Router, ActivatedRoute, Routes } from '@angular/router';
import { NavItems } from './../sidenav/nav-items-interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private authServ : AuthService) { }

  items: NavItems[];

  approvalCount : number = 0

  ngOnInit() {
    /*
      [{ 'name': 'SIA Records', 'route': 'applications' },
                  { 'name': 'Sync Service Catalogue', 'route': 'applications/sync-catalogue' },
                  { 'name': 'BW Async Service Catalogue', 'route': 'applications/async-catalogue' },
                  { 'name': 'EMS Async Service Catalogue', 'route': 'applications/async-ems-catalogue' },
                  { 'name': 'Batch Service Catalogue', 'route': 'applications/batch-catalogue' },
                  { 'name': 'E2E Incident Management', 'route': 'incident-management' }];    */
    this.items = [{ 'name': 'SIA Records', 'route': 'applications' },
                  { 'name': 'Sync Service Catalogue', 'route': 'applications/sync-catalogue' },
                  { 'name': 'Async Service Catalogue', 'route': 'applications/async-ems-catalogue' },
                  { 'name': 'Batch Service Catalogue', 'route': 'applications/batch-catalogue' },
                  { 'name': 'LHMDW-ZAMBAS Information', 'route': 'incident-management' }];
    if (this.authServ.isLoggedInUserAdmin()) {
      this.items.push({ 'name': 'User Management', 'route': 'user-management' });
    }
    /*
    if((this.authServ.getUserApplications() && this.authServ.getUserApplications().length != 0) || 
        (this.authServ.getUserRoles() && this.authServ.getUserRoles().includes("CREATE_NEW_APP"))) {
      this.items.push({ 'name': 'My SIA Records', 'route': 'my-applications' });
    }
    */
    if (this.authServ.isUserACapacityApprover()) {
      this.items.push({ 'name': 'Approvals', 'route': 'approvals/pending-approvals' });
    }
  }

  onItemClicked(item: NavItems) {
    this.router.navigate([item.route], { relativeTo: this.route });
  }

}

