import { ApplicationsModule } from './applications/applications.module';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CustomFormsModule } from 'ng2-validation'
import { TypeaheadModule } from "ngx-bootstrap/typeahead";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { TabsModule } from 'ngx-bootstrap/tabs';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { COMPONENTS, SERVICE_PROVIDERS } from './app-imports'

import { AppComponent } from './app.component';
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
import { UserManagementComponent } from './user-management/user-management.component';
import {MatSelectModule} from '@angular/material/select';



@NgModule({
  declarations: [
    COMPONENTS
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    HttpModule,
    MatSelectModule,
    AppRoutingModule,
    CustomFormsModule,
    ApplicationsModule,
    BrowserAnimationsModule,
    TypeaheadModule.forRoot(),
    TooltipModule.forRoot(),
    TabsModule.forRoot()
  ],
  providers: [
    SERVICE_PROVIDERS,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
