import { AdminGuardService } from './shared/routeguard/admin-guard.service';
import { UserManagementComponent } from './user-management/user-management.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './shared/routeguard/auth-guard.service';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { AuthLoginComponent } from './auth/auth-login/auth-login.component';

export const appRoutes: Routes = [
  {
    path: '', redirectTo: 'auth', pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'applications', pathMatch: 'full' },
      { path: 'applications', loadChildren: 'app/applications/applications.module#ApplicationsModule' },
      { path: 'my-applications', loadChildren: 'app/applications/applications.module#ApplicationsModule' },
      { path: 'approvals', loadChildren: 'app/applications/applications.module#ApplicationsModule' },
      { path: 'review', loadChildren: 'app/applications/applications.module#ApplicationsModule' },
      { path: 'incident-management', loadChildren: 'app/applications/applications.module#ApplicationsModule' },
      { path: 'user-management', component : UserManagementComponent, canActivate : [AdminGuardService] }
    ]
  },
  {
    path: 'auth',
    component: AuthComponent,
    children: [
      { path: '', redirectTo: 'signin', pathMatch: 'full' },
      { path: 'signin', component: AuthLoginComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }

