export class BatchService {

	title

    serviceName: string;
	version: string;
	sensitive : boolean
	appRole: string;
	connectionInitiatedBy: string;
	comments: string;
	purposeOfUsage: string;
	criticality: string;
	workaround: string;
	criticalityJustification: string;
	dataWorthProtection
	ipAddPort: string;
	fileRetentionZR6
	filenamePatternZR6: string;
	filenameLocationZR6: string;
	filenamePatternApp: string;
	filenameLocationApp: string;
	fileTransferDirection: string;
	internetAccess
	meanFileSize: string;
	maxFileSize: string;
	meanFileTransferPerMonth: string;
	maxFileTransferPerMonth: string;
	ftpTransferDist: string;
    protocolUsed: string;
    
    constructor(){
		this.protocolUsed = "SFTP"
		this.fileRetentionZR6 = 7
    }
}