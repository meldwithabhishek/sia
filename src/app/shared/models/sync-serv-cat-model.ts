export class SyncServiceCat {
    
        id : number
        name : string
        version : string
        operation : string
        criticality : string
        
        lastModifiedBy  : string
        lastModifiedOn : string
    
        constructor(){
        }
    }
    