import { EMSService } from "./ems-service-model";

export class AsyncService {

	title

    serviceName : string;
	appRole : string;
	connectionInitiatedBy : string;
	comments : string;
	parameters : string
	purposeOfUsage : string
	criticality : string
	workaround : string
	criticalityJustification : string
	dataWorthProtection : string
	version : string
	sensitive : boolean
		

	lastModifiedBy  : string
	lastModifiedOn : string

	emsDetails : EMSService[] = []

    constructor(){
    }
}
