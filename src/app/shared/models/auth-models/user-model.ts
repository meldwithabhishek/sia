export class UserModel {

    id : number;
    userName : string;
    password : string;
    carrier : string;
    email : string
    roles : string[];
    applications : any[]

    constructor(username?,password?){
        this.userName = username;
        this.password = password;
    }
}
