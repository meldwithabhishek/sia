export class EMSService {
	
	meanCallsPerDay
	peakCallsPerDay
	meanCallsPerSec
	peakCallsPerSec
	dailyLoadDistributionPeriod : string
	

	emsDestinationType
	emsDestinationName
	emsDestinationProp
	emsDestinationPurpose
	emsDestinationFullStrategy
	deployedInInstance

	emsDurableName
	emsDurableProp

	emsTechUserGroup
	userPermission

	bridgedDestinationType
	bridgedDestinationName
	
    constructor(){
    }
}
