export class ChannelInfo {

    meanSessionsOpenedPerDay : string
	peakSessionsOpenedPerDay : string
	meanSessionsOpenedPerSecond : string
	peakSessionsOpenedPerSecond : string
	sessionUsageDistribution : string
	meanConcurrentSessionsOpen : string
	peakConcurrentSessionsOpen : string
	meanLoggingServiceCallsPerDay : string
	blacklistingLevel : string
	warningThreshold : string
	blacklistingThreshold : string
	callerIdPattern : string
	capiVersion : string
	

    constructor(){
    }
}
