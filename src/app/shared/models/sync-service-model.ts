import { SyncServiceCat } from './sync-serv-cat-model';
export class SyncService {

	title

	//syncCat : SyncServiceCat
	serviceName: string
	version: string
	operation: string
	appRole: string;
	connectionInitiatedBy: string;
	comments: string;
	parameters: string
	ipAddPort: string
	serviceUrl: string
	protocolUsed: string
	purposeOfUsage: string
	criticality: string
	workaround: string
	criticalityJustification: string
	dataWorthProtection: any
	sensitive: any
	meanCallsPerDay: any
	peakCallsPerDay: any
	meanCallsPerSec: any
	peakCallsPerSec: any
	dailyLoadDistributionPeriod: string
	provViaExtern: string
	maxCap: string


	lastModifiedBy: string
	lastModifiedOn: string

	constructor() {
	}
}
