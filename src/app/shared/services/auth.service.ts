import { ConfigurationService } from './configuration.service';
import { UserModel } from './../models/auth-models/user-model';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {

  constructor(private http: Http, private configService: ConfigurationService) { }

  getConfig() {
    return this.configService.getConfig();
  }

  login(param) {   
    //return this.http.post(this.getConfig().SIA_WEB_APP + 'Login', param, this.headers())
    return this.http.post(this.getConfig().SIA_WEB_APP + 'Login', param, this.headers())
      .map((response: Response) => response.json())
          .catch(error => Observable.throw(error.json()));
  }

  logout() {
    // remove user from session storage to log user out
    /*let user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user) {
      let params = new URLSearchParams();
      params.set('username', user.username);
      params.set('token', user.token);
      return this.http.post(this.getConfig().AD_BASE_URL + 'logout', params.toString(), this.headers())
        .map((response: Response) => {
          sessionStorage.removeItem('currentUser');
          return response;
        });
    }*/
    let options = new RequestOptions();
    let headers = new Headers();
    headers.set('Content-Type', 'application/json');
    headers.set('Requester', this.getUserName());
    options.headers = headers;

    sessionStorage.removeItem("isGuestLoggedIn");
    sessionStorage.removeItem("currentUser");

    return this.http.get(this.getConfig().SIA_WEB_APP + 'UnlockAllSIAs', options)
      .map((response: Response) => response.json())
          .catch(error => Observable.throw(error.json()));


    
  }

  unlockAllSIAs() {
    let options = new RequestOptions();
    let headers = new Headers();
    headers.set('Content-Type', 'application/json');
    headers.set('Requester', this.getUserName());
    options.headers = headers;
    return this.http.get(this.getConfig().SIA_WEB_APP + 'UnlockAllSIAs', options)
      .map((response: Response) => response.json())
          .catch(error => Observable.throw(error.json()));
  }

  updateCurrentUser() {       
    return this.http.get(this.getConfig().SIA_WEB_APP + 'User/' + this.getUserName(), this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  logoutExistingSession(user) {
    let params = new URLSearchParams();
    params.set('username', user.username);
    params.set('token', 'dummyToken');
    return this.http.post(this.getConfig().AD_BASE_URL + 'logout', params.toString(), this.headers())
      .map((response: Response) => {
        return response;
      });
  }


  getUserName() {
    let user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user) {
      return user.userName;
    }
  }

  getUserGroup() {
    let user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user) {
      return user.group;
    }
  }

  getUserCarrier() {
    let user : UserModel = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user) {
      return user.carrier;
    }
  }

  getEnvironment() {
    return "TEST";
  }

  getUserRoles() : string[] {
    let user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user) {
      return user.roles;
    }
  }

  getUserApplications() : string[] {
    let user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user) {
      return user.applications;
    }
  }

  isLoggedIn() {
    let user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user) return true;
    else return false;
  }

  isLoggedInUserAdmin() : boolean {
    let user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user && user.roles != undefined && user.roles != null ) {
      let roles : string[] = user.roles;
      if ( roles.includes("ADMIN") ) {
        return true
      }
    }
    return false
  }

  isUserACapacityApprover() : boolean {
    let user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user && user.roles != undefined && user.roles != null ) {
      let roles : string[] = user.roles;
      if ( roles.includes("CAPACITY_APPROVER") ) {
        return true
      }
    }
    return false
  }

  isUserAllowedToCreateNewApplication() : boolean {
    let user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (user && user.roles != undefined && user.roles != null ) {
      let roles : string[] = user.roles;
      if ( roles.includes("CREATE_NEW_APP") ) {
        return true
      }
    }
    return false
  }

  create(user: UserModel) {
    return this.http.post('/api/users', user, this.jwt()).map((response: Response) => response.json());
  }


  jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
      return new RequestOptions({ headers: headers });
    }
  }

  headers() {
    let options = new RequestOptions();
    let headers = new Headers();
    headers.set('Content-Type', 'application/json');
    options.headers = headers;
    return options;
  }

}