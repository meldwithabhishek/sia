import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class ConfigurationService {

  constructor(private http : Http) { }

  private config : any;
  private _url_airlines: string = "assets/json/carrier.json";

  readConfigFile(){
    
    return this.http.get('assets/config.json').map(res => {
        this.config = res.json();
        if( this.config != undefined && this.config !=null){
          return true;
        }
        return false;
    });
  }

  getConfig(){
    if (this.config == undefined) {
      this.config = JSON.parse(sessionStorage.getItem("config"));
    }
    return this.config;
  }

  getAirlinesList() {
    return this.http.get(this._url_airlines).map((response : Response) => response.json());    
  }

}


