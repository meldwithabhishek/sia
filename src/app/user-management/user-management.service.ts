import { AuthService } from './../shared/services/auth.service';
import { Observable } from 'rxjs/Rx';
import { ConfigurationService } from './../shared/services/configuration.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';


@Injectable()
export class UserManagementService {

  constructor(private _http: Http, private config: ConfigurationService, private authService: AuthService) { }

  getRegisteredUsers(): Observable<any> {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "User")
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  getUserDetails(userName: string): Observable<any> {
    return this._http.get(this.config.getConfig().SIA_WEB_APP + "User/" + userName)
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  saveOrUpdateUser(param, user: string, userList: string[]): Observable<any> {
    if (userList.includes(user)) {
      return this._http.put(this.config.getConfig().SIA_WEB_APP + "User", param, this.headers())
        .map((response: Response) => response.json())
        .catch(error => Observable.throw(error.json()));
    } else {
      return this._http.post(this.config.getConfig().SIA_WEB_APP + "User", param, this.headers())
        .map((response: Response) => response.json())
        .catch(error => Observable.throw(error.json()));
    }
  }

  removeUser(userName: string) {
    return this._http.delete(this.config.getConfig().SIA_WEB_APP + "User/" + userName, this.headers())
      .map((response: Response) => response.json())
      .catch(error => Observable.throw(error.json()));
  }

  headers() {
    let options = new RequestOptions();
    let headers = new Headers();
    headers.set('Content-Type', 'application/json');
    headers.set('Requester', this.authService.getUserName());
    headers.set('Caller', 'SIAWA');
    options.headers = headers;
    return options;
  }

}
