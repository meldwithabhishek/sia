import { ApplicationDetailsService } from './../applications/application-details/application-details.service';
import { element } from 'protractor';
import { UserModel } from './../shared/models/auth-models/user-model';
import { UserManagementService } from './user-management.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';


@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {

  userList: string[] = []
  user: string;
  email: string
  isAdmin = false;
  isCapacityApprover = false
  isEntitledToCreateApplication = false;

  selectedApps: string[] = [];
  nonSelectedApps: string[] = [];

  selectedApps_selected: string[]
  nonSelectedApps_selected: string[]

  allApps: string[] = [];

  userDetails: UserModel = new UserModel();

  showProperties = false;

  constructor(private usrMng: UserManagementService, private appService: ApplicationDetailsService, private authService: AuthService) { }

  ngOnInit() {



    this.getRegisteredUsers()

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appService.getUniqueApplications().subscribe(resp => {
      resp.forEach(element => {
        this.allApps.push(element.airlineName + ' - ' + element.name)
      });
      this.allApps.sort(function (a, b) {
        return a.toLowerCase().localeCompare(b.toLowerCase());
      });
      HoldOn.close();
    },
      error => {
        console.error(error);
        HoldOn.close();
        bootbox.alert({
          title: "Error: " + error.httpErrorStatus + "(" + error.httpErrorCode + ")",
          size: "small",
          message: error.errorMessage
        });
      });


  }

  getRegisteredUsers() {
    this.userList = new Array
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.usrMng.getRegisteredUsers().subscribe(resp => {
      /*
      let loggedInUser = this.authService.getUserName()
      resp.forEach(element => {
        if (loggedInUser != element) {
          this.userList.push(element)
        }
      });*/
      this.userList = resp;
      HoldOn.close();
    },
      error => {
        console.error(error);
        HoldOn.close();
        bootbox.alert({
          title: "Error: " + error.httpErrorStatus + "(" + error.httpErrorCode + ")",
          size: "small",
          message: error.errorMessage
        });
      });
  }


  userSelected() {



    if (this.userList.indexOf(this.user) > -1) {
      this.showProperties = false
      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.usrMng.getUserDetails(this.user).subscribe(resp => {
        this.userDetails = resp;
        HoldOn.close();
        this.populateRegisteredUserDetails()
        this.showProperties = true
      },
        error => {
          console.error(error);
          HoldOn.close();
          bootbox.alert({
            title: "Error: " + error.httpErrorStatus + "(" + error.httpErrorCode + ")",
            size: "small",
            message: error.errorMessage
          });
        });
    }
  }

  isNewUser() {
    if (this.userList.indexOf(this.user) > -1) {
      return true;
    } else {
      return false;
    }
  }

  populateRegisteredUserDetails() {
    this.isAdmin = false
    this.isCapacityApprover = false
    this.email = this.userDetails.email
    this.isEntitledToCreateApplication = false;
    this.nonSelectedApps = []
    this.selectedApps = []
    if (this.userDetails.roles != undefined && this.userDetails.roles.indexOf("ADMIN") > -1) {
      this.isAdmin = true
    }
    if (this.userDetails.roles != undefined && this.userDetails.roles.indexOf("CAPACITY_APPROVER") > -1) {
      this.isCapacityApprover = true
    }
    if (this.userDetails.roles != undefined && this.userDetails.roles.indexOf("CREATE_NEW_APP") > -1) {
      this.isEntitledToCreateApplication = true
    }
    if (this.userDetails.applications != undefined) {
      this.userDetails.applications.forEach(app => {
        this.selectedApps.push(app.customerId + " - " + app.application)
      }
      )
    }

    let adminCarrier = this.authService.getUserCarrier()
    this.allApps.forEach(app => {
      if (this.selectedApps.indexOf(app) == -1) {       
        if (adminCarrier != "1A" && app.indexOf(adminCarrier) == 0) {
          this.nonSelectedApps.push(app)
        } else if (adminCarrier == "1A") {
          this.nonSelectedApps.push(app)
        } else if (adminCarrier == "LX" && (app.indexOf("LX") == 0 || app.indexOf("SN") == 0)) {
          this.nonSelectedApps.push(app)
        }
      }
    }
    )
  }

  setNewUser() {
    if (this.user != undefined && this.user != "" && this.userList.indexOf(this.user) == -1) {
      this.isAdmin = false;
      this.isCapacityApprover = false;
      this.isEntitledToCreateApplication = false;
      this.showProperties = true
      this.userDetails.userName = this.user
      this.userDetails.roles = []
      this.userDetails.applications = []
      this.nonSelectedApps = this.allApps;
      this.selectedApps = []
      this.email = undefined

    }
  }

  moveAppsToSelected() {
    this.selectedApps = this.selectedApps.concat(this.nonSelectedApps_selected).sort(function (a, b) {
      return a.toLowerCase().localeCompare(b.toLowerCase());
    });
    var temp = []
    this.nonSelectedApps.forEach(element => {
      if (this.nonSelectedApps_selected.indexOf(element) == -1) {
        temp.push(element)
      }
    })
    this.nonSelectedApps = temp
    this.nonSelectedApps_selected = []
  }

  moveAppsToNonSelected() {
    this.nonSelectedApps = this.nonSelectedApps.concat(this.selectedApps_selected).sort(function (a, b) {
      return a.toLowerCase().localeCompare(b.toLowerCase());
    });
    var temp = []
    this.selectedApps.forEach(element => {
      if (this.selectedApps_selected.indexOf(element) == -1) {
        temp.push(element)
      }
    })
    this.selectedApps = temp
    this.selectedApps_selected = []
  }

  saveOrCreateUser() {
    var roleArray: string[] = []
    if (this.isAdmin) {
      roleArray.push("ADMIN")
      /*this.selectedApps_selected = this.selectedApps;
      this.moveAppsToNonSelected()*/
      //this.isEntitledToCreateApplication = false
      this.isCapacityApprover = false
    } else if (this.isCapacityApprover) {
      roleArray.push("CAPACITY_APPROVER")
      this.selectedApps_selected = this.selectedApps;
      this.moveAppsToNonSelected()
      this.isEntitledToCreateApplication = false
      this.isAdmin = false
    } 
    
    if (this.isEntitledToCreateApplication) {
      roleArray.push("CREATE_NEW_APP")
    }

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });

    let appList : any[] = []
    this.selectedApps.forEach(

      element => {
        appList.push({ 'customerId': element.split(' - ')[0], 'application': element.split(' - ')[1]})
      }
    )

    console.log(appList)

    this.usrMng.saveOrUpdateUser({ userName: this.user, email: this.email, roles: roleArray, applications: appList }, this.user, this.userList).subscribe(resp => {
      HoldOn.close();
      this.getRegisteredUsers()
      this.showProperties = true
      bootbox.alert({
        title: "Success",
        size: "small",
        message: "User Created/Updated Successfully"
      });
    },
      error => {
        console.error(error);
        HoldOn.close();
        bootbox.alert({
          title: "Error: " + error.httpErrorStatus + "(" + error.httpErrorCode + ")",
          size: "small",
          message: error.errorMessage
        });
      });

  }

  remove() {
    let result = confirm("Are you sure?")

    if (result) {

      HoldOn.open({
        theme: "sk-cube-grid",
        message: 'Loading...'
      });
      this.usrMng.removeUser(this.user).subscribe(resp => {
        HoldOn.close();
        this.showProperties = false

        if (resp && resp.message) {
          bootbox.alert({
            title: "Alert",
            size: "medium",
            message: resp.message
          });
        } else {
          bootbox.alert({
            title: "Success",
            size: "small",
            message: "User Deleted Successful"
          });
        }


        this.user = undefined
        this.email = undefined
        this.getRegisteredUsers()
      },
        error => {
          console.error(error);
          HoldOn.close();
          bootbox.alert({
            title: "Error: " + error.httpErrorStatus + "(" + error.httpErrorCode + ")",
            size: "small",
            message: error.errorMessage
          });
        });

    }
  }

  onAdminChange() {
    this.isAdmin = !this.isAdmin
    if (this.isAdmin) {
      this.isCapacityApprover = false
    }
  }

  onCapacityApproverChange() {
    this.isCapacityApprover = !this.isCapacityApprover
    if (this.isCapacityApprover) {
      this.isAdmin = false
    }
  }

}

declare let HoldOn: any;
declare let bootbox: any;
