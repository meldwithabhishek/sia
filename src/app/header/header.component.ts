import { Router } from '@angular/router';
import { AuthService } from './../shared/services/auth.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
  }

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }


  isLoggedIn() {
    if (sessionStorage.getItem('isGuestLoggedIn')) {
      // logged in for guest users
      return true;
    } else {
      return this.authService.isLoggedIn();
    }
  }

  getUsername() {
    if (sessionStorage.getItem('isGuestLoggedIn')) {
      // logged in for guest users
      return "Guest";
    } else {
      return this.authService.getUserName();
    }
  }

  logout() {
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Logging out..'
    });
    this.authService.logout().subscribe(resp => {
      HoldOn.close();
    },
      error => {
        HoldOn.close();
        console.log(error)
      }
    )
    this.router.navigate(['/auth']);
    /*.subscribe(data => {
      let response = JSON.parse(data.text());
      bootbox.alert({
          title: "Info",
          size: "small",
          message: response.message
        });
      this.router.navigate(['/auth']);
    },
    error => {
      bootbox.alert({
          title: "Info",
          size: "small",
          message: "Server Error",
        });
      this.router.navigate(['/auth']);
    });*/
  }
}

declare let bootbox: any;
declare let HoldOn: any;