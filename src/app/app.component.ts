import { ConfigurationService } from './shared/services/configuration.service';
import { AlertService } from './alert/alert.service';
import { AuthService } from './shared/services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit, HostListener } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  

  totalAlerts: number;
  username: any;

  constructor(private router: Router, private authService: AuthService, private alertService: AlertService, private configService: ConfigurationService) {
    
  }

  /*
  @HostListener('window:unload', [ '$event' ])
  unloadHandler(event) {
    
  }

  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHander(event) {
    if (this.isLoggedIn()) {
      this.logout()
    }
  }
  */

  ngOnDestroy(): void {
  }

  logout() {    

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Logging out..'
    });
    this.authService.logout().subscribe(resp => {
      HoldOn.close();
    },
    error => {
      HoldOn.close();
      console.log(error)
    }
    )
    
    /*.subscribe(data => {
      let response = JSON.parse(data.text());
      bootbox.alert({
          title: "Info",
          size: "small",
          message: response.message
        });
      this.router.navigate(['/auth']);
    },
    error => {
      bootbox.alert({
          title: "Info",
          size: "small",
          message: "Server Error",
        });
      this.router.navigate(['/auth']);
    });*/
    sessionStorage.clear();
  }

  getUsername() {
    return this.authService.getUserName();
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

 

  ngOnInit() {


    //this.getAlerts();
    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading Application'
    });
    this.configService.readConfigFile().subscribe(
      data => {
        HoldOn.close();
        if (data == true) {
          this.configService.getConfig();
          sessionStorage.setItem("config", JSON.stringify(this.configService.getConfig()))
        }
        else {
          bootbox.alert({
            title: "Error",
            size: "small",
            message: "Failed to load application configuration"
          });
        }
      },
      error => {
        HoldOn.close();
        bootbox.alert({
          title: "Error",
          size: "small",
          message: "Failed to load application configuration"
        });

      }
    )
  }  

}


declare let bootbox: any;
declare let HoldOn: any;
