import { Router, Routes, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApplicationDetailsService } from '../applications/application-details/application-details.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})


export class SidenavComponent implements OnInit {

  constructor(location: Location, private activatedRoute: ActivatedRoute, private appDetailServ: ApplicationDetailsService) { }

  @Input() headerTitle: string = "Dashboard";
  @Input() items: any;
  @Output() itemClicked = new EventEmitter();
  selected : any;
  approvalCount : number = 0

  ngOnInit() {

    HoldOn.open({
      theme: "sk-cube-grid",
      message: 'Loading...'
    });
    this.appDetailServ.getApplicationsPendingApproval().subscribe(appList => {
      this.approvalCount = appList.length
      HoldOn.close();      
    },
      error => {
        console.error(error);
        HoldOn.close();       
      });

    let path = location.toString()
    
    if (path.includes("sync-catalogue")) {
      this.selected = this.items[1];
    } else if (path.includes("async-ems-catalogue")) {
      this.selected = this.items[2];
    } else if (path.includes("batch-catalogue")) {
      this.selected = this.items[3];
    } else if (path.includes("user-management")) {
      this.selected = this.items[5];
    }  else if (path.includes("approvals")) {
      this.selected = this.items[5];
    } else if (path.includes("applications")) {
      this.selected = this.items[0];
    } else if (path.includes("incident-management")) {
      this.selected = this.items[4]
    }
    
    
  }

  onItemClicked(item: any) {
    this.selected = item;
    this.itemClicked.emit(item)
  }
}


declare let HoldOn: any;
declare let bootbox: any;
